/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <sys/vfs.h>
#include <sys/stat.h>
#include <linux/magic.h>
#include <dirent.h>
#include <errno.h>
#include <limits.h>
#include <sys/mount.h>

#include <cthulhu/cthulhu_helpers.h>
#include <cthulhu/cthulhu_defines.h>

#include "cthulhu_priv.h"

#define ME "cgroup"


#ifdef CGROUP_DIR_OVERRIDE
#define CGROUP_DIR STR(CGROUP_DIR_OVERRIDE)
#else
#define CGROUP_DIR "/sys/fs/cgroup"
#endif

#define CGROUP_BASE "cthulhu"


// initialize all supported cgroup controllers
static controller_t controllers[controller_last] = {
    {"cpuset", controller_cpuset, cgroup_v1},
    {"cpu", controller_cpu, cgroup_v1},
    {"memory", controller_mem, cgroup_v1},
    {"devices", controller_dev, cgroup_v1}
};

controller_t* cthulhu_cgroup_get_controller(controller_type_t type) {
    ASSERT_FALSE(type < controller_cpuset, return NULL, "controller type is invalid [%d]", type);
    ASSERT_FALSE(type < controller_cpuset, return NULL, "controller type is invalid [%d]", type);
    ASSERT_FALSE(type >= controller_last, return NULL, "controller type is invalid [%d]", type);
    return &controllers[type];
}

/**
 * @brief Set a setting of a cgroup
 *
 * @param cgroup_name The cgroup name of the sandbox
 * @param cgroup the cgroup
 * @param setting
 * @param data
 * @return int 0 on success, -1 on failure
 */
static int cthulhu_cgroup_setting_set(const char* cgroup_name, controller_t* controller, const char* setting, const char* data) {
    int res = -1;
    FILE* f = NULL;
    char path[PATH_MAX] = {0};

    ASSERT_NOT_NULL(controller, goto exit, "Controller is not defined");
    ASSERT_NOT_NULL(setting, goto exit, "Setting is not defined");
    ASSERT_NOT_NULL(data, goto exit, "Data is not defined");

    if(STRING_EMPTY(cgroup_name)) {
        if(controller->version == cgroup_v1) {
            sprintf(path, "%s/%s/%s", CGROUP_DIR, controller->name, setting);
        } else {
            sprintf(path, "%s/%s", CGROUP_DIR, setting);
        }
    } else {
        if(controller->version == cgroup_v1) {
            sprintf(path, "%s/%s/%s/%s", CGROUP_DIR, controller->name, cgroup_name, setting);
        } else {
            sprintf(path, "%s/%s/%s", CGROUP_DIR, cgroup_name, setting);
        }
    }

    f = fopen(path, "w");
    ASSERT_NOT_NULL(f, goto exit, "Could not open [%s] (%d: %s)", path, errno, strerror(errno));

    SAH_TRACEZ_INFO(ME, "Write %s to %s", data, path);
    ASSERT_NOT_EQUAL(fputs(data, f), EOF, goto exit,
                     "Could not write %s to %s (%d: %s)", data, path, errno, strerror(errno));

    res = 0;
exit:
    if(f) {
        fclose(f);
    }
    return res;
}

char* cthulhu_cgroup_setting_get(const char* cgroup_name, controller_t* controller, const char* setting) {
    FILE* f = NULL;
    char path[PATH_MAX] = {0};
    char* data = NULL;

    data = (char*) calloc(1, 64);

    if(controller->version == cgroup_v1) {
        sprintf(path, "%s/%s/%s/%s", CGROUP_DIR, controller->name, cgroup_name, setting);
    } else {
        sprintf(path, "%s/%s/%s", CGROUP_DIR, cgroup_name, setting);
    }
    f = fopen(path, "r");
    ASSERT_NOT_NULL(f, goto exit, "Could not open [%s] (%d: %s)", path, errno, strerror(errno));
    ASSERT_NOT_NULL(fgets(data, 64, f), goto error,
                    "Could not read from %s (%d: %s)", path, errno, strerror(errno));

    // replace '\n' with '\0'
    for(size_t i = 0; i < sizeof(data); i++) {
        if(data[i] == '\0') {
            break;
        } else if(data[i] == '\n') {
            data[i] = '\0';
            break;
        }
    }

    goto exit;
error:
    free(data);
    data = NULL;
exit:
    if(f) {
        fclose(f);
    }
    return data;
}

/**
 * @brief Create cgroups for a sandbox
 *
 * @param name
 * @return int 0 on success, -1 on failure
 */
int cthulhu_cgroup_create(const char* cgroup_name) {
    int res = -1;
    DIR* cgroup_root = NULL;
    char path[PATH_MAX] = {0};
    bool cgroupv2_present = false;
    controller_t* v2controller = NULL;
    amxc_string_t controllers_to_enable;
    amxc_string_init(&controllers_to_enable, 0);

    for(controller_type_t ct = controller_cpuset; ct < controller_last; ct++) {
        if(controllers[ct].version == cgroup_v1) {
            snprintf(path, sizeof(path), "%s/%s/%s", CGROUP_DIR, controllers[ct].name, cgroup_name);
            ASSERT_SUCCESS(cthulhu_mkdir(path, true), goto exit, "Could not make cgroup dir [%s]", path);
        } else {
            cgroupv2_present = true;
            v2controller = &controllers[ct];
            // devices does not have a dedicated controller in cgroupv2, so it cannot be enabled
            if(ct != controller_dev) {
                amxc_string_appendf(&controllers_to_enable, " +%s", controllers[ct].name);
            }
        }
    }

    if(cgroupv2_present) {
        snprintf(path, sizeof(path), "%s/%s", CGROUP_DIR, cgroup_name);
        if(!cthulhu_isdir(path)) {
            ASSERT_SUCCESS(cthulhu_mkdir(path, false), goto exit, "Could not make cgroup dir [%s]", path);
        }
        // enable the controlers for the subgroups
        if(!amxc_string_is_empty(&controllers_to_enable)) {
            cthulhu_cgroup_setting_set(cgroup_name, v2controller, "cgroup.subtree_control", controllers_to_enable.buffer + 1);
        }
    }

    res = 0;
exit:
    if(cgroup_root) {
        closedir(cgroup_root);
    }
    amxc_string_clean(&controllers_to_enable);
    return res;
}

/**
 * @brief Destroy cgroups from a sandbox
 *
 * @param cgroup_name
 * @return int 0 on success, -1 on failure
 */
void cthulhu_cgroup_destroy(const char* cgroup_name) {
    DIR* cgroup_root = NULL;
    char path[PATH_MAX] = {0};
    bool cgroupv2_present = false;

    for(controller_type_t ct = controller_cpuset; ct < controller_last; ct++) {
        if(controllers[ct].version == cgroup_v1) {
            snprintf(path, sizeof(path), "%s/%s/%s", CGROUP_DIR, controllers[ct].name, cgroup_name);
            ASSERT_SUCCESS(rmdir(path), NO_INSTRUCTION, "Could not remove cgroup dir [%s] (%d: %s)", path, errno, strerror(errno));
        } else {
            cgroupv2_present = true;
        }
    }

    if(cgroupv2_present) {
        snprintf(path, sizeof(path), "%s/%s", CGROUP_DIR, cgroup_name);
        ASSERT_SUCCESS(rmdir(path), NO_INSTRUCTION, "Could not remove cgroup dir [%s] (%d: %s)", path, errno, strerror(errno));
    }

    if(cgroup_root) {
        closedir(cgroup_root);
    }
}

static bool cthulhu_cgroup_setting_exists(controller_t* controller, const char* setting) {
    bool exists = false;
    char path[PATH_MAX] = {0};

    if(controller->version == cgroup_v1) {
        sprintf(path, "%s/%s/%s", CGROUP_DIR, controller->name, setting);
    } else {
        sprintf(path, "%s/%s", CGROUP_DIR, setting);
    }

    if(access(path, F_OK) == 0) {
        exists = true;
    }
    return exists;
}


static int64_t cgroup_mem_undefined = 0x1FFFFFFFFFFFFC;  // this is 0x7FFFFFFFFFFFF000 / 1024
/**
 * @brief recursively get the smallest available memory in the sandbox hierarchy
 *
 * @param sb_id
 * @return uint64_t
 */
static uint64_t cthulhu_cgroup_available_mem_get(const char* sb_id) {
    uint64_t my_limit = cgroup_mem_undefined;
    uint64_t parent_limit = cgroup_mem_undefined;
    char* parent_sb_id = NULL;
    char* cgroup_dir = NULL;
    char* data = NULL;
    amxd_object_t* sb_obj = NULL;

    sb_obj = cthulhu_sb_get(sb_id);
    when_null(sb_obj, exit);
    parent_sb_id = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_PARENT, NULL);

    cgroup_dir = cthulhu_cgroup_dir_get(sb_id);
    if(string_is_empty(cgroup_dir)) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get cgroup dir", sb_id);
        goto exit;
    }
    if(controllers[controller_mem].version == cgroup_v1) {
        data = cthulhu_cgroup_setting_get(cgroup_dir, &controllers[controller_mem], "memory.limit_in_bytes");
        if(!data) {
            SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get memory.limit_in_bytes", sb_id);
            goto exit;
        }
        my_limit = atol(data) / 1024;
    } else {
        data = cthulhu_cgroup_setting_get(cgroup_dir, &controllers[controller_mem], "memory.max");
        if(!data) {
            SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get memory.max", sb_id);
            goto exit;
        }
        if(strcmp(data, "max") != 0) {
            my_limit = atol(data) / 1024;
        }
    }
    if(!string_is_empty(parent_sb_id)) {
        parent_limit = cthulhu_cgroup_available_mem_get(parent_sb_id);
    }

exit:
    free(cgroup_dir);
    free(parent_sb_id);
    free(data);
    return (my_limit < parent_limit) ? my_limit : parent_limit;
}

int cthulhu_cgroup_memory_resources_get(const char* sb_id, cthulhu_resource_stats_t* cthulhu_stats) {
    int res = -1;
    char* cgroup_dir = NULL;
    char* data = NULL;

    when_null(cthulhu_stats, exit);

    cgroup_dir = cthulhu_cgroup_dir_get(sb_id);
    if(string_is_empty(cgroup_dir)) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get cgroup dir", sb_id);
        goto exit;
    }
    if(controllers[controller_mem].version == cgroup_v1) {
        data = cthulhu_cgroup_setting_get(cgroup_dir, &controllers[controller_mem], "memory.usage_in_bytes");
    } else {
        data = cthulhu_cgroup_setting_get(cgroup_dir, &controllers[controller_mem], "memory.current");
    }
    ASSERT_NOT_NULL(data, goto exit, "SB[%s]: Could not get memory usage", sb_id);

    cthulhu_stats->used = atol(data) / 1024;
    cthulhu_stats->total = cthulhu_cgroup_available_mem_get(sb_id);

    if(cthulhu_stats->total == cgroup_mem_undefined) {
        // no limit configured
        // optimization: get free memory from /proc/meminfo
        cthulhu_stats->total = -1;
        cthulhu_stats->free = -1;
        goto exit;
    } else {
        cthulhu_stats->free = cthulhu_stats->total - cthulhu_stats->used;
    }
    // SAH_TRACEZ_INFO(ME, "SB[%s]: Memory Total: %" PRIu64 " Kb Used: %" PRIu64 " Kb, Free: %" PRIu64 " Kb",
    //                 sb_id, cthulhu_stats->total, cthulhu_stats->used, cthulhu_stats->free);
    res = 0;

exit:
    free(data);
    free(cgroup_dir);
    return res;
}

/**
 * @brief Configure the devices cgroup
 *
 * @param cgroup_name The cgroup name of the sandbox
 * @param sb_obj
 * @return int 0 on success, -1 on failure
 */
static int cthulhu_config_devices(const char* cgroup_name, amxd_object_t* sb_obj) {
    int res = 0;

    if(controllers[controller_dev].version == cgroup_v2) {
        SAH_TRACEZ_ERROR(ME, "cgroup v2 devices are not supported yet");
        return -1;
    }

    amxd_object_t* devices = amxd_object_get_child(sb_obj, CTHULHU_SANDBOX_DEVICES);
    ASSERT_NOT_NULL(devices, return -1, "Could not get Devices of sandbox");

    amxd_object_for_each(instance, it, devices) {
        amxd_object_t* device = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* cgroup_dev = NULL;
        char maj_str[16] = {0};
        char min_str[16] = {0};

        int32_t major = amxd_object_get_int32_t(device, CTHULHU_SANDBOX_DEVICES_MAJOR, NULL);
        int32_t minor = amxd_object_get_int32_t(device, CTHULHU_SANDBOX_DEVICES_MINOR, NULL);
        char* type = amxd_object_get_cstring_t(device, CTHULHU_SANDBOX_DEVICES_TYPE, NULL);
        char* access = amxd_object_get_cstring_t(device, CTHULHU_SANDBOX_DEVICES_ACCESS, NULL);
        char* permission = amxd_object_get_cstring_t(device, CTHULHU_SANDBOX_DEVICES_PERMISSION, NULL);

        if(major == -1) {
            sprintf(maj_str, "*");
        } else {
            snprintf(maj_str, sizeof(maj_str), "%u", major);
        }
        if(minor == -1) {
            sprintf(min_str, "*");
        } else {
            snprintf(min_str, sizeof(min_str), "%u", minor);
        }


        if(asprintf(&cgroup_dev, "%s %s:%s %s", type, maj_str, min_str, access) < 0) {
            SAH_TRACEZ_ERROR(ME, "cgroup[%s]: Failed to allocate memory for string", cgroup_name);
            free(type);
            free(access);
            free(permission);
            goto exit;
        }
        if(cgroup_dev && permission) {
            if(strcmp(permission, "Allow") == 0) {
                if(controllers[controller_dev].version == cgroup_v1) {
                    cthulhu_cgroup_setting_set(cgroup_name, &controllers[controller_dev], "devices.allow", cgroup_dev);
                }
            } else {
                if(controllers[controller_dev].version == cgroup_v1) {
                    cthulhu_cgroup_setting_set(cgroup_name, &controllers[controller_dev], "devices.deny", cgroup_dev);
                }
            }
        }
        free(cgroup_dev);
        free(type);
        free(access);
        free(permission);

    }
exit:
    return res;
}

/**
 * @brief Configure the memory cgroup
 *
 * the hard limit will be set to the requested value
 * the soft limit will be set to 90% of the requested value
 *
 * @param cgroup_name The cgroup name of the sandbox
 * @param sb_obj
 * @return int 0 on success, -1 on failure
 */
static int cthulhu_cgroup_config_memory(const char* cgroup_name, amxd_object_t* sb_obj) {
    int res = 0;
    char memory_soft[32] = {0};
    char memory_hard[32] = {0};
    int32_t memory = amxd_object_get_int32_t(sb_obj, CTHULHU_SANDBOX_MEMORY, NULL);

    if(controllers[controller_mem].version == cgroup_v1) {
        if(memory < 0) {
            // Writing -1 resets the previous memory restriction
            snprintf(memory_soft, sizeof(memory_soft), "-1");
            snprintf(memory_hard, sizeof(memory_hard), "-1");
        } else {
            snprintf(memory_soft, sizeof(memory_soft), "%d", memory * 880);
            snprintf(memory_hard, sizeof(memory_hard), "%d", memory * 1024);
        }
        res |= cthulhu_cgroup_setting_set(cgroup_name, &controllers[controller_mem], "memory.soft_limit_in_bytes", memory_soft);
        res |= cthulhu_cgroup_setting_set(cgroup_name, &controllers[controller_mem], "memory.limit_in_bytes", memory_hard);
    } else {
        if(memory < 0) {
            // Writing -1 resets the previous memory restriction
            snprintf(memory_soft, sizeof(memory_soft), "max");
            snprintf(memory_hard, sizeof(memory_hard), "max");
        } else {
            snprintf(memory_soft, sizeof(memory_soft), "%d", memory * 880);
            snprintf(memory_hard, sizeof(memory_hard), "%d", memory * 1024);
        }
        res |= cthulhu_cgroup_setting_set(cgroup_name, &controllers[controller_mem], "memory.high", memory_soft);
        res |= cthulhu_cgroup_setting_set(cgroup_name, &controllers[controller_mem], "memory.max", memory_hard);
    }
    return res;
}

/**
 * @brief helper to convert cgroup settings to int
 *
 * "1" -> 1
 * "0-3" -> 4
 *
 * @param element

 */
static int cthulhu_cgroup_nr_of_cpus_convert(const amxc_string_t* element) {
    amxc_llist_t list;
    int res = 1;

    amxc_llist_init(&list);
    if(amxc_string_split_to_llist(element, &list, '-') != AMXC_STRING_SPLIT_OK) {
        SAH_TRACEZ_ERROR(ME, "Could not split setting [%s]", element->buffer);
        goto exit;
    }
    switch(amxc_llist_size(&list)) {
    case 1:
        goto exit;
    case 2:
    {
        int one, two;
        amxc_string_t* s = amxc_string_from_llist_it(amxc_llist_get_at(&list, 0));
        one = atoi(s->buffer);
        s = amxc_string_from_llist_it(amxc_llist_get_at(&list, 1));
        two = atoi(s->buffer);
        res = two - one + 1;
        goto exit;
    }
    default:
        SAH_TRACEZ_ERROR(ME, "String cannot be decoded [%s]", element->buffer);
        break;
    }

exit:
    amxc_llist_clean(&list, amxc_string_list_it_free);
    return res;
}

static int cthulhu_cgroup_nr_of_cpus(const char* cgroup_name) {
    amxc_string_t cpus;
    amxc_llist_t list;
    amxc_llist_it_t* it;
    int res = 0;

    char* setting = NULL;

    if(controllers[controller_cpuset].version == cgroup_v1) {
        setting = cthulhu_cgroup_setting_get(cgroup_name, &controllers[controller_cpuset], "cpuset.cpus");
    } else {
        setting = cthulhu_cgroup_setting_get(cgroup_name, &controllers[controller_cpuset], "cpuset.cpus.effective");
    }
    ASSERT_NOT_NULL(setting, return 1, "Failed to get the number of CPUs for the cgroup");

    amxc_string_init(&cpus, 0);
    amxc_llist_init(&list);
    amxc_string_setf(&cpus, "%s", setting);

    if(amxc_string_split_to_llist(&cpus, &list, ',') != AMXC_STRING_SPLIT_OK) {
        SAH_TRACEZ_ERROR(ME, "Could not split setting [%s]", setting);
        goto exit;
    }
    while((it = amxc_llist_take_first(&list)) != NULL) {
        amxc_string_t* cpu_str = amxc_string_from_llist_it(it);
        res += cthulhu_cgroup_nr_of_cpus_convert(cpu_str);
        amxc_string_delete(&cpu_str);
    }

exit:
    amxc_string_clean(&cpus);
    amxc_llist_clean(&list, amxc_string_list_it_free);
    free(setting);
    return res;
}

/**
 * @brief Configure the CPU cgroup
 *
 * @param cgroup_name The cgroup name of the sandbox
 * @param sb_obj
 * @return int 0 on success, -1 on failure
 */
static int cthulhu_cgroup_config_cpu(const char* cgroup_name, amxd_object_t* sb_obj) {
    int res = 0;
    char cpu_string[32] = {0};

    int32_t cpu = amxd_object_get_int32_t(sb_obj, CTHULHU_SANDBOX_CPU, NULL);

    if(controllers[controller_cpu].version == cgroup_v1) {

        if(cthulhu_cgroup_setting_exists(&controllers[controller_cpu], "cpu.cfs_quota_us")) {
            // cfs scheduler
            if((cpu < 0) || (cpu >= 100)) {
                res |= cthulhu_cgroup_setting_set(cgroup_name, &controllers[controller_cpu], "cpu.cfs_quota_us", "-1");
            } else {
                int nr_of_cpus = cthulhu_cgroup_nr_of_cpus(cgroup_name);
                SAH_TRACEZ_INFO(ME, "Nr of cpus %d", nr_of_cpus);

                snprintf(cpu_string, sizeof(cpu_string), "%d", cpu * 1000 * nr_of_cpus);
                res |= cthulhu_cgroup_setting_set(cgroup_name, &controllers[controller_cpu], "cpu.cfs_quota_us", cpu_string);
                res |= cthulhu_cgroup_setting_set(cgroup_name, &controllers[controller_cpu], "cpu.cfs_period_us", "100000");
            }
        } else if(cthulhu_cgroup_setting_exists(&controllers[controller_cpu], "cpu.shares")) {
            // rt scheduler
            if((cpu < 0) || (cpu >= 100)) {
                res |= cthulhu_cgroup_setting_set(cgroup_name, &controllers[controller_cpu], "cpu.shares", "1024");
            } else {
                snprintf(cpu_string, sizeof(cpu_string), "%d", (cpu * 1024) / 100);
                res |= cthulhu_cgroup_setting_set(cgroup_name, &controllers[controller_cpu], "cpu.shares", cpu_string);

            }
        }
    } else {
        if((cpu < 0) || (cpu >= 100)) {
            res |= cthulhu_cgroup_setting_set(cgroup_name, &controllers[controller_cpu], "cpu.max", "max 100000");
        } else {
            int nr_of_cpus = cthulhu_cgroup_nr_of_cpus(cgroup_name);
            SAH_TRACEZ_INFO(ME, "Nr of cpus %d", nr_of_cpus);
            // we allocate for a total % for all cpus
            // so if there are 2 cpus, and requested load is 60%, then
            // the cgroup can have 120000 slices of the cpus
            snprintf(cpu_string, sizeof(cpu_string), "%d 100000", cpu * 1000 * nr_of_cpus);
            res |= cthulhu_cgroup_setting_set(cgroup_name, &controllers[controller_cpu], "cpu.max", cpu_string);
        }
    }
    return res;
}

/**
 * @brief Configure the cgroups of a sandbox
 *
 * @param cgroup_name The cgroup name of the sandbox
 * @param sb_obj
 * @param notif_obj
 * @param command_id
 * @return int 0 on success, -1 on failure
 */
int cthulhu_cgroup_config(const char* cgroup_name, amxd_object_t* sb_obj, cthulhu_notif_data_t* notif_data) {
    int res = -1;
    if(cthulhu_config_devices(cgroup_name, sb_obj) < 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not configure device cgroup for [%s]", cgroup_name);
        goto exit;
    }
    if(cthulhu_cgroup_config_cpu(cgroup_name, sb_obj) < 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not configure cpu cgroup for [%s]", cgroup_name);
        goto exit;
    }
    if(cthulhu_cgroup_config_memory(cgroup_name, sb_obj) < 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not configure memory cgroup for [%s]", cgroup_name);
        goto exit;
    }
    res = 0;
exit:
    return res;
}

/**
 * @brief get or create a cgroup name for a sandbox.
 *
 * the function takes cgroups of parents into consideration
 * It will contruct the hierarchy of cgroups for a sandbox,
 * so the part after /sys/fs/cgroup
 *
 * The returned char* should be freed
 *
 * @param sb_id
 * @return char*
 */
char* cthulhu_cgroup_dir_get(const char* sb_id) {
    char* cgroup_dir = NULL;
    char* parent_cgroup_dir = NULL;
    char* parent_sb_id = NULL;
    amxd_object_t* sb_obj = NULL;
    cthulhu_sb_priv_t* sb_priv = NULL;

    sb_obj = cthulhu_sb_get(sb_id);
    ASSERT_NOT_NULL(sb_obj, return NULL, "SB[%s]: Sandbox does not exist", sb_id);

    sb_priv = cthulhu_sb_get_priv(sb_obj);
    if(sb_priv && sb_priv->cgroup_dir) {
        // return the cached value
        return strdup(sb_priv->cgroup_dir);
    }
    parent_sb_id = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_PARENT, NULL);
    // case 0: the sb has no parent
    if(string_is_empty(parent_sb_id)) {
        if(asprintf(&cgroup_dir, "%s/%s", CGROUP_BASE, sb_id) < 0) {
            SAH_TRACEZ_ERROR(ME, "SB[%s]: Failed to allocate memory for string", sb_id);
        }
        goto exit;
    }
    // case 1: append this sb to the parent dir
    parent_cgroup_dir = cthulhu_cgroup_dir_get(parent_sb_id);
    if(string_is_empty(parent_cgroup_dir)) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Parent cgroup dir is empty", sb_id);
        goto exit;
    }
    if(asprintf(&cgroup_dir, "%s/%s", parent_cgroup_dir, sb_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Failed to allocate memory for string", sb_id);
    }

exit:
    free(parent_sb_id);
    free(parent_cgroup_dir);
    return cgroup_dir;
}

/**
 * @brief check if the cgroup setup is v1 or v2 or a combination
 *
 * @return int
 */
static int cthulhu_cgroup_probe_layout(void) {
    int res = -1;
    char controllers_file[PATH_MAX] = {0};
    FILE* f = NULL;
    char line[1024] = {0};
    amxc_string_t controllers_str;
    amxc_llist_t controllers_list;
    amxc_string_init(&controllers_str, 0);
    amxc_llist_init(&controllers_list);
    controller_t* v2controller = NULL;
    amxc_string_t controllers_to_enable;
    amxc_string_init(&controllers_to_enable, 0);

    snprintf(controllers_file, sizeof(controllers_file), "%s/cgroup.controllers", CGROUP_DIR);
    if(!cthulhu_isfile(controllers_file)) {
        //everything is v1
        res = 0;
        goto exit;
    }
    f = fopen(controllers_file, "r");
    ASSERT_NOT_NULL(f, goto exit, "Failed to open file [%s]", controllers_file);
    ASSERT_NOT_NULL(fgets(line, sizeof(line), f),
                    goto exit, "failed to read file [%s]", controllers_file);

    ASSERT_TRUE(amxc_string_set(&controllers_str, line) == strlen(line), goto exit, "Could not set controllers_str");

    ASSERT_SUCCESS(amxc_string_split_to_llist(&controllers_str, &controllers_list, ' '),
                   goto exit, "Could not split controllers_str [%s]", controllers_str.buffer);

    amxc_llist_for_each(it, &controllers_list) {
        amxc_string_t* s = amxc_string_from_llist_it(it);
        if(strcmp(s->buffer, "cpuset") == 0) {
            controllers[controller_cpuset].version = cgroup_v2;
            amxc_string_appendf(&controllers_to_enable, " +%s", controllers[controller_cpuset].name);
            v2controller = &controllers[controller_cpuset];
        } else if(strcmp(s->buffer, "cpu") == 0) {
            controllers[controller_cpu].version = cgroup_v2;
            amxc_string_appendf(&controllers_to_enable, " +%s", controllers[controller_cpu].name);
            v2controller = &controllers[controller_cpuset];

        } else if(strcmp(s->buffer, "memory") == 0) {
            controllers[controller_mem].version = cgroup_v2;
            amxc_string_appendf(&controllers_to_enable, " +%s", controllers[controller_mem].name);
            v2controller = &controllers[controller_cpuset];

        }
    }
    if(!amxc_string_is_empty(&controllers_to_enable)) {
        cthulhu_cgroup_setting_set(NULL, v2controller, "cgroup.subtree_control", controllers_to_enable.buffer + 1);
    }
    // devices does not have a controller in v2, so if the v1 controller does not exist,
    // we will assume v2
    snprintf(controllers_file, sizeof(controllers_file), "%s/devices", CGROUP_DIR);
    if(!cthulhu_isdir(controllers_file)) {
        controllers[controller_dev].version = cgroup_v2;
    }
    res = 0;
exit:
    if(f) {
        fclose(f);
    }
    amxc_llist_clean(&controllers_list, amxc_string_list_it_free);
    amxc_string_clean(&controllers_to_enable);
    amxc_string_clean(&controllers_str);
    return res;
}


/**
 * @brief mount the cgroup v1 controller if it does not exist yet
 *
 * @return int
 */
static int cthulhu_cgroup_create_devices_v1(void) {
    char path[PATH_MAX] = {0};

    snprintf(path, sizeof(path), "%s/%s", CGROUP_DIR, controllers[controller_dev].name);
    if(cthulhu_isdir(path)) {
        return 0;
    }
    ASSERT_SUCCESS(cthulhu_mkdir(path, false), return -1, "Failed to create dir [%s]", path);

    if(mount("devices", path, "cgroup", MS_NODEV | MS_NOEXEC | MS_NOSUID, "devices") < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to mount devices cgroup (%d: %s)", errno, strerror(errno));
        return -1;
    }
    return 0;
}

/**
 * @brief Set global cgroup settings that are needed for cthulhu to work
 *
 * @return int
 */
int cthulhu_cgroup_init(void) {
    int res = -1;
    // the devices cgroup has been removed in cgroup v2, in preference of
    // BPF filtering to allow access to devices. This has not been implemented yet,
    // so we will try to mount the cgroup v1 devices controller here.
    cthulhu_cgroup_create_devices_v1();
    ASSERT_SUCCESS(cthulhu_cgroup_probe_layout(), goto exit, "Could not probe cgroup layout");
    if(controllers[controller_cpuset].version == cgroup_v1) {
        ASSERT_SUCCESS(cthulhu_cgroup_setting_set(NULL, &controllers[controller_cpuset], "cgroup.clone_children", "1"),
                       goto exit, "Failed to set cgroup.clone_children");
    }
    ASSERT_SUCCESS(cthulhu_cgroup_create(CGROUP_BASE), goto exit, "Could not create cgroup [%s]", CGROUP_BASE);
    res = 0;
exit:
    return res;
}
