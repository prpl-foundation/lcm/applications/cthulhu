/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <errno.h>
#include <libgen.h>

#include "cthulhu_priv.h"
#include "cthulhu_unprivileged.h"

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_config.h>
#include <cthulhu/cthulhu_config_variant.h>
#include <cthulhu/cthulhu_ctr_info.h>
#include <cthulhu/cthulhu_ctr_info_variant.h>
#include <cthulhu/cthulhu_ctr_start_data.h>
#include <cthulhu/cthulhu_ctr_start_data_variant.h>
#include <cthulhu/cthulhu_defines.h>
#include <amxj/amxj_variant.h>
#include <amxb/amxb.h>
#include <lcm/amxc_data.h>
#include <lcm/lcm_dump_rpc.h>
#include <lcm/lcm_helpers.h>

#define ME "rpc"

static amxm_shared_object_t* backend = NULL;
static amxp_timer_t* update_timer = NULL;
static unsigned int update_poll_time = 120000;      // time in ms between polling of container states
static amxp_timer_t* dm_changed_timer = NULL;       // when the dm has changed, an update will be sent
static amxc_htable_t ctr_sigmngrs;
static amxc_set_t* ctr_start_cb_underway_set;

static void cthulhu_ctr_start_canceled_cb(UNUSED const char* const sig_name,
                                          UNUSED const amxc_var_t* const data,
                                          void* const priv);

typedef struct _cb_data_ctr {
    char* ctr_id;
    cthulhu_notif_data_t* notif_data;
} cb_data_ctr_t;

/**
 * Helper to append a string as a amxc_var_t to an amxc_llist_t
 *
 * @return 0 on success
 *
 */
int llist_append_string(amxc_llist_t* list, const char* str) {
    int retval = -1;
    amxc_var_t* var = NULL;

    ASSERT_NOT_NULL(list, goto exit);
    ASSERT_NOT_NULL(str, goto exit);

    ASSERT_SUCCESS(amxc_var_new(&var), goto exit);
    ASSERT_SUCCESS(amxc_var_set(cstring_t, var, str), goto exit);

    ASSERT_SUCCESS(amxc_llist_append(list, &var->lit), goto exit);
    retval = 0;

exit:
    if(retval != 0) {
        amxc_var_delete(&var);
    }
    return retval;
}

amxm_shared_object_t* cthulhu_backend_get(void) {
    return backend;
}

cthulhu_ctr_priv_t* cthulhu_ctr_get_priv(amxd_object_t* ctr) {
    if(!ctr) {
        return NULL;
    }
    if(!ctr->priv) {
        ctr->priv = (cthulhu_ctr_priv_t*) calloc(1, sizeof(cthulhu_ctr_priv_t));
        cthulhu_ctr_priv_t* ctr_priv = (cthulhu_ctr_priv_t* ) ctr->priv;
        cthulhu_ctr_priv_init(ctr_priv);

    }
    return (cthulhu_ctr_priv_t*) ctr->priv;
}

void delete_ctr_priv(amxd_object_t* ctr) {
    cthulhu_ctr_priv_t* ctr_priv = (cthulhu_ctr_priv_t*) ctr->priv;
    cthulhu_ctr_priv_clean(ctr_priv);
    free(ctr_priv);
    ctr->priv = NULL;
}

/**
 * Get a container with a specific name
 *
 * @param ctr_id the container name
 *
 * @return amxd_object_t* ctr or NULL when not found
 */
amxd_object_t* cthulhu_ctr_get(const char* ctr_id) {
    return amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES ".[" CTHULHU_CONTAINER_ID " == '%s'].", ctr_id);
}

/**
 * @brief get the status of the container as shown in the DM
 * this status can be different from the real status, since the DM does funny things
 * with the restart status
 *
 * @param ctr_obj
 * @return cthulhu_ctr_status_t
 */
cthulhu_ctr_status_t cthulhu_ctr_status_dm_get(amxd_object_t* ctr_obj) {
    cthulhu_ctr_status_t ctr_status = cthulhu_ctr_status_unknown;
    if(!ctr_obj) {
        return ctr_status;
    }
    char* ctr_status_str = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_STATUS, NULL);
    if(!ctr_status_str) {
        return ctr_status;
    }
    ctr_status = cthulhu_string_to_ctr_status(ctr_status_str);
    free(ctr_status_str);
    return ctr_status;
}

/**
 * @brief get the real status of the container
 * this status can be different from the status in the DM, since the DM does funny things
 * with the restart status
 *
 * @param ctr_obj
 * @return cthulhu_ctr_status_t
 */
cthulhu_ctr_status_t cthulhu_ctr_status_real_get(amxd_object_t* ctr_obj) {
    cthulhu_ctr_priv_t* ctr_priv = cthulhu_ctr_get_priv(ctr_obj);
    if(!ctr_priv) {
        return cthulhu_ctr_status_unknown;
    }
    return ctr_priv->real_status;
}

int cthulhu_ctr_status_real_set(amxd_object_t* ctr_obj, cthulhu_ctr_status_t ctr_status) {
    int res = -1;
    cthulhu_ctr_priv_t* ctr_priv = cthulhu_ctr_get_priv(ctr_obj);
    if(!ctr_priv) {
        goto exit;
    }
    ctr_priv->real_status = ctr_status;
    res = 0;
exit:
    return res;
}

static void cthulhu_ctr_priv_sb_stop(amxd_object_t* ctr_obj, cthulhu_notif_data_t* notif_data) {
    char* priv_sb_id = NULL;
    cthulhu_notif_data_t* notif_data_no_id = NULL;
    cthulhu_action_res_t res;
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);
    // start the private sandbox of the container
    priv_sb_id = cthulhu_ctr_get_priv_sb_id(ctr_obj);
    if(string_is_empty(priv_sb_id)) {
        SAH_TRACEZ_ERROR(ME, "CTR: priv_sb_id is empty");
        goto exit;
    }
    if((res = cthulhu_sb_stop(priv_sb_id, notif_data_no_id)) != cthulhu_action_res_done) {
        SAH_TRACEZ_ERROR(ME, "CTR: Expected state 'done' instead of %d", res);
    }
exit:
    free(priv_sb_id);
    cthulhu_notif_data_delete(&notif_data_no_id);
}

static void cthulhu_ctr_priv_sb_remove(amxd_object_t* ctr_obj, cthulhu_notif_data_t* notif_data) {
    char* priv_sb_id = NULL;
    cthulhu_notif_data_t* notif_data_no_id = NULL;
    cthulhu_action_res_t res;
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);
    // start the private sandbox of the container
    priv_sb_id = cthulhu_ctr_get_priv_sb_id(ctr_obj);
    if(string_is_empty(priv_sb_id)) {
        SAH_TRACEZ_ERROR(ME, "CTR: priv_sb_id is empty");
        goto exit;
    }
    if((res = cthulhu_sb_remove(priv_sb_id, notif_data_no_id)) != cthulhu_action_res_done) {
        SAH_TRACEZ_ERROR(ME, "CTR: Expected state 'done' instead of %d", res);
    }
exit:
    free(priv_sb_id);
    cthulhu_notif_data_delete(&notif_data_no_id);
}


/**
 * @brief Callback to continue stopping when the container has stopped
 *
 * @param sig_name
 * @param data
 * @param priv
 */
static void cthulhu_ctr_start_cb(UNUSED const char* const sig_name,
                                 UNUSED const amxc_var_t* const data,
                                 void* const priv) {
    amxd_object_t* ctr_obj = NULL;
    amxp_signal_mngr_t* sigmngr = NULL;
    if(!priv) {
        SAH_TRACEZ_ERROR(ME, "No private data");
        return;
    }
    cb_data_ctr_t* cb_data = (cb_data_ctr_t*) priv;
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Callback to start container", cb_data->ctr_id);
    amxc_set_remove_flag(ctr_start_cb_underway_set, cb_data->ctr_id);

    ctr_obj = cthulhu_ctr_get(cb_data->ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: does not exist anymore", cb_data->ctr_id);
    }

    // remove the callback
    sigmngr = cthulhu_ctr_get_sigmngr(cb_data->ctr_id);
    if(sigmngr) {
        amxp_slot_disconnect(sigmngr, SIG_RUNNING, cthulhu_ctr_start_cb);
        amxp_slot_disconnect(sigmngr, SIG_RUNNING_CANCELED, cthulhu_ctr_start_canceled_cb);
    }
    // notify
    // NOTE replace here with cthulhu_notif_ctr_started if this would ever be necessary
    if(cb_data->notif_data && ctr_obj) {
        cb_data->notif_data->obj = ctr_obj;
        cthulhu_notif_ctr_updated(cb_data->notif_data, NULL);
    }
    cthulhu_plugin_ctr_poststart(cb_data->ctr_id);

    cthulhu_notif_data_delete(&cb_data->notif_data);
    free(cb_data->ctr_id);
    free(cb_data);
}

/**
 * @brief Callback to continue stopping when the container has stopped
 *
 * @param sig_name
 * @param data
 * @param priv
 */
static void cthulhu_ctr_start_canceled_cb(UNUSED const char* const sig_name,
                                          UNUSED const amxc_var_t* const data,
                                          void* const priv) {
    amxd_object_t* ctr_obj = NULL;
    amxp_signal_mngr_t* sigmngr = NULL;
    if(!priv) {
        SAH_TRACEZ_ERROR(ME, "No private data");
        return;
    }
    cb_data_ctr_t* cb_data = (cb_data_ctr_t*) priv;

    SAH_TRACEZ_INFO(ME, "CTR[%s]: Callback to start cancel container", cb_data->ctr_id);
    amxc_set_remove_flag(ctr_start_cb_underway_set, cb_data->ctr_id);

    ctr_obj = cthulhu_ctr_get(cb_data->ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: does not exist anymore", cb_data->ctr_id);
    }

    // remove the callback
    sigmngr = cthulhu_ctr_get_sigmngr(cb_data->ctr_id);
    if(sigmngr) {
        amxp_slot_disconnect(sigmngr, SIG_RUNNING, cthulhu_ctr_start_cb);
        amxp_slot_disconnect(sigmngr, SIG_RUNNING_CANCELED, cthulhu_ctr_start_canceled_cb);
    }

    cthulhu_notif_data_delete(&cb_data->notif_data);
    free(cb_data->ctr_id);
    free(cb_data);
}

/**
 * @brief add data related to mounts to the start_data
 *
 * the start_data structure is passed to the container backend (lxc, ...), and the backend needs
 * this information in order to mount the requested objects in the container filesystem
 *
 * @param ctr_id
 * @param ctr_obj
 * @param start_data
 * @return int
 */
static int cthulhu_ctr_add_mount_data(const char* const ctr_id, amxd_object_t* ctr_obj, cthulhu_ctr_start_data_t* start_data) {
    int res = -1;
    amxd_object_t* mount_templ = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_MOUNTS);
    if(!mount_templ) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get the mounts", ctr_id);
        goto exit;
    }
    // go over all instances
    amxd_object_for_each(instance, it_mnt, mount_templ) {
        amxd_object_t* instance = amxc_llist_it_get_data(it_mnt, amxd_object_t, it);
        char* source = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_MOUNTS_SOURCE, NULL);
        char* destination = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_MOUNTS_DESTINATION, NULL);
        char* type = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_MOUNTS_TYPE, NULL);
        char* options = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_MOUNTS_OPTIONS, NULL);
        if(cthulhu_ctr_start_data_add_mount_data(start_data, source, destination, type, options) < 0) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to add mountpoint to the start_data", ctr_id);
            free(source);
            free(destination);
            free(type);
            free(options);
            goto exit;
        }
        free(source);
        free(destination);
        free(type);
        free(options);
    }
    res = 0;
exit:
    return res;
}

/**
 * @brief add data related to unprivileged configuration to the start_data
 *
 * if the container should run unprivileged, this information is needed by the backend to
 * configure user mappings between the host and the container
 *
 * @param ctr_id
 * @param start_data
 * @return int
 */
static int cthulhu_ctr_add_unprivileged_data(const char* const ctr_id, cthulhu_ctr_start_data_t* start_data) {
    int res = -1;
    bool change_ownership = false;
    uid_t uid = 0;
    gid_t gid = 0;

    ASSERT_SUCCESS(cthulhu_unpriv_get_settings(ctr_id, &change_ownership, &uid, &gid), goto exit);
    if(change_ownership) {
        ASSERT_SUCCESS(cthulhu_ctr_start_data_add_usermapping_data(start_data, cthulhu_usermapping_user, 0, uid, 1), goto exit);
        ASSERT_SUCCESS(cthulhu_ctr_start_data_add_usermapping_data(start_data, cthulhu_usermapping_group, 0, gid, 1), goto  exit);
        uid_t uid_map = 0;
        gid_t gid_map = 0;
        uint32_t range = 0;
        cthulhu_unpriv_get_user_subids(ctr_id, &uid_map, &range);
        if(range) {
            ASSERT_SUCCESS(cthulhu_ctr_start_data_add_usermapping_data(start_data, cthulhu_usermapping_user, 1, uid_map, range), goto exit);
        }
        cthulhu_unpriv_get_group_subids(ctr_id, &gid_map, &range);
        if(range) {
            ASSERT_SUCCESS(cthulhu_ctr_start_data_add_usermapping_data(start_data, cthulhu_usermapping_group, 1, gid_map, range), goto exit);
        }
    }
    start_data->run_user_id = uid;
    start_data->run_group_id = gid;

    res = 0;
exit:
    return res;
}

static int cthulhu_ctr_add_device_data(const char* const sb_id, cthulhu_ctr_start_data_t* start_data) {
    int res = -1;
    amxd_object_t* sb_obj;
    amxd_object_t* devices_templ = NULL;
    char* parent_sb_id = NULL;

    ASSERT_NOT_NULL(sb_id, goto exit);
    ASSERT_NOT_NULL(start_data, goto exit);

    sb_obj = cthulhu_sb_get(sb_id);
    if(!sb_obj) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Sandbox does not exist", sb_id);
        goto exit;
    }

    devices_templ = amxd_object_get(sb_obj, CTHULHU_SANDBOX_DEVICES);
    if(!devices_templ) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get devices", sb_id);
        goto exit;
    }

    amxd_object_for_each(instance, it_mnt, devices_templ) {
        amxd_object_t* instance = amxc_llist_it_get_data(it_mnt, amxd_object_t, it);
        bool err_in_loop = false;
        char* permission = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_DEVICES_PERMISSION, NULL);

        if((strcmp(permission, "Allow") != 0)) {
            free(permission);
            continue;
        }

        char* device = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_DEVICES_DEVICE, NULL);
        char* type = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_DEVICES_TYPE, NULL);
        int32_t major = amxd_object_get_int32_t(instance, CTHULHU_SANDBOX_DEVICES_MAJOR, NULL);
        int32_t minor = amxd_object_get_int32_t(instance, CTHULHU_SANDBOX_DEVICES_MINOR, NULL);
        bool create = amxd_object_get_bool(instance, CTHULHU_SANDBOX_DEVICES_CREATE, NULL);

        if(strlen(type) <= 0) {
            SAH_TRACEZ_ERROR(ME, "SB[%s]: Device type is not defined", sb_id);
            goto end_loop;
        }

        if(cthulhu_ctr_start_data_add_device_data(start_data, device, major, minor, type[0], create) < 0) {
            SAH_TRACEZ_ERROR(ME, "SB[%s]: Failed to add device [%s] to start_data", sb_id, device);
            goto end_loop;
        }
end_loop:
        free(permission);
        free(device);
        free(type);
        if(err_in_loop) {
            goto exit;
        }
    }
    res = 0;
    parent_sb_id = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_PARENT, NULL);
    if(!string_is_empty(parent_sb_id)) {
        res = cthulhu_ctr_add_device_data(parent_sb_id, start_data);
    }

exit:
    free(parent_sb_id);
    return res;
}

/**
 * @brief Start all containers of a sandbox
 *
 * @param sb_id
 * @return int 0 on success, -1 on failure
 */
static cthulhu_ctr_start_data_t* cthulhu_ctr_get_start_data(amxd_object_t* const ctr_obj) {
    cthulhu_ctr_start_data_t* start_data = NULL;
    char* priv_sb_id = NULL;
    char* ctr_id = NULL;
    amxd_object_t* environmentvariables_templ = NULL;
    amxd_object_t* environmentvariables_oci_templ = NULL;

    ASSERT_SUCCESS(cthulhu_ctr_start_data_new(&start_data), goto exit);
    ctr_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_ID, NULL);
    ASSERT_NOT_NULL(ctr_id, goto error);
    priv_sb_id = cthulhu_ctr_get_priv_sb_id(ctr_obj);
    ASSERT_NOT_NULL(priv_sb_id, goto error);

    start_data->sb_data = cthulhu_create_sb_data(priv_sb_id);
    if(!start_data->sb_data) {
        goto error;
    }

    // Add mount data
    if(cthulhu_ctr_add_mount_data(ctr_id, ctr_obj, start_data) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to add mounts to container data", ctr_id);
        goto error;
    }
    // Add environment variables data
    environmentvariables_templ = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES);
    if(!environmentvariables_templ) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get the environment variables", ctr_id);
        goto exit;
    }
    // Add environment variables data from OCI layer
    environmentvariables_oci_templ = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI);
    if(!environmentvariables_oci_templ) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get the container environment variables", ctr_id);
        goto exit;
    }
    // Add EnvironmentVariables data
    amxd_object_for_each(instance, it_environmentvariables, environmentvariables_templ) {
        amxd_object_t* instance = amxc_llist_it_get_data(it_environmentvariables, amxd_object_t, it);
        char* key = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_KEY, NULL);
        char* value = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_VALUE, NULL);
        if(cthulhu_ctr_start_data_add_envvar_data(start_data, key, value) < 0) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to add envvar point to the start_data", ctr_id);
            free(key);
            free(value);
            goto error;
        }
        free(key);
        free(value);
    }
    // Add OCI EnvironmentVariables data
    amxd_object_for_each(instance, it_environmentvariables, environmentvariables_oci_templ) {
        amxd_object_t* instance = amxc_llist_it_get_data(it_environmentvariables, amxd_object_t, it);
        char* key = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI_KEY, NULL);
        char* value = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI_VALUE, NULL);
        if(cthulhu_ctr_start_data_add_envvar_data(start_data, key, value) < 0) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to add container envvar point to the start_data", ctr_id);
            free(key);
            free(value);
            goto error;
        }
        free(key);
        free(value);
    }
    amxc_llist_for_each(it, &start_data->envvar) {
        cthulhu_envvar_data_t* envvar_data = amxc_llist_it_get_data(it, cthulhu_envvar_data_t, lit);
        SAH_TRACEZ_INFO(ME, "Values inside start_data DM - key: %s, value: %s", envvar_data->key, envvar_data->value);
    }
    // Add devices data
    if(cthulhu_ctr_add_device_data(priv_sb_id, start_data) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to add devices to container data", ctr_id);
        goto error;
    }
    // Add unprivileged data
    if(cthulhu_ctr_add_unprivileged_data(ctr_id, start_data) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to add unprivileged to container data", ctr_id);
        goto error;
    }
    goto exit;
error:
    cthulhu_ctr_start_data_delete(&start_data);
exit:
    free(ctr_id);
    free(priv_sb_id);
    return start_data;
}

/**
 * @brief create directories or files in the rootfs so they can be mounted
 *
 * when hostobjects need to be mounted in the container, there needs to be
 * a mount point in the rootfs to mount too. Those mount points will be
 * created here if needed.
 *
 * @param ctr_id
 * @return int
 */
static int cthulhu_create_hostobject_stubs(const char* ctr_id,
                                           cthulhu_ctr_start_data_t* start_data,
                                           uid_t uid, gid_t gid) {
    int res = -1;
    amxd_object_t* ctr_obj = NULL;
    char* rootfs = NULL;
    struct stat sb;

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit);

    rootfs = cthulu_overlayfs_get_rootfs(ctr_id);

    amxc_llist_for_each(it, &start_data->mounts) {
        cthulhu_mount_data_t* mount_data = amxc_llist_it_get_data(it, cthulhu_mount_data_t, lit);
        ASSERT_STR_NOT_EMPTY(mount_data->source, continue);
        ASSERT_STR_NOT_EMPTY(mount_data->destination, continue);
        // if a plugin does not fill in the type, we asume it is a mount
        if(!mount_data->type) {
            mount_data->type = strdup("mount");
        }

        char* dest_full = NULL;
        char* dir = NULL;

        // if destination exists, do nothing
        if(asprintf(&dest_full, "%s/%s", rootfs, mount_data->destination) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not allocate string");
            goto ctu_loop;
        }

        if(stat(dest_full, &sb) == 0) {
            goto ctu_loop;
        }
        // if type is tmpfs or if source is a dir, create a dir
        if((strcmp(mount_data->type, "tmpfs") == 0) || cthulhu_isdir(mount_data->source)) {
            SAH_TRACEZ_INFO(ME, "CTR[%s]: Create directory [%s]", ctr_id, mount_data->destination);
            ASSERT_SUCCESS(cthulhu_mkdir_ext(dest_full, uid, gid, 0755, true), goto ctu_loop, "Could not create dir [%s]", dest_full);
            goto ctu_loop;
        }
        // if source is file, create the dir and then a node
        SAH_TRACEZ_INFO(ME, "CTR[%s]: Create file [%s]", ctr_id, mount_data->destination);
        dir = lcm_get_dirname(dest_full);
        ASSERT_NOT_NULL(dir, goto ctu_loop, "Failed to get dirname for [%s]", dest_full);
        ASSERT_SUCCESS(cthulhu_mkdir_ext(dir, uid, gid, 0755, true), goto ctu_loop, "Could not create dir [%s]", dir);
        if((mknod(dest_full, S_IFREG | 0000, 0) < 0) && (errno != EEXIST)) {
            SAH_TRACEZ_ERROR(ME, "Could not create %s (%d: %s)", dest_full, errno, strerror(errno));
            goto ctu_loop;
        }

ctu_loop:
        free(dest_full);
        free(dir);
    }

exit:
    free(rootfs);
    return res;
}

/**
 * @brief create devices in the rootfs of the container
 *
 * @param ctr_id
 * @return int
 */
static int cthulhu_create_devices(const char* ctr_id, cthulhu_ctr_start_data_t* start_data, uid_t uid, gid_t gid) {
    int res = -1;
    amxd_object_t* ctr_obj = NULL;
    char* rootfs = NULL;
    struct stat sb;

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit);
    rootfs = cthulu_overlayfs_get_rootfs(ctr_id);

    amxc_llist_for_each(it, &start_data->devices) {
        cthulhu_device_data_t* device_data = amxc_llist_it_get_data(it, cthulhu_device_data_t, lit);
        char* dest_full = NULL;
        char* dest_full_cpy = NULL;

        ASSERT_STR_NOT_EMPTY(device_data->device, goto ctu_loop);

        if(!device_data->create) {
            goto ctu_loop;
        }

        if(asprintf(&dest_full, "%s/%s", rootfs, device_data->device) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not allocate string");
            goto ctu_loop;
        }

        // if destination exists, do nothing
        if(stat(dest_full, &sb) == 0) {
            goto ctu_loop;
        }
        dest_full_cpy = strdup(dest_full);
        char* dir = dirname(dest_full_cpy);
        if(!cthulhu_isdir(dir)) {
            cthulhu_mkdir_ext(dir, uid, gid, 0755, true);

        }
        mode_t mode = 0;
        if(device_data->type == 'c') {
            mode |= S_IFCHR;
        } else if(device_data->type == 'b') {
            mode |= S_IFBLK;
        } else {
            SAH_TRACEZ_ERROR(ME, "Unknown type [%c]", device_data->type);
            goto ctu_loop;
        }
        dev_t dev = makedev(device_data->major, device_data->minor);

        SAH_TRACEZ_INFO(ME, "Create device [%s]", device_data->device);
        if(mknod(dest_full, mode, dev) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not create %s (%d: %s)", dest_full, errno, strerror(errno));
            goto ctu_loop;
        }
        if(chmod(dest_full, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not chmod %s (%d: %s)", dest_full, errno, strerror(errno));
        }
        if(chown(dest_full, uid, gid) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not chown %s (%d: %s)", dest_full, errno, strerror(errno));
        }

ctu_loop:
        free(dest_full);
        free(dest_full_cpy);


    }
    res = 0;
exit:
    free(rootfs);
    return res;
}

static tr181_fault_type_t cthulhu_appdata_create(const char* ctr_id, cthulhu_ctr_start_data_t* start_data,
                                                 uid_t uid, gid_t gid) {
    tr181_fault_type_t res = tr181_fault_request_denied;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* sb_obj = NULL;
    amxd_object_t* appdata_list = NULL;
    char* sb_id = NULL;
    amxd_trans_t* trans = NULL;
    amxd_status_t status = amxd_status_ok;
    bool send_update = false;

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit, "CTR[%s] does not exist", ctr_id);
    sb_id = cthulhu_ctr_get_sb_id(ctr_obj);
    sb_obj = cthulhu_sb_get(sb_id);
    ASSERT_NOT_NULL(sb_obj, goto exit, "SB[%s] does not exist", sb_id);
    appdata_list = amxd_object_findf(sb_obj, CTHULHU_SANDBOX_APPLICATIONDATA);
    ASSERT_NOT_NULL(sb_obj, goto exit, "SB[%s]  "CTHULHU_SANDBOX_APPLICATIONDATA " does not exist", sb_id);
    res = tr181_fault_ok;
    amxd_object_for_each(instance, it_appdata, appdata_list) {
        amxd_object_t* instance = amxc_llist_it_get_data(it_appdata, amxd_object_t, it);
        char* appuuid = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_APPLICATIONUUID, NULL);
        // the appuuid is the same as the ctr_id. we are only interested in app_data for this contianer
        if(strcmp(ctr_id, appuuid) != 0) {
            free(appuuid);
            continue;
        }
        char* name = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_NAME, NULL);
        uint32_t capacity = amxd_object_get_uint32_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_CAPACITY, NULL);
        char* str_retain = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_RETAIN, NULL);
        bool retain = !strcmp(str_retain, CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_FOREVER);
        char* accesspath = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_ACCESSPATH, NULL);
        char* mountpoint = NULL;

        res = cthulhu_storage_appdata_mount(sb_id, appuuid, name, capacity, retain, uid, gid, &mountpoint);
        if(res != tr181_fault_ok) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not create appdata [%s]", ctr_id, name);
        } else {
            ASSERT_SUCCESS(cthulhu_ctr_start_data_add_mount_data(start_data, mountpoint, accesspath, NULL, NULL), NO_INSTRUCTION,
                           "Could not add application data to start data object");
        }
        cthulhu_trans_new(&trans, instance);
        amxd_trans_set_cstring_t(trans, CTHULHU_SANDBOX_APPLICATIONDATA_MOUNTPOINT, mountpoint);
        status = cthulhu_trans_send(&trans);
        if(status != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Transaction failed. Status: %d", ctr_id, status);
        }
        send_update = true;
        free(name);
        free(str_retain);
        free(accesspath);
        free(appuuid);
        free(mountpoint);
        if(res != tr181_fault_ok) {
            break;
        }
    }
    if(send_update) {
        cthulhu_notif_data_t* notif_data = NULL;
        cthulhu_notif_data_new(&notif_data, sb_obj, cthulhu_notif_cmd_none, NULL);
        cthulhu_notif_sb_updated(notif_data, NULL);
        cthulhu_notif_data_delete(&notif_data);
    }

exit:
    free(sb_id);
    return res;
}

/**
 * @brief when a container is stopped, the application data should be unmounted and cleaned up
 *
 * @param ctr_id
 * @return int
 */
static int cthulhu_appdata_stop(const char* ctr_id) {
    int res = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* sb_obj = NULL;
    amxd_object_t* appdata_list = NULL;
    char* sb_id = NULL;
    amxd_trans_t* trans = NULL;
    amxd_status_t status = amxd_status_ok;
    bool send_update = false;

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit, "CTR[%s] does not exist", ctr_id);
    sb_id = cthulhu_ctr_get_sb_id(ctr_obj);
    sb_obj = cthulhu_sb_get(sb_id);
    ASSERT_NOT_NULL(sb_obj, goto exit, "SB[%s] does not exist", sb_id);
    appdata_list = amxd_object_findf(sb_obj, CTHULHU_SANDBOX_APPLICATIONDATA);
    ASSERT_NOT_NULL(sb_obj, goto exit, "SB[%s]  "CTHULHU_SANDBOX_APPLICATIONDATA " does not exist", sb_id);
    res = 0;
    amxd_object_for_each(instance, it_appdata, appdata_list) {
        amxd_object_t* instance = amxc_llist_it_get_data(it_appdata, amxd_object_t, it);
        char* appuuid = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_APPLICATIONUUID, NULL);
        // the appuuid is the same as the ctr_id. we are only interested in app_data for this contianer
        if(strcmp(ctr_id, appuuid) != 0) {
            free(appuuid);
            continue;
        }
        char* name = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_NAME, NULL);
        char* mountpoint = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_MOUNTPOINT, NULL);
        char* str_retain = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_RETAIN, NULL);
        bool retain = !strcmp(str_retain, CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_FOREVER);

        if(!STRING_EMPTY(mountpoint)) {
            cthulhu_storage_appdata_umount(mountpoint);
        }
        if(!retain) {
            cthulhu_storage_appdata_remove(sb_id, appuuid, name);
        }
        cthulhu_trans_new(&trans, instance);
        amxd_trans_set_cstring_t(trans, CTHULHU_SANDBOX_APPLICATIONDATA_MOUNTPOINT, "");
        status = cthulhu_trans_send(&trans);
        if(status != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Transaction failed. Status: %d", status);
        }

        free(name);
        free(str_retain);
        free(appuuid);
        free(mountpoint);
        if(res) {
            break;
        }
    }
    if(send_update) {
        cthulhu_notif_data_t* notif_data = NULL;
        cthulhu_notif_data_new(&notif_data, sb_obj, cthulhu_notif_cmd_none, NULL);
        cthulhu_notif_sb_updated(notif_data, NULL);
        cthulhu_notif_data_delete(&notif_data);
    }
exit:
    free(sb_id);
    return res;
}


/**
 * @brief when a container is stopped, the application data should be unmounted and cleaned up
 *
 * @param ctr_id
 * @return int
 */
static int cthulhu_appdata_remove(const char* ctr_id, bool remove_data) {
    int res = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* sb_obj = NULL;
    amxd_object_t* appdata_list = NULL;
    char* sb_id = NULL;
    amxd_trans_t* trans = NULL;
    amxd_status_t status = amxd_status_ok;
    bool send_update = false;

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit, "CTR[%s] does not exist", ctr_id);
    sb_id = cthulhu_ctr_get_sb_id(ctr_obj);
    sb_obj = cthulhu_sb_get(sb_id);
    ASSERT_NOT_NULL(sb_obj, goto exit, "SB[%s] does not exist", sb_id);
    appdata_list = amxd_object_findf(sb_obj, CTHULHU_SANDBOX_APPLICATIONDATA);
    ASSERT_NOT_NULL(sb_obj, goto exit, "SB[%s]  "CTHULHU_SANDBOX_APPLICATIONDATA " does not exist", sb_id);
    res = 0;
    amxd_object_for_each(instance, it_appdata, appdata_list) {
        amxd_object_t* instance = amxc_llist_it_get_data(it_appdata, amxd_object_t, it);
        char* appuuid = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_APPLICATIONUUID, NULL);
        // the appuuid is the same as the ctr_id. we are only interested in app_data for this contianer
        if(strcmp(ctr_id, appuuid) != 0) {
            free(appuuid);
            continue;
        }
        char* name = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_NAME, NULL);
        char* mountpoint = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_MOUNTPOINT, NULL);
        char* str_retain = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_RETAIN, NULL);
        bool retain = !strcmp(str_retain, CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_FOREVER);


        if(retain && !remove_data) {
            SAH_TRACEZ_NOTICE(ME, "Keep ApplicationData [%s]", name);
            cthulhu_trans_new(&trans, instance);
            amxd_trans_set_cstring_t(trans, CTHULHU_SANDBOX_APPLICATIONDATA_MOUNTPOINT, "");
            status = cthulhu_trans_send(&trans);
            if(status != amxd_status_ok) {
                SAH_TRACEZ_ERROR(ME, "Transaction failed. Status: %d", status);
            }
        } else {
            cthulhu_storage_appdata_remove(sb_id, appuuid, name);
            amxd_object_emit_del_inst(instance);
            amxd_object_delete(&instance);
            send_update = true;
        }

        free(name);
        free(str_retain);
        free(appuuid);
        free(mountpoint);
        if(res) {
            break;
        }
    }
    if(send_update) {
        cthulhu_notif_data_t* notif_data = NULL;
        cthulhu_notif_data_new(&notif_data, sb_obj, cthulhu_notif_cmd_none, NULL);
        cthulhu_notif_sb_updated(notif_data, NULL);
        cthulhu_notif_data_delete(&notif_data);
    }
exit:
    free(sb_id);
    return res;
}

cthulhu_action_res_t cthulhu_ctr_start(const char* ctr_id, cthulhu_notif_data_t* notif_data) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Starting container", ctr_id);
    amxc_var_t args;
    amxc_var_t ret;
    char* sb_id = NULL;
    char* priv_sb_id = NULL;
    amxd_object_t* ctr_obj = NULL;
    cthulhu_ctr_status_t ctr_status = cthulhu_ctr_status_unknown;
    cthulhu_ctr_status_t status_in_dm = cthulhu_ctr_status_unknown;
    cthulhu_ctr_start_data_t* start_data = NULL;
    cthulhu_notif_data_t* notif_data_no_id = NULL;
    cthulhu_app_t* app_data = cthulhu_get_app_data();
    amxd_object_t* cthulhu_config = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONFIG ".");

    char* rootfs = NULL;
    amxc_var_init(&args);
    amxc_var_init(&ret);

    // check the backend
    if(backend == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "No backend is loaded");
        goto exit;
    }

    if(!app_data) {
        SAH_TRACEZ_ERROR(ME, "App data is NULL");
        goto exit;
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Container %s does not exist", ctr_id);
        goto exit;
    }
    // check if the container is already running
    ctr_status = cthulhu_ctr_status_real_get(ctr_obj);
    if((ctr_status == cthulhu_ctr_status_starting) ||
       ( ctr_status == cthulhu_ctr_status_running)) {
        SAH_TRACEZ_INFO(ME, "Container %s already %s", ctr_id, cthulhu_ctr_status_to_string(ctr_status));
        res = cthulhu_action_res_done;
        goto exit;
    }
    // add entries to subuid and subgid
    uid_t uid = 0;
    gid_t gid = 0;
    bool change_ownership = false;
    ASSERT_SUCCESS(cthulhu_unpriv_get_settings(ctr_id, &change_ownership, &uid, &gid), goto exit);
    if(change_ownership) {
        cthulhu_unpriv_add_subuid(0, uid, 1);
        cthulhu_unpriv_add_subgid(0, gid, 1);
    }
    // create the sandbox data for the backend
    sb_id = cthulhu_ctr_get_sb_id(ctr_obj);
    if(sb_id) {
        if(!cthulhu_is_sb_enabled(sb_id)) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_du_state, "EE [%s] is disabled. Container [%s] will not be started", sb_id, ctr_id);
            goto exit;
        }
    }
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);

    // start the private sandbox of the container
    priv_sb_id = cthulhu_ctr_get_priv_sb_id(ctr_obj);
    if(cthulhu_sb_start(priv_sb_id, notif_data_no_id) != cthulhu_action_res_done) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_du_state, "Private SB [%s] cannot be started. Container [%s] will not be started", priv_sb_id, ctr_id);
        goto exit;
    }

    start_data = cthulhu_ctr_get_start_data(ctr_obj);
    if(!start_data) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_du_state, "EE [%s] has no valid data. Container [%s] will not be started", priv_sb_id, ctr_id);
        goto exit;
    }
    if(cthulhu_syslogng_enable(ctr_id, start_data) < 0) {
        // do continue when syslogng fails to start
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to configure syslog-ng for this container", ctr_id);
    }

    cthulhu_unpriv_ctr_pre_mount(ctr_id);

    if(!cthulhu_overlayfs_on_create()) {
        // setup overlayfs
        if(cthulhu_overlayfs_supported()) {
            const char* data_dir = cthulhu_storage_get_dir(priv_sb_id);
            cthulhu_overlayfs_create_rootfs(ctr_id, data_dir);
            if(uid || gid) {
                // fix the ownership of overlayfs files in case a container is removed and added again with option RetainData
                // if the unprivileged user changed, the file ownership needs to be changed of the overlayfs upperdir
                cthulhu_overlayfs_fix_ownership(ctr_id, data_dir, uid, gid);
            }
        }
    }
    rootfs = cthulu_overlayfs_get_rootfs(ctr_id);

    // set and create the data dir
    if(asprintf(&(start_data->host_data_dir), "%s/%s", app_data->ctr_host_data_location, ctr_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to allocate string");
        goto exit;
    }
    ASSERT_SUCCESS(cthulhu_mkdir(start_data->host_data_dir, true), goto exit);

    cthulhu_plugin_ctr_prestart(ctr_id, rootfs, start_data);
    tr181_fault_type_t tr181_res = cthulhu_appdata_create(ctr_id, start_data, uid, gid);
    if(tr181_res != tr181_fault_ok) {
        NOTIF_FAILED(notif_data, tr181_res, "Container [%s] could not create ApplicationData", ctr_id);
        goto exit;
    }
    // keep create_hostobject_stubs and create_devices after plugin_ctr_prestart
    // so hostobjects that are added in the plugins will also be created
    cthulhu_create_hostobject_stubs(ctr_id, start_data, uid, gid);
    cthulhu_create_devices(ctr_id, start_data, uid, gid);
    if(change_ownership) {
        char* capabilities = amxd_object_get_cstring_t(cthulhu_config, CTHULHU_CONFIG_UNPRIVCAPABILITIES, NULL);
        if(!string_is_empty(capabilities)) {
            SAH_TRACEZ_INFO(ME, "CTR[%s]: Apply capabilities [%s]", ctr_id, capabilities);
            // container is unprivileged, so drop all capabilities
            start_data->capabilities = strdup(capabilities);
        } else {
            SAH_TRACEZ_INFO(ME, "CTR[%s]: Apply capabilities [none]", ctr_id);
            start_data->capabilities = strdup("none");
        }
        free(capabilities);
    }

    // set a callback to send a message
    if(amxc_set_has_flag(ctr_start_cb_underway_set, ctr_id)) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: a callback is already registered", ctr_id);
    } else {
        cb_data_ctr_t* cb_data = (cb_data_ctr_t*) calloc(1, sizeof(cb_data_ctr_t));
        cb_data->ctr_id = strdup(ctr_id);
        cb_data->notif_data = cthulhu_notif_data_no_obj(notif_data);
        amxp_signal_mngr_t* sigmngr = cthulhu_ctr_get_sigmngr(ctr_id);
        if(sigmngr) {
            amxc_set_add_flag(ctr_start_cb_underway_set, ctr_id);
            amxp_slot_connect(sigmngr,
                              SIG_RUNNING,
                              NULL, cthulhu_ctr_start_cb, cb_data);
            amxp_slot_connect(sigmngr,
                              SIG_RUNNING_CANCELED,
                              NULL, cthulhu_ctr_start_canceled_cb, cb_data);
        } else {
            cthulhu_notif_data_delete(&cb_data->notif_data);
            free(cb_data->ctr_id);
            free(cb_data);
        }
    }

    // request backend to start
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_START_CTRID, ctr_id);
    amxc_var_add_new_key_cthulhu_ctr_start_data_t(&args, CTHULHU_PLUGIN_START_DATA, start_data);

    status_in_dm = cthulhu_ctr_status_dm_get(ctr_obj);
    if(status_in_dm != cthulhu_ctr_status_restarting) {
        amxc_var_t params;
        amxc_var_init(&params);
        amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
        amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_STATUS, cthulhu_ctr_status_to_string(cthulhu_ctr_status_starting));
        amxc_var_add_new_key_bool(&params, CTHULHU_CONTAINER_LASTREQSTATEACTIVE, true);
        cthulhu_ctr_update_dm(ctr_id, &params);
        amxc_var_clean(&params);
    }
    cthulhu_ctr_status_real_set(ctr_obj, cthulhu_ctr_status_starting);

    if(amxm_so_execute_function(backend, MOD_CTHULHU, CTHULHU_BACKEND_CMD_START, &args, &ret) < 0) {
        goto exit;
    }

    res = cthulhu_action_res_done;
exit:
    free(sb_id);
    free(priv_sb_id);
    free(rootfs);
    cthulhu_ctr_start_data_delete(&start_data);
    cthulhu_notif_data_delete(&notif_data_no_id);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return res;
}

static cthulhu_action_res_t cthulhu_ctr_stop_priv(const char* ctr_id, cthulhu_notif_data_t* notif_data) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* ctr_obj = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    cthulhu_ctr_status_t ctr_status = cthulhu_ctr_status_unknown;
    cthulhu_ctr_status_t status_in_dm = cthulhu_ctr_status_unknown;
    amxd_dm_t* dm = cthulhu_get_dm();
    amxd_object_t* cthulhu_config = amxd_dm_findf(dm, CTHULHU_DM_CONFIG ".");
    amxc_var_init(&args);
    amxc_var_init(&ret);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Container %s does not exist", ctr_id);
        goto exit;
    }

    ctr_status = cthulhu_ctr_status_real_get(ctr_obj);
    if(ctr_status == cthulhu_ctr_status_ghost) {
        SAH_TRACEZ_INFO(ME, "CTR[%s]: Container is a Ghost", ctr_id);
        res = cthulhu_action_res_done;
        goto exit;
    }

    if(ctr_status == cthulhu_ctr_status_stopped) {
        SAH_TRACEZ_INFO(ME, "CTR[%s]: Container already %s", ctr_id, cthulhu_ctr_status_to_string(ctr_status));
        res = cthulhu_action_res_done;
        goto exit;
    } else if(ctr_status == cthulhu_ctr_status_stopping) {
        SAH_TRACEZ_INFO(ME, "CTR[%s]: Container already %s", ctr_id, cthulhu_ctr_status_to_string(ctr_status));
        res = cthulhu_action_res_busy;
        goto exit;
    }

    cthulhu_plugin_ctr_prestop(ctr_id);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    status_in_dm = cthulhu_ctr_status_dm_get(ctr_obj);
    if(status_in_dm != cthulhu_ctr_status_restarting) {
        amxc_var_t params;
        amxc_var_init(&params);
        amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
        amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_STATUS, cthulhu_ctr_status_to_string(cthulhu_ctr_status_stopping));
        cthulhu_ctr_update_dm(ctr_id, &params);
        amxc_var_clean(&params);
    }
    cthulhu_ctr_status_real_set(ctr_obj, cthulhu_ctr_status_stopping);

    const uint32_t shutdown_timeout = amxd_object_get_uint32_t(cthulhu_config, CTHULHU_CONFIG_GRACEFUL_SHUTDOWN_TIMEOUT, NULL);
    // Don't bail out if this add fails: maybe the backend can still do something
    // without stop data.
    amxc_var_add_key(uint32_t, &args, CTHULHU_PLUGIN_STOP_DATA_TIMEOUT, shutdown_timeout);

    if(amxm_so_execute_function(backend, MOD_CTHULHU, CTHULHU_BACKEND_CMD_STOP, &args, &ret) < 0) {
        goto exit;
    }

    res = cthulhu_action_res_busy;
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return res;
}

/**
 * @brief Callback to continue stopping when the container has stopped
 *
 * @param sig_name
 * @param data
 * @param priv
 */
static void cthulhu_ctr_stop_cb(UNUSED const char* const sig_name,
                                UNUSED const amxc_var_t* const data,
                                void* const priv) {
    amxd_object_t* ctr_obj = NULL;
    amxp_signal_mngr_t* sigmngr = NULL;
    if(!priv) {
        SAH_TRACEZ_ERROR(ME, "No private data");
        return;
    }
    cb_data_ctr_t* cb_data = (cb_data_ctr_t*) priv;
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Callback to stop sandbox", cb_data->ctr_id);
    ctr_obj = cthulhu_ctr_get(cb_data->ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: does not exist anymore", cb_data->ctr_id);
        goto exit;
    }

    // remove the callback
    sigmngr = cthulhu_ctr_get_sigmngr(cb_data->ctr_id);
    if(sigmngr) {
        amxp_slot_disconnect(sigmngr, SIG_STOPPED, cthulhu_ctr_stop_cb);
    }

    cthulhu_plugin_ctr_poststop(cb_data->ctr_id);
    // continue stopping the container
    cthulhu_ctr_stop(cb_data->ctr_id, cb_data->notif_data);
exit:
    cthulhu_notif_data_delete(&cb_data->notif_data);
    free(cb_data->ctr_id);
    free(cb_data);
}

cthulhu_action_res_t cthulhu_ctr_stop(const char* ctr_id, cthulhu_notif_data_t* notif_data) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* ctr_obj = NULL;

    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_init(&args);
    amxc_var_init(&ret);

    SAH_TRACEZ_INFO(ME, "CTR[%s]: Stopping container", ctr_id);

    if(backend == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "No backend is loaded");
        goto exit;
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Container %s does not exist", ctr_id);
        goto exit;
    }
    if(notif_data) {
        notif_data->obj = ctr_obj;
    }

    res = cthulhu_ctr_stop_priv(ctr_id, notif_data);
    if(res == cthulhu_action_res_error) {
        goto exit;
    }
    if(res == cthulhu_action_res_busy) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Container is not stopped yet", ctr_id);
        cb_data_ctr_t* cb_data = (cb_data_ctr_t*) calloc(1, sizeof(cb_data_ctr_t));
        cb_data->ctr_id = strdup(ctr_id);
        cb_data->notif_data = cthulhu_notif_data_no_obj(notif_data);
        amxp_signal_mngr_t* sigmngr = cthulhu_ctr_get_sigmngr(ctr_id);
        if(sigmngr) {
            amxp_sigmngr_trigger_signal(sigmngr, SIG_RUNNING_CANCELED, NULL);
            amxp_slot_connect(sigmngr,
                              SIG_STOPPED,
                              NULL, cthulhu_ctr_stop_cb, cb_data);
        } else {
            cthulhu_notif_data_delete(&cb_data->notif_data);
            free(cb_data->ctr_id);
            free(cb_data);
        }
        goto exit;
    }
    cthulhu_ctr_priv_sb_stop(ctr_obj, notif_data);
    cthulhu_syslogng_disable(ctr_id);
    cthulhu_appdata_stop(ctr_id);
    cthulhu_notif_ctr_updated(notif_data, NULL);
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return res;
}

typedef struct _cb_data_sb {
    char* ctr_id;
    cthulhu_notif_data_t* notif_data;
} cb_data_sb_t;

static void cthulhu_ctr_restart_cb(UNUSED const char* const sig_name,
                                   UNUSED const amxc_var_t* const data,
                                   void* const priv) {
    if(!priv) {
        SAH_TRACEZ_ERROR(ME, "No private data");
        return;
    }
    cb_data_sb_t* cb_data = (cb_data_sb_t*) priv;
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Callback to restart container", cb_data->ctr_id);
    // remove the callback
    amxp_signal_mngr_t* sigmngr = cthulhu_sigmngrs_get(&ctr_sigmngrs, cb_data->ctr_id);
    if(sigmngr) {
        amxp_slot_disconnect(sigmngr, SIG_STOPPED, cthulhu_ctr_restart_cb);
    }
    // restart the container
    cthulhu_ctr_restart(cb_data->ctr_id, cb_data->notif_data);

    cthulhu_notif_data_delete(&cb_data->notif_data);
    free(cb_data->ctr_id);
    free(cb_data);
}

cthulhu_action_res_t cthulhu_ctr_restart(const char* ctr_id, cthulhu_notif_data_t* notif_data) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* ctr_obj = NULL;
    cthulhu_notif_data_t* notif_data_no_id = NULL;

    if(backend == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "No backend is loaded");
        goto exit;
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "CTR[%s]: Container does not exist", ctr_id);
        goto exit;
    }
    if(notif_data) {
        notif_data->obj = ctr_obj;
    }
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);

    res = cthulhu_ctr_stop(ctr_id, notif_data_no_id);
    if(res == cthulhu_action_res_error) {
        goto exit;
    }
    if(res == cthulhu_action_res_busy) {
        cthulhu_ctr_priv_t* priv = cthulhu_ctr_get_priv(ctr_obj);
        if(!priv) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Container does not have private data!", ctr_id);
            goto exit;
        }
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Container will be restarted after it has stopped", ctr_id);
        cb_data_sb_t* cb_data = (cb_data_sb_t*) calloc(1, sizeof(cb_data_sb_t));
        cb_data->ctr_id = strdup(ctr_id);
        cb_data->notif_data = cthulhu_notif_data_no_obj(notif_data);

        amxp_signal_mngr_t* sigmngr = cthulhu_sigmngrs_get(&ctr_sigmngrs, ctr_id);
        if(sigmngr) {
            amxp_slot_connect(sigmngr,
                              SIG_STOPPED,
                              NULL, cthulhu_ctr_restart_cb, cb_data);
        } else {
            cthulhu_notif_data_delete(&cb_data->notif_data);
            free(cb_data->ctr_id);
            free(cb_data);
        }
        goto exit;
    }

    // restart when the container is stopped
    res = cthulhu_ctr_start(ctr_id, notif_data);
    if(res != 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Failed to start container");
    }
exit:
    cthulhu_notif_data_delete(&notif_data_no_id);
    return res;
}

typedef struct _cb_data_update {
    char* ctr_id;
    cthulhu_notif_data_t* notif_data;
    char* linked_uuid;
    char* bundle;
    char* bundle_version;
    char* disk_location;
    char* sb_id;
    bool remove_data;
    amxc_var_t* meta_data;
    amxc_ts_t* ts_created;
    char* module_version;
} cb_data_update_t;

static void cthulhu_data_update_delete(cb_data_update_t** cb_data) {
    ASSERT_NOT_NULL(cb_data, goto exit);
    ASSERT_NOT_NULL(*cb_data, goto exit);

    cthulhu_notif_data_delete(&(*cb_data)->notif_data);
    free((*cb_data)->ctr_id);
    free((*cb_data)->linked_uuid);
    free((*cb_data)->bundle);
    free((*cb_data)->bundle_version);
    free((*cb_data)->disk_location);
    free((*cb_data)->sb_id);
    free((*cb_data)->ts_created);
    amxc_var_delete(&(*cb_data)->meta_data);
    free((*cb_data)->module_version);
    free((*cb_data));
    *cb_data = NULL;

exit:
    return;
}

static cthulhu_action_res_t cthulhu_ctr_update_ctu(const char* ctr_id,
                                                   const char* linked_uuid,
                                                   const char* bundle,
                                                   const char* bundle_version,
                                                   const char* disk_location,
                                                   const char* sb_id,
                                                   cthulhu_notif_data_t* notif_data,
                                                   const amxc_var_t* const metadata,
                                                   amxc_ts_t* ts_created,
                                                   const char* module_version);

static void cthulhu_ctr_update_cb(UNUSED const char* const sig_name,
                                  UNUSED const amxc_var_t* const data,
                                  void* const priv) {
    cb_data_update_t* cb_data = (cb_data_update_t*) priv;
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Callback to update container", cb_data->ctr_id);
    // remove the callback
    amxp_signal_mngr_t* sigmngr = cthulhu_sigmngrs_get(&ctr_sigmngrs, cb_data->ctr_id);
    if(sigmngr) {
        amxp_slot_disconnect(sigmngr, SIG_REMOVED, cthulhu_ctr_update_cb);
    }
    // create the new container
    cthulhu_ctr_update_ctu(cb_data->ctr_id, cb_data->linked_uuid, cb_data->bundle,
                           cb_data->bundle_version, cb_data->disk_location, cb_data->sb_id,
                           cb_data->notif_data, cb_data->meta_data,
                           cb_data->ts_created, cb_data->module_version);

    cthulhu_data_update_delete(&cb_data);
}

cthulhu_action_res_t cthulhu_ctr_update(const char* ctr_id, const char* linked_uuid, const char* bundle,
                                        const char* bundle_version, const char* disklocation,
                                        const char* sb_id, bool remove_data,
                                        cthulhu_notif_data_t* notif_data, const amxc_var_t* const metadata,
                                        amxc_ts_t* ts_created, const char* module_version) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* ctr_obj = NULL;
    cthulhu_notif_data_t* notif_data_no_id = NULL;

    if(backend == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "No backend is loaded");
        goto exit;
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "CTR[%s]: Container does not exist", ctr_id);
        goto exit;
    }

    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);
    if(notif_data_no_id) {
        notif_data_no_id->obj = ctr_obj;
    }

    res = cthulhu_ctr_remove(ctr_id, remove_data, notif_data_no_id, true);
    if(res == cthulhu_action_res_error) {
        goto exit;
    }
    if(res == cthulhu_action_res_busy) {
        cthulhu_ctr_priv_t* priv = cthulhu_ctr_get_priv(ctr_obj);
        if(!priv) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Container does not have private data!", ctr_id);
            goto exit;
        }
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Container will be updated after it has stopped", ctr_id);
        cb_data_update_t* cb_data = (cb_data_update_t*) calloc(1, sizeof(cb_data_update_t));
        amxc_ts_t* ts_created_copy = (amxc_ts_t*) calloc(1, sizeof(amxc_ts_t));
        if(ts_created_copy && ts_created) {
            memcpy(ts_created_copy, ts_created, sizeof(amxc_ts_t));
        }
        cb_data->ctr_id = strdup(ctr_id);
        cb_data->notif_data = cthulhu_notif_data_no_obj(notif_data);
        cb_data->bundle = strdup(bundle);
        cb_data->bundle_version = strdup(bundle_version);
        cb_data->linked_uuid = strdup(linked_uuid);
        cb_data->ts_created = ts_created_copy;
        if(disklocation) {
            cb_data->disk_location = strdup(disklocation);
        }
        cb_data->sb_id = strdup(sb_id);
        cb_data->remove_data = remove_data;
        amxc_var_new(&cb_data->meta_data);
        amxc_var_copy(cb_data->meta_data, metadata);
        if(module_version) {
            cb_data->module_version = strdup(module_version);
        }
        amxp_signal_mngr_t* sigmngr = cthulhu_sigmngrs_get(&ctr_sigmngrs, ctr_id);
        if(sigmngr) {
            amxp_slot_connect(sigmngr,
                              SIG_REMOVED,
                              NULL, cthulhu_ctr_update_cb, cb_data);
        } else {
            cthulhu_data_update_delete(&cb_data);
        }
        goto exit;
    }

    res = cthulhu_ctr_update_ctu(ctr_id, linked_uuid, bundle, bundle_version, disklocation, sb_id, notif_data, metadata, ts_created, module_version);

exit:
    cthulhu_notif_data_delete(&notif_data_no_id);
    return res;
}


static cthulhu_action_res_t cthulhu_ctr_update_ctu(const char* ctr_id,
                                                   const char* linked_uuid,
                                                   const char* bundle,
                                                   const char* bundle_version,
                                                   const char* disk_location,
                                                   const char* sb_id,
                                                   cthulhu_notif_data_t* notif_data,
                                                   const amxc_var_t* const metadata,
                                                   amxc_ts_t* ts_created,
                                                   const char* module_version) {
    cthulhu_notif_data_t* notif_data_no_id = NULL;
    amxd_object_t* ctr_obj = NULL;
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);
    // todo get initial created
    cthulhu_action_res_t res = cthulhu_ctr_create(ctr_id, linked_uuid, bundle, bundle_version, disk_location, sb_id, notif_data_no_id, true, metadata, ts_created, 0, module_version);
    if(res < 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not create container");
        goto exit;
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(notif_data && ctr_obj) {
        notif_data->obj = ctr_obj;
        cthulhu_notif_ctr_updated(notif_data, NULL);
    }
exit:
    cthulhu_notif_data_delete(&notif_data_no_id);
    return res;
}
typedef struct _cb_data_remove {
    char* ctr_id;
    cthulhu_notif_data_t* notif_data;
    bool remove_data;
    bool updating;
} cb_data_remove_t;

static void cthulhu_ctr_remove_cb(UNUSED const char* const sig_name,
                                  UNUSED const amxc_var_t* const data,
                                  void* const priv) {
    cb_data_remove_t* cb_data = (cb_data_remove_t*) priv;
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Callback to remove container", cb_data->ctr_id);

    // remove the callback
    amxp_signal_mngr_t* sigmngr = cthulhu_sigmngrs_get(&ctr_sigmngrs, cb_data->ctr_id);
    if(sigmngr) {
        amxp_slot_disconnect(sigmngr, SIG_STOPPED, cthulhu_ctr_remove_cb);
    }
    // remove the container
    cthulhu_ctr_remove(cb_data->ctr_id, cb_data->remove_data, cb_data->notif_data, cb_data->updating);

    cthulhu_notif_data_delete(&cb_data->notif_data);
    free(cb_data->ctr_id);
    free(cb_data);
}

static bool cthulhu_ctr_exists(const char* const ctr_id) {
    return !!cthulhu_ctr_get(ctr_id);
}

/**
 * @brief Internal function to remove a container
 *
 * @param ctr_id
 * @param remove_data when true, the persistent data of the ctr will be removed
 * @param notif_data contains data for notifications
 * @param updating set to true when updating, then no notif will be sent
 * @return cthulhu_action_res_t
 */
cthulhu_action_res_t cthulhu_ctr_remove(const char* ctr_id, bool remove_data,
                                        cthulhu_notif_data_t* notif_data, bool updating) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* ctr_obj = NULL;
    char* sb_id = NULL;
    cthulhu_notif_data_t* notif_data_no_id = NULL;
    amxp_signal_mngr_t* sigmngr = NULL;
    char* rootfs = NULL;

    if(backend == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "CTR[%s]: No backend is loaded", ctr_id);
        goto exit;
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "CTR[%s]: Container does not exist", ctr_id);
        goto exit;
    }
    if(notif_data) {
        notif_data->obj = ctr_obj;
    }
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);

    res = cthulhu_ctr_stop(ctr_id, notif_data_no_id);
    if(res == cthulhu_action_res_error) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Error when stopping. Try to remove anyway.", ctr_id);
    } else if(res == cthulhu_action_res_busy) {
        cthulhu_ctr_priv_t* priv = cthulhu_ctr_get_priv(ctr_obj);
        if(!priv) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Container does not have private data!", ctr_id);
            goto exit;
        }
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Container will be removed when it has stopped", ctr_id);
        cb_data_remove_t* cb_data = ( cb_data_remove_t* ) calloc(1, sizeof(cb_data_remove_t));
        cb_data->ctr_id = strdup(ctr_id);
        cb_data->notif_data = cthulhu_notif_data_no_obj(notif_data);
        cb_data->remove_data = remove_data;
        cb_data->updating = updating;
        sigmngr = cthulhu_sigmngrs_get(&ctr_sigmngrs, cb_data->ctr_id);
        if(sigmngr) {
            amxp_slot_connect(sigmngr,
                              SIG_STOPPED,
                              NULL, cthulhu_ctr_remove_cb, cb_data);
        } else {
            cthulhu_notif_data_delete(&cb_data->notif_data);
            free(cb_data->ctr_id);
            free(cb_data);
        }
        goto exit;
    }

    cthulhu_plugin_ctr_preremove(ctr_id);

    {
        amxc_var_t args;
        amxc_var_t ret;
        amxc_var_init(&args);
        amxc_var_init(&ret);

        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
        if(amxm_so_execute_function(backend, MOD_CTHULHU, CTHULHU_BACKEND_CMD_REMOVE, &args, &ret)) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not remove container on backend", ctr_id);
            res = cthulhu_action_res_error;
            amxc_var_clean(&args);
            amxc_var_clean(&ret);
            goto exit;
        }
        amxc_var_clean(&args);
        amxc_var_clean(&ret);
    }

    if(remove_data) {
        cthulhu_ctr_priv_sb_remove(ctr_obj, notif_data);
    }

    cthulhu_plugin_ctr_postremove(ctr_id);
    ASSERT_SUCCESS(cthulhu_unpriv_ctr_post_remove(ctr_id), NO_INSTRUCTION, "CTR[%s]: unprivileged postremove action failed", ctr_id);
    ASSERT_SUCCESS(cthulhu_unpriv_remove_user(ctr_id), NO_INSTRUCTION, "CTR[%s]: Failed to remove container user", ctr_id);
    ASSERT_SUCCESS(cthulhu_appdata_remove(ctr_id, remove_data), NO_INSTRUCTION, "CTR[%s]: Failed to remove Application Data", ctr_id);

    rootfs = cthulu_overlayfs_get_rootfs(ctr_id);
    CHECK_STR_EMPTY_LOG(WARNING, rootfs, NO_INSTRUCTION, "CTR[%s]: Could not get rootfs string", ctr_id);
    if(rootfs) {
        ASSERT_SUCCESS(cthulhu_rmdir(rootfs), NO_INSTRUCTION, "CTR[%s]: Could not remove rootfs [%s]", ctr_id, rootfs);
    }

    delete_ctr_priv(ctr_obj);
    if(!updating) {
        cthulhu_notif_ctr_removed(notif_data);
        amxd_object_emit_del_inst(ctr_obj);
        amxd_object_delete(&ctr_obj);
    } else {
        // only remove the layers if updating
        cthulhu_layer_remove_from_dm(ctr_obj);
    }
    /********************************************************************************************
    * After this point, the ctr_obj does not exist anymore!!!
    ********************************************************************************************/

    cthulhu_remove_unused_layers(ctr_id);
    cthulhu_syslogng_cleanup(ctr_id);
    sigmngr = cthulhu_sigmngrs_get(&ctr_sigmngrs, ctr_id);
    if(sigmngr) {
        amxp_sigmngr_emit_signal(sigmngr, SIG_REMOVED, NULL);
    }
    cthulhu_sigmngrs_delayed_remove(&ctr_sigmngrs, ctr_id, cthulhu_ctr_exists);

    res = cthulhu_action_res_done;
exit:
    cthulhu_notif_data_delete(&notif_data_no_id);
    free(rootfs);
    free(sb_id);
    return res;
}

void cthulhu_ctr_update_dm(const char* ctr_id, amxc_var_t* params) {
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* ctr_instances_obj = NULL;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_dm_t* dm = cthulhu_get_dm();

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj != NULL) {
        // update instance
        amxd_trans_select_object(&trans, ctr_obj);
    } else {
        // add new instance
        ctr_instances_obj = amxd_dm_findf(dm, CTHULHU_DM_CONTAINER_INSTANCES);
        char alias[ALIAS_SIZE + 1];
        if(ctr_instances_obj == NULL) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not find " CTHULHU_DM_CONTAINER_INSTANCES, ctr_id);
            goto exit;
        }
        amxd_trans_select_object(&trans, ctr_instances_obj);
        snprintf(alias, ALIAS_SIZE, "cpe-%s", ctr_id);
        amxd_trans_add_inst(&trans, 0, alias);
        // these params should be immutable, so they should only be set once
        amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_ID, ctr_id);
    }
    amxc_var_for_each(param, params) {
        const char* key = amxc_var_key(param);
        amxd_trans_set_param(&trans, key, param);
    }

    status = amxd_trans_apply(&trans, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Transaction failed. Status: %d", ctr_id, status);
    }
exit:
    amxd_trans_clean(&trans);
}

static void cthulhu_ctr_started(amxd_object_t* ctr_obj) {
    cthulhu_autorestart_ctr_started(ctr_obj);
}

static void cthulhu_ctr_stopped(const char* ctr_id, amxd_object_t* ctr_obj, cthulhu_ctr_status_t before) {

    if(before == cthulhu_ctr_status_ghost) {
        return;
    }
    cthulhu_autorestart_ctr_stopped(ctr_obj);
    if(cthulhu_overlayfs_supported()) {
        cthulhu_overlayfs_umount_rootfs(ctr_id, false);
        cthulhu_unpriv_ctr_post_umount(ctr_id);
    }
    cthulhu_unpriv_ctr_dm_reset_mappings(ctr_id);
}

static void cthulhu_ctr_status_changed(const char* ctr_id, cthulhu_ctr_status_t before, cthulhu_ctr_status_t after) {
    amxd_object_t* ctr_obj = NULL;
    amxd_trans_t trans;
    amxd_dm_t* dm = cthulhu_get_dm();
    amxc_ts_t now;
    amxc_ts_t ts_zero = {0, 0, 0};

    SAH_TRACEZ_INFO(ME, "CTR[%s]: STATE changed from %s to %s", ctr_id, cthulhu_ctr_status_to_string(before),
                    cthulhu_ctr_status_to_string(after));
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not find ctr object", ctr_id);
        return;
    }

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, ctr_obj);
    amxp_signal_mngr_t* sigmngr = cthulhu_sigmngrs_get(&ctr_sigmngrs, ctr_id);
    switch(after) {
    case cthulhu_ctr_status_running:
        amxc_ts_now(&now);
        amxd_trans_set_amxc_ts_t(&trans, CTHULHU_CONTAINER_STARTTIME, &now);
        amxd_trans_apply(&trans, dm);
        cthulhu_ctr_started(ctr_obj);
        if(sigmngr) {
            amxp_sigmngr_emit_signal(sigmngr, SIG_RUNNING, NULL);
        }
        break;
    case cthulhu_ctr_status_stopped:
        amxd_trans_set_amxc_ts_t(&trans, CTHULHU_CONTAINER_STARTTIME, &ts_zero);
        amxd_trans_apply(&trans, dm);
        cthulhu_ctr_stopped(ctr_id, ctr_obj, before);
        if(sigmngr) {
            amxp_sigmngr_emit_signal(sigmngr, SIG_STOPPED, NULL);
        }
    default:
        break;
    }
    cthulhu_plugin_ctr_statechanged(ctr_id, before, after);
    amxd_trans_clean(&trans);
}

static int cthulhu_ctr_add_ip_interface(amxd_object_t* itf_instances_obj, const char* const itf_name) {
    int res = -1;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, itf_instances_obj);
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_INTERFACES_NAME, itf_name);
    status = amxd_trans_apply(&trans, cthulhu_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "Transaction failed. IP interface not added. Status: %d", status);
        goto exit;
    }
    res = 0;
exit:
    amxd_trans_clean(&trans);
    return res;
}

static int cthulhu_ctr_delete_ip_interface(amxd_object_t* addr_instances_obj, uint32_t index) {
    int res = -1;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, addr_instances_obj);
    amxd_trans_del_inst(&trans, index, NULL);
    status = amxd_trans_apply(&trans, cthulhu_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "Transaction failed. IP interface not deleted. Status: %d", status);
        goto exit;
    }
    res = 0;
exit:
    amxd_trans_clean(&trans);
    return res;
}

static int cthulhu_ctr_add_ip_address(amxd_object_t* addr_instances_obj, const char* const ip_address) {
    int res = -1;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, addr_instances_obj);
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_INTERFACES_ADDRESSES_ADDRESS, ip_address);
    status = amxd_trans_apply(&trans, cthulhu_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "Transaction failed. IP address not added. Status: %d", status);
        goto exit;
    }
    res = 0;
exit:
    amxd_trans_clean(&trans);
    return res;
}

/**
 * @brief Add IP Addresses to the container
 *
 * @param ctr_id
 * @param interfaces
 * @return int -1 on error, otherwise the number of IP addresses added
 */
static int cthulhu_ctr_add_ip_addresses(amxd_object_t* ctr_obj, const amxc_llist_t* const interfaces) {
    int res = 0;
    amxd_object_t* itf_obj = NULL;
    amxd_object_t* itf_instances_obj = NULL;

    ASSERT_NOT_NULL(ctr_obj, goto error);

    itf_instances_obj = amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_INTERFACES);
    ASSERT_NOT_NULL(itf_instances_obj, goto error);

    amxc_llist_for_each(it, interfaces) {
        cthulhu_ctr_info_itf_t* itf = amxc_llist_it_get_data(it, cthulhu_ctr_info_itf_t, it);
        ASSERT_NOT_NULL(itf, goto error);
        itf_obj = amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_INTERFACES ".[" CTHULHU_CONTAINER_INTERFACES_NAME " == '%s'].", itf->name);
        if(!itf_obj) {
            // create new instance
            cthulhu_ctr_add_ip_interface(itf_instances_obj, itf->name);
            itf_obj = amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_INTERFACES ".[" CTHULHU_CONTAINER_INTERFACES_NAME " == '%s'].", itf->name);
        }
        ASSERT_NOT_NULL(itf_obj, goto exit);


        amxc_llist_for_each(it_ips, (&itf->ips)) {
            amxc_string_t* ip = amxc_string_from_llist_it(it_ips);
            amxd_object_t* addr_obj = amxd_object_findf(itf_obj, CTHULHU_CONTAINER_INTERFACES_ADDRESSES ".[" CTHULHU_CONTAINER_INTERFACES_ADDRESSES_ADDRESS " == '%s'].", ip->buffer);
            if(!addr_obj) {
                amxd_object_t* addr_instances_obj = amxd_object_findf(itf_obj, CTHULHU_CONTAINER_INTERFACES_ADDRESSES);
                cthulhu_ctr_add_ip_address(addr_instances_obj, ip->buffer);
                res++;
            }
        }
    }

    // remove interfaces that are not there anymore
    amxd_object_for_each(instance, it, itf_instances_obj) {
        itf_obj = amxc_container_of(it, amxd_object_t, it);
        ASSERT_NOT_NULL(itf_obj, goto exit);
        char* name = amxd_object_get_cstring_t(itf_obj, CTHULHU_CONTAINER_INTERFACES_NAME, NULL);
        ASSERT_NOT_NULL(name, goto exit);
        bool found = false;
        amxc_llist_for_each(it_itf, interfaces) {
            cthulhu_ctr_info_itf_t* itf = amxc_llist_it_get_data(it_itf, cthulhu_ctr_info_itf_t, it);
            if(strcmp(name, itf->name) == 0) {
                found = true;
                break;
            }
        }
        if(!found) {
            cthulhu_ctr_delete_ip_interface(itf_instances_obj, amxd_object_get_index(itf_obj));
        }
        free(name);
    }
    goto exit;
error:
    res = -1;
exit:
    return res;
}

static int cthulhu_update_container(const cthulhu_ctr_info_t* ctr_info) {
    int res = -1;
    cthulhu_ctr_status_t status_before = cthulhu_ctr_status_unknown;
    cthulhu_ctr_status_t status_in_dm = cthulhu_ctr_status_unknown;


    const char* ctr_id = ctr_info->ctr_id;
    amxd_object_t* ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_WARNING(ME, "did not find container %s", ctr_id);
        goto exit;
    }
    status_before = cthulhu_ctr_status_real_get(ctr_obj);
    status_in_dm = cthulhu_ctr_status_dm_get(ctr_obj);

    amxc_var_t params;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_OWNER, str_or_empty(ctr_info->owner));
    if((status_in_dm != cthulhu_ctr_status_restarting) ||
       ((status_in_dm == cthulhu_ctr_status_restarting) && (ctr_info->status != cthulhu_ctr_status_stopped)
        && (ctr_info->status != cthulhu_ctr_status_stopping))) {
        // While a restart of a container, the status has to keep its value to "Restarting" until the container is restarted.
        // Therefore, if the status of the container is "stopped" or "stopping" we do not set the value.
        amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_STATUS, cthulhu_ctr_status_to_string(ctr_info->status));
    }
    amxc_var_add_new_key_int32_t(&params, CTHULHU_CONTAINER_PID, ctr_info->pid);
    cthulhu_ctr_update_dm(ctr_id, &params);
    amxc_var_clean(&params);
    cthulhu_ctr_add_ip_addresses(ctr_obj, &ctr_info->interfaces);
    cthulhu_ctr_status_real_set(ctr_obj, ctr_info->status);

    if(status_before != ctr_info->status) {
        cthulhu_ctr_status_changed(ctr_id, status_before, ctr_info->status);
    }

    cthulhu_notif_data_t* notif_data = NULL;
    cthulhu_notif_data_new(&notif_data, ctr_obj, cthulhu_notif_cmd_ctr_update, NULL);
    cthulhu_notif_ctr_updated(notif_data, NULL);
    cthulhu_notif_data_delete(&notif_data);

    res = 0;
exit:
    return res;
}

/**
 * Update the containers
 *
 * All container info will be fetched form the backend, and the
 * datamodel will be updated accordingly
 *
 *
 * @return int 0 on success
 */
int cthulhu_update_containers(void) {
    int res = -1;
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* ctr_obj = NULL;
    amxd_dm_t* dm = cthulhu_get_dm();
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    if(backend == NULL) {
        SAH_TRACEZ_ERROR(ME, "No backend is loaded");
        goto exit;

    }

    res = amxm_so_execute_function(backend, MOD_CTHULHU, CTHULHU_BACKEND_CMD_LIST, &args, &ret);
    if(res != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to call '" CTHULHU_BACKEND_CMD_LIST "' on module (res: %d)", res);
        goto exit;
    }

    amxc_var_for_each(var, (&ret)) {
        const cthulhu_ctr_info_t* ctr_info = amxc_var_get_const_cthulhu_ctr_info_t(var);
        if(ctr_info == NULL) {
            SAH_TRACEZ_ERROR(ME, "Could not convert list item to cthulhu_ctr_info_t");
            continue;
        }
        cthulhu_update_container(ctr_info);
    }

    // now put the status to Ghost for all containers that are not reported back
    ctr_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES);
    if(ctr_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not get " CTHULHU_DM_CONTAINER_INSTANCES);
        goto exit;
    }
    amxd_object_for_each(instance, it, ctr_obj) {
        bool found = false;
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* ctr_id = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_ID, NULL);
        amxc_var_for_each(var, (&ret)) {
            const cthulhu_ctr_info_t* ctr_info = amxc_var_get_const_cthulhu_ctr_info_t(var);
            if(ctr_info == NULL) {
                SAH_TRACEZ_ERROR(ME, "Could not convert list item to cthulhu_ctr_info_t");
                continue;
            }
            if(strcmp(ctr_info->ctr_id, ctr_id) == 0) {
                found = true;
                break;
            }
        }
        free(ctr_id);

        if(!found) {
            amxd_trans_init(&trans);
            amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
            amxd_trans_select_object(&trans, instance);
            amxd_trans_set_value(cstring_t, &trans, CTHULHU_CONTAINER_STATUS,
                                 cthulhu_ctr_status_to_string(cthulhu_ctr_status_ghost));
            status = amxd_trans_apply(&trans, dm);
            if(status != amxd_status_ok) {
                SAH_TRACEZ_INFO(ME, "Transaction failed. " CTHULHU_CONTAINER_STATUS ": %d", status);
            }
            amxd_trans_clean(&trans);
        }
    }

    res = 0;
exit:
    amxc_var_for_each(var, (&ret)) {
        cthulhu_ctr_info_t* ctr_info = amxc_var_take_cthulhu_ctr_info_t(var);
        cthulhu_ctr_info_delete(&ctr_info);
    }
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return res;
}

static int cthulhu_ctr_metadata_check_constraints_resources(amxc_var_t* ctr_metadata, cthulhu_notif_data_t* notif_data) {
    int res = -1;
    amxc_var_t* var = NULL;
    if((var = GET_ARG(ctr_metadata, CTHULHU_CONTAINER_RESOURCES_CPU)) != NULL) {
        int32_t cpu = amxc_var_dyncast(int32_t, var);
        if(cpu > 100) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments,
                         "CPU should be between 0 and 100 [not %d]", cpu);
            goto exit;
        }
    }
    if((var = GET_ARG(ctr_metadata, CTHULHU_CONTAINER_RESOURCES_DISKSPACE)) != NULL) {
        int32_t diskspace = amxc_var_dyncast(int32_t, var);
        if((diskspace > 0) && (diskspace < SB_MIN_DISKSIZE)) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments,
                         "Diskspace should be bigger then %d KB [not %d]", SB_MIN_DISKSIZE, diskspace);
            goto exit;
        }
    }
    res = 0;
exit:
    return res;
}

static int cthulhu_ctr_metadata_check_constraints_hostobjects(amxc_var_t* ctr_metadata, cthulhu_notif_data_t* notif_data) {
    int res = -1;
    amxc_var_t* var = GET_ARG(ctr_metadata, CTHULHU_CONTAINER_HOSTOBJECT);
    if(!var) {
        res = 0;
        goto exit;
    }

    const amxc_llist_t* hostobjects = amxc_var_get_const_amxc_llist_t(var);
    if(!hostobjects) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments,
                     "Argument [%s] is not a list", CTHULHU_CONTAINER_HOSTOBJECT);
        goto exit;
    }
    amxc_var_t* inst = NULL;
    amxc_llist_for_each(it, hostobjects) {
        inst = amxc_llist_it_get_data(it, amxc_var_t, lit);
        if(amxc_var_type_of(inst) != AMXC_VAR_ID_HTABLE) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments,
                         "HostObject element is not a htable");
            goto exit;
        }
        if(!amxc_var_get_key(inst, CTHULHU_CONTAINER_HOSTOBJECT_SOURCE, AMXC_VAR_FLAG_DEFAULT)) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments,
                         "HostObject element misses argument [%s]", CTHULHU_CONTAINER_HOSTOBJECT_SOURCE);
            goto exit;
        }
        if(!amxc_var_get_key(inst, CTHULHU_CONTAINER_HOSTOBJECT_DESTINATION, AMXC_VAR_FLAG_DEFAULT)) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments,
                         "HostObject element misses argument [%s]", CTHULHU_CONTAINER_HOSTOBJECT_DESTINATION);
            goto exit;
        }
        if(!amxc_var_get_key(inst, CTHULHU_CONTAINER_HOSTOBJECT_OPTIONS, AMXC_VAR_FLAG_DEFAULT)) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments,
                         "HostObject element misses argument [%s]", CTHULHU_CONTAINER_HOSTOBJECT_OPTIONS);
            goto exit;
        }
    }

    res = 0;
exit:
    return res;
}

static int cthulhu_ctr_metadata_check_constraints_envvars(amxc_var_t* ctr_metadata, cthulhu_notif_data_t* notif_data) {
    int res = -1;
    amxc_var_t* var = GET_ARG(ctr_metadata, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES);
    if(!var) {
        res = 0;
        goto exit;
    }

    const amxc_llist_t* envvars = amxc_var_get_const_amxc_llist_t(var);
    if(!envvars) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments,
                     "Argument [%s] is not a list", CTHULHU_CONTAINER_ENVIRONMENTVARIABLES);
        goto exit;
    }
    const amxc_llist_t* environmentvariables = amxc_var_get_const_amxc_llist_t(var);
    if(!environmentvariables) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "EnvironmentVariables is not a list");
        goto exit;
    }
    amxc_var_t* inst = NULL;
    amxc_llist_for_each(it, environmentvariables) {
        inst = amxc_llist_it_get_data(it, amxc_var_t, lit);
        if(amxc_var_type_of(inst) != AMXC_VAR_ID_HTABLE) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "EnvironmentVariable element is not a htable");
            goto exit;
        }
        if(!amxc_var_get_key(inst, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_KEY, AMXC_VAR_FLAG_DEFAULT)) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "EnvironmentVariables element does not have key");
            goto exit;
        }
        if(!amxc_var_get_key(inst, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_VALUE, AMXC_VAR_FLAG_DEFAULT)) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "EnvironmentVariables element does not have value");
            goto exit;
        }
    }

    res = 0;
exit:
    return res;
}


static int cthulhu_ctr_metadata_check_constraints_appdata(amxc_var_t* ctr_metadata, cthulhu_notif_data_t* notif_data) {
    int res = -1;
    amxc_var_t* var = NULL;
    amxc_var_t* inst = NULL;

    if((var = GET_ARG(ctr_metadata, CTHULHU_CONTAINER_APPLICATIONDATA)) != NULL) {
        const amxc_llist_t* appdatalist = amxc_var_get_const_amxc_llist_t(var);
        if(!appdatalist) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments,
                         "ApplicationData is not a list");
            goto exit;
        }
        amxc_llist_for_each(it, appdatalist) {
            inst = amxc_llist_it_get_data(it, amxc_var_t, lit);
            if(amxc_var_type_of(inst) != AMXC_VAR_ID_HTABLE) {
                NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments,
                             "ApplicationData element is not a htable");
                goto exit;
            }
            if(!amxc_var_get_key(inst, CTHULHU_CONTAINER_APPLICATIONDATA_NAME, AMXC_VAR_FLAG_DEFAULT)) {
                NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments,
                             "ApplicationData element does not have key [%s]", CTHULHU_CONTAINER_APPLICATIONDATA_NAME);
                goto exit;
            }
            if(!amxc_var_get_key(inst, CTHULHU_CONTAINER_APPLICATIONDATA_CAPACITY, AMXC_VAR_FLAG_DEFAULT)) {
                NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments,
                             "ApplicationData element does not have key [%s]", CTHULHU_CONTAINER_APPLICATIONDATA_CAPACITY);
                goto exit;
            }
            if(!amxc_var_get_key(inst, CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN, AMXC_VAR_FLAG_DEFAULT)) {
                NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments,
                             "ApplicationData element does not have key [%s]", CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN);
                goto exit;
            }
            const char* retain = GET_CHAR(inst, CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN);
            if(!retain) {
                NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments,
                             "ApplicationData key [%s] value is invalid", CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN);
                goto exit;
            }
            if(strcmp(retain, CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_FOREVER) &&
               strcmp(retain, CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_UNTILSTOPPED)) {
                NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments,
                             "ApplicationData key [%s] value [%s] is invalid", CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN, retain);
                goto exit;
            }
            if(!amxc_var_get_key(inst, CTHULHU_CONTAINER_APPLICATIONDATA_ACCESSPATH, AMXC_VAR_FLAG_DEFAULT)) {
                NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments,
                             "ApplicationData element does not have key [%s]", CTHULHU_CONTAINER_APPLICATIONDATA_ACCESSPATH);
                goto exit;
            }
        }
    }
    res = 0;
exit:
    return res;
}

static int cthulhu_ctr_metadata_check_constraints(amxc_var_t* ctr_metadata, cthulhu_notif_data_t* notif_data) {
    int res = -1;
    amxc_var_t* var = NULL;

    ASSERT_SUCCESS(cthulhu_ctr_metadata_check_constraints_resources(ctr_metadata, notif_data), goto exit, "Resources arguments are invalid");
    ASSERT_SUCCESS(cthulhu_ctr_metadata_check_constraints_hostobjects(ctr_metadata, notif_data), goto exit, "HostObject arguments are invalid");
    ASSERT_SUCCESS(cthulhu_ctr_metadata_check_constraints_envvars(ctr_metadata, notif_data), goto exit, "Environment Variables arguments are invalid");
    ASSERT_SUCCESS(cthulhu_ctr_metadata_check_constraints_appdata(ctr_metadata, notif_data), goto exit, "ApplicationData arguments are invalid");

    if((var = GET_ARG(ctr_metadata, CTHULHU_CONTAINER_AUTORESTART)) != NULL) {
        amxc_var_t* ar_var = NULL;
        int32_t int_val;
        ar_var = GET_ARG(var, CTHULHU_DM_CTR_AR_ENABLE);
        if(!ar_var) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "AutoRestart parameter "CTHULHU_DM_CTR_AR_ENABLE " is not provided");
            goto exit;
        }

        ar_var = GET_ARG(var, CTHULHU_DM_CTR_AR_MIN_INT);
        if(!ar_var) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "AutoRestart parameter "CTHULHU_DM_CTR_AR_MIN_INT " is not provided");
            goto exit;
        }
        int_val = amxc_var_dyncast(int32_t, ar_var);
        if(int_val < 0) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "AutoRestart parameter "CTHULHU_DM_CTR_AR_MIN_INT " should be positive");
        }

        ar_var = GET_ARG(var, CTHULHU_DM_CTR_AR_MAX_INT);
        if(!ar_var) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "AutoRestart parameter "CTHULHU_DM_CTR_AR_MAX_INT " is not provided");
            goto exit;
        }
        int_val = amxc_var_dyncast(int32_t, ar_var);
        if(int_val < 0) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "AutoRestart parameter "CTHULHU_DM_CTR_AR_MAX_INT " should be positive");
        }

        ar_var = GET_ARG(var, CTHULHU_DM_CTR_AR_RETRY_INT);
        if(!ar_var) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "AutoRestart parameter "CTHULHU_DM_CTR_AR_RETRY_INT " is not provided");
            goto exit;
        }
        int_val = amxc_var_dyncast(int32_t, ar_var);
        if(int_val < 0) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "AutoRestart parameter "CTHULHU_DM_CTR_AR_RETRY_INT " should be positive");
        }

        ar_var = GET_ARG(var, CTHULHU_DM_CTR_AR_RESET);
        int_val = amxc_var_dyncast(int32_t, ar_var);
        if(int_val < 0) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "AutoRestart parameter "CTHULHU_DM_CTR_AR_RESET " should be positive");
        }

        ar_var = GET_ARG(var, CTHULHU_DM_CTR_AR_MAX_RETRYCOUNT);
        int_val = amxc_var_dyncast(int32_t, ar_var);
        if(int_val < 0) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "AutoRestart parameter "CTHULHU_DM_CTR_AR_MAX_RETRYCOUNT " should be positive");
        }
    }
    res = 0;
exit:
    return res;
}

static int cthulhu_ctr_metadata_fill(amxc_var_t* ctr_metadata, amxc_var_t* args) {
    const char* metadata_params[] = {CTHULHU_CONTAINER_RESOURCES_CPU,
        CTHULHU_CONTAINER_RESOURCES_DISKSPACE,
        CTHULHU_CONTAINER_RESOURCES_MEMORY,
        CTHULHU_CONTAINER_AUTOSTART,
        CTHULHU_CONTAINER_AUTORESTART,
        CTHULHU_CONTAINER_HOSTOBJECT,
        CTHULHU_CONTAINER_NETWORKCONFIG,
        CTHULHU_CONTAINER_ENVIRONMENTVARIABLES,
        CTHULHU_CONTAINER_REQUIRED_ROLES,
        CTHULHU_CONTAINER_OPTIONAL_ROLES,
        CTHULHU_CONTAINER_UNPRIVILEGED_PRIVILEGED,
        CTHULHU_CONTAINER_UNPRIVILEGED_NUMREQUIREDUIDS,
        CTHULHU_CONTAINER_APPLICATIONDATA,
        CTHULHU_CONTAINER_DATA};
    return cthulhu_metadata_fill(ctr_metadata, args, metadata_params, cthulhu_metadata_count(metadata_params));
}

static int cthulhu_ctr_update_dm_args_fill(amxc_var_t* args, amxc_var_t* dm_params) {
    const char* metadata_params[] = {
        CTHULHU_CONTAINER_REQUIRED_ROLES,
        CTHULHU_CONTAINER_OPTIONAL_ROLES
    };
    const char* metadata_dm_params[] = {
        CTHULHU_CONTAINER_REQUIRED_ROLES,
        CTHULHU_CONTAINER_OPTIONAL_ROLES
    };
    return cthulhu_dm_args_fill(args, dm_params, metadata_params, metadata_dm_params, cthulhu_metadata_count(metadata_params));
}

/**
 * Validate container arguments. Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Container_validate(amxd_object_t* obj,
                                  amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret) {
    lcm_dump_rpc(obj, func, args, NULL);
    amxd_status_t status = amxd_status_unknown_error;
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;
    amxc_var_t* ctr_metadata = NULL;

    amxc_var_new(&ctr_metadata);
    amxc_var_set_type(ctr_metadata, AMXC_VAR_ID_HTABLE);

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_ctr_validate, command_id);

    cthulhu_ctr_metadata_fill(ctr_metadata, args);
    if(cthulhu_ctr_metadata_check_constraints(ctr_metadata, notif_data) < 0) {
        status = amxd_status_invalid_arg;
        goto exit;
    }

    if(cthulhu_plugin_ctr_validate(args, ret) > 0) {
        int32_t error_type = GET_INT32(ret, CTHULHU_NOTIF_ERROR_TYPE);
        const char* reason = GET_CHAR(ret, CTHULHU_NOTIF_REASON);
        NOTIF_FAILED(notif_data, error_type, "%s", reason);
        status = amxd_status_invalid_arg;
        goto exit;
    }

    cthulhu_notif_ctr_validated(notif_data);
    status = amxd_status_ok;

exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_delete(&ctr_metadata);
    return status;
}

/**
 * Create a container. Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Container_create(amxd_object_t* obj,
                                amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret) {
    lcm_dump_rpc(obj, func, args, NULL);
    amxd_status_t status = amxd_status_unknown_error;
    cthulhu_action_res_t res = cthulhu_action_res_error;
    const char* bundle_name = GET_CHAR(args, CTHULHU_CONTAINER_BUNDLE);
    const char* bundle_version = GET_CHAR(args, CTHULHU_CONTAINER_BUNDLEVERSION);
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    const char* sb_id = GET_CHAR(args, CTHULHU_CONTAINER_SANDBOX);
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    const char* disklocation = GET_CHAR(args, CTHULHU_CONTAINER_DISKLOCATION);
    const char* linked_uuid = GET_CHAR(args, CTHULHU_CONTAINER_LINKED_UUID);
    const char* module_version = GET_CHAR(args, CTHULHU_CONTAINER_MODULEVERSION);
    amxc_var_t* ctr_metadata = NULL;
    cthulhu_notif_data_t* notif_data = NULL;
    uint64_t deferred_call_id = 0;

    amxd_object_t* ctr_obj = NULL;
    bool update = false;

    amxc_var_new(&ctr_metadata);
    amxc_var_set_type(ctr_metadata, AMXC_VAR_ID_HTABLE);

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_ctr_create, command_id);

    if(cthulhu_plugin_ctr_validate(args, ret) > 0) {
        int32_t error_type = GET_INT32(ret, CTHULHU_NOTIF_ERROR_TYPE);
        const char* reason = GET_CHAR(ret, CTHULHU_NOTIF_REASON);
        NOTIF_FAILED(notif_data, error_type, "%s", reason);
        goto exit;
    }

    cthulhu_ctr_metadata_fill(ctr_metadata, args);
    if(cthulhu_ctr_metadata_check_constraints(ctr_metadata, notif_data) < 0) {
        goto exit;
    }

    if(backend == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "No backend is loaded");
        goto exit;
    }
    if(string_is_empty(bundle_name)) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, CTHULHU_CONTAINER_BUNDLE " is empty");
        goto exit;
    }
    if(string_is_empty(ctr_id)) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, CTHULHU_CONTAINER_ID " is empty");
        goto exit;
    }
    if(string_is_empty(bundle_version)) {
        SAH_TRACEZ_WARNING(ME, "No " CTHULHU_CONTAINER_BUNDLEVERSION " given using 'latest'");
        bundle_version = "latest";
    }
    if(string_is_empty(linked_uuid)) {
        SAH_TRACEZ_WARNING(ME, CTHULHU_CONTAINER_LINKED_UUID " is empty");
    }
    if(!string_is_empty(sb_id)) {
        amxd_object_t* sb_obj = cthulhu_sb_get(sb_id);
        if(!sb_obj) {
            NOTIF_FAILED(notif_data, tr181_fault_unknown_exec_env, "ExecutionEnvironment %s does not exist", sb_id);
            goto exit;
        }
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj) {
        if(cthulhu_ctr_status_dm_get(ctr_obj) != cthulhu_ctr_status_ghost) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "The container already exists");
            goto exit;
        }
        update = true;
    }

    amxd_function_defer(func, &deferred_call_id, ret, NULL, NULL);

    res = cthulhu_ctr_create(ctr_id, linked_uuid, bundle_name, bundle_version, disklocation, sb_id, notif_data, update, ctr_metadata, NULL, deferred_call_id, module_version);
    if(res != cthulhu_action_res_deferred) {
        amxd_function_deferred_remove(deferred_call_id);
        cthulhu_ctr_remove(ctr_id, update, NULL, false);
        amxc_var_set(bool, ret, false);
    } else {
        status = amxd_status_deferred;
    }
exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_delete(&ctr_metadata);
    return status;
}

/**
 * If ContainerId does not match the ContainerId of an existing
 * container then Cthulhu will publish an error message and will
 * take no further action.
 * If the specified container is already running then Cthulhu
 * will immediately publish a "started" notification and will
 * take no further action. Otherwise Cthulhu will attempt to
 * start the container and either a "started" notification or an
 * error will result.
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Container_start(amxd_object_t* obj,
                               UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    lcm_dump_rpc(obj, func, args, NULL);
    int res = 0;
    amxd_status_t status = amxd_status_unknown_error;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_ctr_start, command_id);

    if(string_is_empty(ctr_id)) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "No ContainerId provided");
        goto exit;
    }


    res = cthulhu_ctr_start(ctr_id, notif_data);
    if(res != 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Failed to start container %s",
                     ctr_id);
        goto exit;
    }

    status = amxd_status_ok;
exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

/**
 * If ContainerId does not match the ContainerId of an existing
 * container then Cthulhu will publish an error message and will
 * take no further action.
 * If the specified container is not running then Cthulhu will
 * immediately publish a "stopped" notification and will take no
 * further action. Otherwise Cthulhu will attempt to stop the
 * container and either a "stopped" notification or an error
 * will result.
 */
amxd_status_t _Container_stop(amxd_object_t* obj,
                              UNUSED amxd_function_t* func,
                              amxc_var_t* args,
                              amxc_var_t* ret) {
    lcm_dump_rpc(obj, func, args, NULL);
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_status_t status = amxd_status_unknown_error;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;
    amxd_trans_t trans;
    amxd_object_t* ctr_obj = NULL;
    amxd_dm_t* dm = cthulhu_get_dm();
    amxd_trans_init(&trans);

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_ctr_stop, command_id);

    if(string_is_empty(ctr_id)) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "No ContainerId provided");
        goto exit;
    }

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Container %s does not exist", ctr_id);
        goto exit;
    }

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, ctr_obj);
    amxd_trans_set_bool(&trans, CTHULHU_CONTAINER_LASTREQSTATEACTIVE, false);
    amxd_trans_apply(&trans, dm);
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Stopping container", ctr_id);
    res = cthulhu_ctr_stop(ctr_id, notif_data);
    if(res == cthulhu_action_res_error) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Failed to stop container %s",
                     ctr_id);
        goto exit;
    }

    status = amxd_status_ok;
exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    amxd_trans_clean(&trans);
    return status;
}

/**
 * If ContainerId does not match the ContainerId of an existing
 * container then Cthulhu will publish an error message and will
 * take no further action.
 * If the specified container is not running then Cthulhu will
 * immediately publish a "stopped" notification and will take no
 * further action. Otherwise Cthulhu will attempt to restart the
 * container and either a "restarted" notification or an error
 * will result.
 */
amxd_status_t _Container_restart(amxd_object_t* obj,
                                 UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    lcm_dump_rpc(obj, func, args, NULL);
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* ctr_obj = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    char* sb_id = NULL;
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_ctr_restart, command_id);
    if(string_is_empty(ctr_id)) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "No ContainerId provided");
        goto exit;
    }

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "CTR[%s]: container does not exist", ctr_id);
        goto exit;
    }
    sb_id = cthulhu_ctr_get_sb_id(ctr_obj);
    if(sb_id) {
        if(!cthulhu_is_sb_enabled(sb_id)) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_du_state, "EE [%s] is disabled. Container [%s] will not be started", sb_id, ctr_id);
            goto exit;
        }
    }
    {
        amxd_trans_t* trans = NULL;
        amxd_status_t trans_status = amxd_status_unknown_error;
        cthulhu_trans_new(&trans, ctr_obj);
        amxd_trans_set_cstring_t(trans, CTHULHU_CONTAINER_STATUS, cthulhu_ctr_status_to_string(cthulhu_ctr_status_restarting));
        trans_status = cthulhu_trans_send(&trans);
        if(trans_status != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Transaction failed: %d", ctr_id, trans_status);
            goto exit;
        }
    }
    res = cthulhu_ctr_restart(ctr_id, notif_data);
    if(res == cthulhu_action_res_error) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "CTR[%s]: Failed to restart container",
                     ctr_id);
        goto exit;
    }
    status = amxd_status_ok;
exit:
    free(sb_id);
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

/**
 * If ContainerId does not match the ContainerId of an existing
 * container then Cthulhu will publish an error message and will
 * take no further action.
 * If the specified container is running then Cthulhu will stop
 * the container before removing it.
 */
amxd_status_t _Container_remove(amxd_object_t* obj,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret) {
    lcm_dump_rpc(obj, func, args, NULL);
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_status_t status = amxd_status_unknown_error;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    bool remove_data = GET_BOOL(args, CTHULHU_CONTAINER_REMOVEDATA);
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_ctr_remove, command_id);

    if(string_is_empty(ctr_id)) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "No ContainerId provided");
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Removing container. %s data.", ctr_id, remove_data ? "Removing" : "Keeping");
    res = cthulhu_ctr_remove(ctr_id, remove_data, notif_data, false);
    if(res == cthulhu_action_res_error) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Failed to remove container %s",
                     ctr_id);
        goto exit;
    }

    cthulhu_update_containers();
    status = amxd_status_ok;
exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

/**
 * If ContainerId does not match the ContainerId of an existing
 * container then Cthulhu will publish an error message and will
 * take no further action.
 * If the specified container is running then Cthulhu will stop
 * the container before updating it.
 */
amxd_status_t _Container_update(amxd_object_t* obj,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret) {
    lcm_dump_rpc(obj, func, args, NULL);
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* ctr_obj = NULL;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    const char* bundle = GET_CHAR(args, CTHULHU_CONTAINER_BUNDLE);
    const char* bundle_version = GET_CHAR(args, CTHULHU_CONTAINER_BUNDLEVERSION);
    const char* disklocation = GET_CHAR(args, CTHULHU_CONTAINER_DISKLOCATION);
    bool remove_data = GET_BOOL(args, CTHULHU_CONTAINER_REMOVEDATA);
    const char* bundle_default = NULL;
    const char* sb_id = NULL;
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    const char* module_version = GET_CHAR(args, CTHULHU_CONTAINER_MODULEVERSION);
    amxc_var_t* ctr_metadata;
    cthulhu_notif_data_t* notif_data = NULL;
    amxc_ts_t* ts_created = NULL;
    const char* linked_uuid = NULL;
    amxc_var_t ctr_obj_params;

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_ctr_update, command_id);
    amxc_var_new(&ctr_metadata);
    amxc_var_set_type(ctr_metadata, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&ctr_obj_params);

    if(string_is_empty(ctr_id)) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "No ContainerId provided");
        goto exit;
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Container %s does not exist", ctr_id);
        goto exit;
    }

    amxd_object_get_params(ctr_obj, &ctr_obj_params, amxd_dm_access_public);

    cthulhu_ctr_update_dm_args_fill(args, &ctr_obj_params);

    if(cthulhu_plugin_ctr_validate(args, ret) > 0) {
        int32_t error_type = GET_INT32(ret, CTHULHU_NOTIF_ERROR_TYPE);
        const char* reason = GET_CHAR(ret, CTHULHU_NOTIF_REASON);
        cthulhu_notif_error(notif_data, error_type, reason);
        goto exit;
    }

    ts_created = amxc_var_dyncast(amxc_ts_t, GET_ARG(&ctr_obj_params, CTHULHU_CONTAINER_CREATED));
    if(ts_created == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot get " CTHULHU_CONTAINER_CREATED);
    }

    if(string_is_empty(bundle)) {
        bundle_default = GET_CHAR(&ctr_obj_params, CTHULHU_CONTAINER_BUNDLEVERSION);
        bundle = bundle_default;
        if(string_is_empty(bundle)) {
            NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Container %s Bundle is unknown", ctr_id);
            goto exit;
        }
    }
    if(string_is_empty(bundle_version)) {
        bundle_version = "latest";
    }

    linked_uuid = GET_CHAR(&ctr_obj_params, CTHULHU_CONTAINER_LINKED_UUID);
    if(string_is_empty(linked_uuid)) {
        SAH_TRACEZ_WARNING(ME, CTHULHU_CONTAINER_LINKED_UUID " is empty");
    }

    cthulhu_ctr_metadata_fill(ctr_metadata, args);
    if(cthulhu_ctr_metadata_check_constraints(ctr_metadata, notif_data) < 0) {
        goto exit;
    }

    sb_id = GET_CHAR(&ctr_obj_params, CTHULHU_CONTAINER_SANDBOX);
    res = cthulhu_ctr_update(ctr_id, linked_uuid, bundle, bundle_version, disklocation, sb_id, remove_data, notif_data, ctr_metadata, ts_created, module_version);
    if(res == cthulhu_action_res_error) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Failed to update container %s",
                     ctr_id);
        goto exit;
    }

    cthulhu_update_containers();
    status = amxd_status_ok;
exit:
    amxc_var_clean(&ctr_obj_params);
    free(ts_created);
    amxc_var_delete(&ctr_metadata);
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

amxd_status_t _Container_modifyNetworkConfig(UNUSED amxd_object_t* obj,
                                             UNUSED amxd_function_t* func,
                                             UNUSED amxc_var_t* args,
                                             amxc_var_t* ret) {
    lcm_dump_rpc(obj, func, args, NULL);
    amxd_status_t status = amxd_status_unknown_error;
    cthulhu_notif_data_t* notif_data = NULL;
    amxd_object_t* ctr_obj = NULL;
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_ctr_update, command_id);

    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);

    // check if there is already an container with this ctr_id
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Container %s does not exist", ctr_id);
        goto exit;
    }

    cthulhu_plugin_ctr_modify(ctr_id, args);

    cthulhu_notif_ctr_list(notif_data, false);

    status = amxd_status_ok;
exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

/**
 * Call a generic command. Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Container_command(amxd_object_t* obj,
                                 UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    // lcm_dump_rpc(obj, func, args, NULL);
    amxd_status_t status = amxd_status_unknown_error;

    const char* command = GET_CHAR(args, CTHULHU_COMMAND_COMMAND);
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    amxc_var_t* params = GET_ARG(args, CTHULHU_COMMAND_PARAMETERS);
    amxc_var_t* params_new = NULL;

    if(!string_is_empty(command_id)) {
        if(params == NULL) {
            amxc_var_new(&params_new);
            amxc_var_set_type(params_new, AMXC_VAR_ID_HTABLE);
            params = params_new;
        }
        amxc_var_add_key(cstring_t, params, CTHULHU_NOTIF_COMMAND_ID, command_id);
    }

    status = amxd_object_invoke_function(obj, command, params, ret);

    amxc_var_delete(&params_new);
    return status;
}

/**
 * Call a generic command. Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Container_ls(amxd_object_t* obj,
                            UNUSED amxd_function_t* func,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    lcm_dump_rpc(obj, func, args, NULL);
    bool quiet = amxc_var_dyncast(bool, GET_ARG(args, CTHULHU_CMD_CTR_LS_QUIET));
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_ctr_list, command_id);

    cthulhu_notif_ctr_list(notif_data, quiet);

    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, true);
    return amxd_status_ok;
}

bool cthulhu_load_backend(const char* backend_so) {
    bool res = false;

    if(backend) {
        SAH_TRACEZ_ERROR(ME, "Backend already set: %p", backend);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Load backend [%s]", backend_so);
    if(amxm_so_open(&backend, "packager", backend_so) != 0) {
        SAH_TRACEZ_ERROR(ME, "Can not load backend [%s]", backend_so);
        goto exit;
    }

    if(amxm_so_get_module(backend, MOD_CTHULHU) == NULL) {
        SAH_TRACEZ_ERROR(ME, "Library does not provide %s", MOD_CTHULHU);
        amxm_so_close(&backend);
        goto exit;
    }

    amxc_var_t init_params;
    amxc_var_t ret;
    amxc_var_init(&init_params);
    amxc_var_init(&ret);

    amxc_var_set_type(&init_params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_data(&init_params, CTHULHU_BACKEND_CMD_INIT_PARAM_PARSER, cthulhu_get_parser());
    amxc_var_add_new_key_data(&init_params, CTHULHU_BACKEND_CMD_INIT_PARAM_ROOTOBJECT, cthulhu_root_get());
    if(amxm_so_execute_function(backend, MOD_CTHULHU, CTHULHU_BACKEND_CMD_INIT, &init_params, &ret) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not init backend");
        amxc_var_clean(&init_params);
        amxc_var_clean(&ret);
        goto exit;
    }
    amxc_var_clean(&init_params);
    amxc_var_clean(&ret);

    res = true;
exit:
    return res;
}

#define EXEC_DATA_CALL_ID "call_id"
#define EXEC_DATA_CMD_RET "cmd_ret"

static int cthulhu_execute_task_callback(lcm_worker_t* worker, UNUSED amxc_var_t* var, int rc, amxc_var_t* data) {
    int res = -1;
    uint64_t call_id = 0;

    amxc_var_t* var_callid = GET_ARG(data, EXEC_DATA_CALL_ID);
    ASSERT_NOT_NULL(var_callid, goto exit);
    call_id = amxc_var_get_uint64_t(var_callid);

    amxc_var_t* cmd_ret = GET_ARG(data, EXEC_DATA_CMD_RET);

    amxd_function_deferred_done(call_id, rc == 0 ? amxd_status_ok : amxd_status_unknown_error, NULL, cmd_ret);

    lcm_worker_stop(worker, true);
    lcm_worker_delete(&worker, true);
    res = 0;
exit:
    return res;
}

static int cthulhu_execute_task(UNUSED lcm_worker_t* worker, amxc_var_t* cmd_info, UNUSED int rc, amxc_var_t* data) {
    ASSERT_NOT_NULL(data, return -1);

    amxc_var_t* cmd_ret = amxc_var_add_new_key(data, EXEC_DATA_CMD_RET);

    return amxm_so_execute_function(cthulhu_backend_get(), MOD_CTHULHU, CTHULHU_BACKEND_CMD_CTR_EXECUTE, cmd_info, cmd_ret);
}

static int cthulhu_container_execute(amxd_function_t* func, amxc_var_t* cmd_info, amxc_var_t* ret) {
    lcm_worker_t* worker = NULL;
    lcm_worker_task_t* task = NULL;
    uint64_t callid = 0;
    amxc_var_t task_data;
    amxc_var_init(&task_data);

    ASSERT_SUCCESS(amxd_function_defer(func, &callid, ret, NULL, NULL), return amxd_status_unknown_error);

    ASSERT_SUCCESS(amxc_var_set_type(&task_data, AMXC_VAR_ID_HTABLE), goto error);
    amxc_var_add_key(uint64_t, &task_data, EXEC_DATA_CALL_ID, callid);


    ASSERT_SUCCESS(lcm_worker_new(&worker, NULL, "w_execute"), goto error);
    ASSERT_SUCCESS(lcm_worker_start(worker, cthulhu_get_parser()), goto error);

    ASSERT_SUCCESS(lcm_worker_task_new(&task), goto error);
    ASSERT_SUCCESS(lcm_worker_task_add_function(task, cthulhu_execute_task, cmd_info), goto error);
    ASSERT_SUCCESS(lcm_worker_task_add_callback(task, cthulhu_execute_task_callback, NULL), goto error);
    ASSERT_SUCCESS(lcm_worker_task_add_data(task, &task_data), goto error);
    ASSERT_SUCCESS(lcm_worker_add_task(worker, task, false), goto error);

    amxc_var_clean(&task_data);
    return amxd_status_deferred;

error:
    lcm_worker_task_delete(&task);
    lcm_worker_stop(worker, true);
    lcm_worker_delete(&worker, true);
    amxd_function_deferred_remove(callid);

    amxc_var_clean(&task_data);
    return amxd_status_unknown_error;
}

/**
 * Execute a command inside a container.
 * A timer of CTR_EXECUTE_TIMEOUT seconds is used to
 * stop the execution in case it is not finished within
 * the defined timeout.
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Container_execute(amxd_object_t* obj,
                                 UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    lcm_dump_rpc(obj, func, args, NULL);
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* cmd_info = NULL;

    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    const char* command = GET_CHAR(args, CTHULHU_CMD_CTR_EXECUTE_CMD);
    amxc_var_t* arguments = GET_ARG(args, CTHULHU_CMD_CTR_EXECUTE_ARGUMENTS);
    int timeout = GET_UINT32(args, CTHULHU_CMD_CTR_EXECUTE_TIMEOUT);

    if(string_is_empty(ctr_id) || string_is_empty(command) || (timeout == 0)) {
        return amxd_status_invalid_arg;
    }
    if(true != amxm_so_has_function(cthulhu_backend_get(), MOD_CTHULHU, CTHULHU_BACKEND_CMD_CTR_EXECUTE)) {
        SAH_TRACEZ_ERROR(ME, "Cthulhu backend does not support " CTHULHU_BACKEND_CMD_CTR_EXECUTE);
        return amxd_status_not_supported;
    }

    amxc_var_new(&cmd_info);
    amxc_var_set_type(cmd_info, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, cmd_info, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, cmd_info, CTHULHU_MOD_BACKEND_COMMAND_EXECUTE_PARAMS_COMMAND, command);
    amxc_var_set_key(cmd_info, CTHULHU_MOD_BACKEND_COMMAND_EXECUTE_PARAMS_ARGUMENTS, arguments, AMXC_VAR_FLAG_COPY);
    amxc_var_add_key(uint32_t, cmd_info, CTHULHU_MOD_BACKEND_COMMAND_EXECUTE_PARAMS_TIMEOUT, timeout);

    status = cthulhu_container_execute(func, cmd_info, ret);
    amxc_var_delete(&cmd_info);
    return status;
}

/**
 * Update the versions of cthulhu and the backend
 *
 */
static void cthulhu_update_versions(void) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_dm_t* dm = cthulhu_get_dm();
    amxd_object_t* info = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    info = amxd_dm_findf(dm, CTHULHU_DM_INFO);
    if(!info) {
        SAH_TRACEZ_ERROR(ME, "Could not get " CTHULHU_DM_INFO);
        goto exit;
    }
    amxd_trans_select_object(&trans, info);
#ifdef VERSION
    amxd_trans_set_cstring_t(&trans, CTHULHU_INFO_VERSION, STR(VERSION));
#endif
    if(backend) {
        amxc_var_t ret;
        amxc_var_t args;
        amxc_var_init(&ret);
        amxc_var_init(&args);
        int res = amxm_so_execute_function(backend, MOD_CTHULHU, CTHULHU_BACKEND_CMD_VERSION, &args, &ret);
        if(res != 0) {
            SAH_TRACEZ_ERROR(ME, "Could not get version from backend");
        } else {
            amxd_trans_set_param(&trans, CTHULHU_INFO_BACKENDVERSION, &ret);
        }
        amxc_var_clean(&ret);
        amxc_var_clean(&args);
    }

    status = amxd_trans_apply(&trans, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "Transaction failed. " CTHULHU_CONTAINER_STATUS ": %d", status);
    }
exit:
    amxd_trans_clean(&trans);
}

/**
 * Set Cthulhu.Information.Initialized to true
 *
 */
void cthulhu_set_dm_info_initialized(void) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_dm_t* dm = cthulhu_get_dm();
    amxd_object_t* info = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    info = amxd_dm_findf(dm, CTHULHU_DM_INFO);
    if(!info) {
        SAH_TRACEZ_ERROR(ME, "Could not get " CTHULHU_DM_INFO);
        goto exit;
    }
    amxd_trans_select_object(&trans, info);
    amxd_trans_set_bool(&trans, CTHULHU_INFO_INITIALIZED, true);
    status = amxd_trans_apply(&trans, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "Transaction failed. " CTHULHU_CONTAINER_STATUS ": %d", status);
    }
exit:
    amxd_trans_clean(&trans);
}

/**
 * Get information about the backend
 */
static void cthulhu_get_backend_information(void) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_dm_t* dm = cthulhu_get_dm();
    amxd_object_t* info = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    info = amxd_dm_findf(dm, CTHULHU_DM_INFO);
    if(!info) {
        SAH_TRACEZ_ERROR(ME, "Could not get " CTHULHU_DM_INFO);
        goto exit;
    }
    amxd_trans_select_object(&trans, info);

    if(backend) {
        amxc_var_t ret;
        amxc_var_t args;

        amxc_var_init(&ret);
        amxc_var_init(&args);
        int res = amxm_so_execute_function(backend, MOD_CTHULHU, CTHULHU_BACKEND_CMD_INFORMATION, &args, &ret);
        if(res != 0) {
            SAH_TRACEZ_ERROR(ME, "Could not get information from backend");
        } else {
            amxc_var_t* backend_name = amxc_var_get_key(&ret, CTHULHU_INFO_BACKENDNAME, AMXC_VAR_FLAG_DEFAULT);
            amxc_var_t* bundles_supported = amxc_var_get_key(&ret, CTHULHU_INFO_BUNDLES_SUPPORTED, AMXC_VAR_FLAG_DEFAULT);
            amxc_var_t* overlayfs_on_create = amxc_var_get_key(&ret, CTHULHU_INFO_OVERLAYFSONCREATE, AMXC_VAR_FLAG_DEFAULT);

            amxd_trans_set_param(&trans, CTHULHU_INFO_BACKENDNAME, backend_name);
            amxd_trans_set_param(&trans, CTHULHU_INFO_BUNDLES_SUPPORTED, bundles_supported);
            amxd_trans_set_param(&trans, CTHULHU_INFO_OVERLAYFSONCREATE, overlayfs_on_create);
        }
        amxc_var_clean(&ret);
        amxc_var_clean(&args);
    }

    status = amxd_trans_apply(&trans, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "Transaction failed. " CTHULHU_CONTAINER_STATUS ": %d", status);
    }
exit:
    amxd_trans_clean(&trans);
}

void cthulhu_backend_changed_handler(const char* const sig_name,
                                     UNUSED const amxc_var_t* const notif,
                                     UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "Notification received [%s]:", sig_name);
    ASSERT_NOT_NULL(notif, goto exit);
    amxc_var_t* data = GET_ARG(notif, "data");
    const cthulhu_ctr_info_t* ctr_info = amxc_var_get_const_cthulhu_ctr_info_t(data);
    ASSERT_NOT_NULL(ctr_info, goto exit);
    cthulhu_update_container(ctr_info);
exit:
    return;
}

static int cthulhu_backend_notif_init(void) {
    int res = -1;
    amxb_bus_ctx_t* amxb_bus_ctx_cthulhu = amxb_be_who_has(CTHULHU_DM);
    if(!amxb_bus_ctx_cthulhu) {
        SAH_TRACEZ_ERROR(ME, "Cannot get bus ctx for " CTHULHU_DM);
        goto exit;
    }
    if(amxb_subscribe(amxb_bus_ctx_cthulhu,
                      CTHULHU,
                      "notification == '" CTHULHU_BACKEND_NOTIF_CHANGED "'",
                      cthulhu_backend_changed_handler,
                      NULL) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not subscribe to backend changed events");
        goto exit;
    }
    res = 0;
exit:
    return res;
}
/**
 * Load a plugin. Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Cthulhu_loadBackend(UNUSED amxd_object_t* obj,
                                   UNUSED amxd_function_t* func,
                                   amxc_var_t* args,
                                   amxc_var_t* ret) {
    lcm_dump_rpc(obj, func, args, NULL);
    amxd_status_t status = amxd_status_unknown_error;
    const char* backend_so = GET_CHAR(args, CTHULHU_CMD_CTR_LOAD_BACKEND);

    if(cthulhu_load_backend(backend_so)) {
        status = amxd_status_ok;
        cthulhu_update_versions();
        cthulhu_get_backend_information();
    }

    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

static void update_timer_cb(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    if(backend != NULL) {
        cthulhu_update_containers();
    }
}

/**
 * @brief send the updated datamodel to subscribers if the dm update timer has expired
 *
 * @param timer
 * @param priv
 */
static void dm_changed_timer_cb(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    cthulhu_notif_data_t* notif_data = NULL;
    cthulhu_notif_data_new(&notif_data, cthulhu_root_get(), cthulhu_notif_cmd_ctr_list, NULL);
    cthulhu_notif_ctr_list(notif_data, false);
    cthulhu_notif_data_delete(&notif_data);
}

static void cthulhu_dm_notif_handler(UNUSED const char* const sig_name,
                                     UNUSED const amxc_var_t* const data,
                                     UNUSED void* const priv) {
    amxp_timer_start(dm_changed_timer, 2000);
}

/**
 * @brief DM changed events
 *
 * every time the DM changes, the update timer is reset. I it finally triggers, the new state
 * of the containers is sent
 * the timer is used to prevent overloading subscribers
 *
 */
static void cthulhu_dm_changed_init(void) {
    amxp_timer_new(&dm_changed_timer, dm_changed_timer_cb, NULL);
    amxb_bus_ctx_t* amxb_bus_ctx_cthulhu = amxb_be_who_has(CTHULHU_DM);
    if(!amxb_bus_ctx_cthulhu) {
        SAH_TRACEZ_ERROR(ME, "Cannot get bus ctx for " CTHULHU_DM);
        goto exit;
    }
    if(amxb_subscribe(amxb_bus_ctx_cthulhu,
                      CTHULHU_DM_CONTAINER_INSTANCES,
                      "notification in 'dm:object-added,dm:object-removed,dm:instance-added,dm:instance-removed'",
                      cthulhu_dm_notif_handler,
                      NULL) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not subscribe to IP interface events");
        goto exit;
    }
exit:
    return;
}

static void cthulhu_dm_changed_cleanup(void) {
    amxb_bus_ctx_t* amxb_bus_ctx_cthulhu = amxb_be_who_has(CTHULHU_DM);
    if(!amxb_bus_ctx_cthulhu) {
        SAH_TRACEZ_ERROR(ME, "Cannot get bus ctx for " CTHULHU_DM);
        goto exit;
    }
    amxb_unsubscribe(amxb_bus_ctx_cthulhu, CTHULHU_DM_CONTAINER_INSTANCES, cthulhu_dm_notif_handler, NULL);
    amxp_timer_delete(&dm_changed_timer);
exit:
    return;
}


char* cthulhu_ctr_get_sb_id(amxd_object_t* ctr_obj) {
    char* sb_id = NULL;
    ASSERT_NOT_NULL(ctr_obj, goto exit);
    sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_SANDBOX, NULL);
    if(string_is_empty(sb_id)) {
        free(sb_id);
        sb_id = NULL;
    }
exit:
    return sb_id;
}

char* cthulhu_ctr_get_priv_sb_id(amxd_object_t* const ctr_obj) {
    char* priv_sb_id = NULL;
    ASSERT_NOT_NULL(ctr_obj, goto exit);
    priv_sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_PRIVATESANDBOX, NULL);
    if(string_is_empty(priv_sb_id)) {
        free(priv_sb_id);
        priv_sb_id = NULL;
    }
exit:
    return priv_sb_id;
}

amxp_signal_mngr_t* cthulhu_ctr_get_sigmngr(const char* ctr_id) {
    return cthulhu_sigmngrs_get(&ctr_sigmngrs, ctr_id);
}

static int cthulhu_worker_init(void) {
    int res = -1;
    cthulhu_app_t* app_data = cthulhu_get_app_data();
    if(!app_data) {
        SAH_TRACEZ_ERROR(ME, "App data is NULL");
        goto exit;
    }
    if(lcm_worker_new(&app_data->worker, NULL, "worker") != 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create worker");
        goto exit;

    }
    if(lcm_worker_start(app_data->worker, cthulhu_get_parser()) != 0) {
        SAH_TRACEZ_ERROR(ME, "Could not start worker");
        goto exit;
    }

    res = 0;
exit:
    return res;
}

static void cthulhu_worker_cleanup(void) {
    cthulhu_app_t* app_data = cthulhu_get_app_data();
    if(!app_data) {
        SAH_TRACEZ_ERROR(ME, "App data is NULL");
        return;
    }
    if(!app_data->worker) {
        SAH_TRACEZ_ERROR(ME, "Worker is NULL");
        return;
    }
    lcm_worker_stop(app_data->worker, true);
    lcm_worker_delete(&app_data->worker, true);
}

static void cthulhu_prestart_init(void) {
    cthulhu_prestartup_config_t* config = cthulhu_get_prestartup_config();
    if(!config) {
        SAH_TRACEZ_ERROR(ME, "Prestart config NULL!");
        return;
    }
    amxc_var_new(&(config->ctr_create));
    amxc_var_set_type(config->ctr_create, AMXC_VAR_ID_LIST);

    amxc_var_new(&(config->sb_create));
    amxc_var_set_type(config->sb_create, AMXC_VAR_ID_LIST);
}

static void cthulhu_prestart_cleanup(void) {
    cthulhu_prestartup_config_t* config = cthulhu_get_prestartup_config();
    if(!config) {
        SAH_TRACEZ_ERROR(ME, "Prestart config NULL!");
        return;
    }
    amxc_var_delete(&(config->ctr_create));
    amxc_var_delete(&(config->sb_create));
}

int cthulhu_init(void) {
    int res = -1;
    amxd_object_t* config = NULL;
    char* backend_path = NULL;
    char* storage_location = NULL;
    amxp_timer_new(&update_timer, update_timer_cb, NULL);
    amxp_timer_set_interval(update_timer, update_poll_time);
    // get the initial state of lxc containers after 2s
    amxp_timer_start(update_timer, 2000);
    cthulhu_app_t* app_data = cthulhu_get_app_data();
    cthulhu_sigmngrs_init(&ctr_sigmngrs);
    amxc_set_new(&ctr_start_cb_underway_set, false);

    config = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONFIG);
    if(config) {
        backend_path = amxd_object_get_cstring_t(config, CTHULHU_CONFIG_DEFAULTBACKEND, NULL);
        storage_location = amxd_object_get_cstring_t(config, CTHULHU_CONFIG_STORAGE_LOCATION, NULL);
        if(!string_is_empty(backend_path)) {
            SAH_TRACEZ_NOTICE(ME, "Load backend %s", backend_path);
            cthulhu_load_backend(backend_path);
        }
        app_data->image_location = amxd_object_get_cstring_t(config, CTHULHU_CONFIG_IMAGE_LOCATION, NULL);
        app_data->blob_location = amxd_object_get_cstring_t(config, CTHULHU_CONFIG_BLOB_LOCATION, NULL);
        app_data->bundle_location = amxd_object_get_cstring_t(config, CTHULHU_CONFIG_BUNDLE_LOCATION, NULL);
        if(storage_location) {
            if(asprintf(&(app_data->rootfs_location), "%s/%s", storage_location, "rootfs") < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not init string app_data->rootfs_location");
                goto exit;
            }
            if(asprintf(&(app_data->data_location), "%s/%s", storage_location, "data") < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not init string app_data->data_location");
                goto exit;
            }
            if(asprintf(&(app_data->sb_host_data_location), "%s/%s", storage_location, "sb_host_data") < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not init string app_data->sb_host_data_location");
                goto exit;
            }
            if(asprintf(&(app_data->ctr_host_data_location), "%s/%s", storage_location, "ctr_host_data") < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not init string app_data->ctr_host_data_location");
                goto exit;
            }
            if(asprintf(&(app_data->layer_location), "%s/%s", storage_location, "layers") < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not init string app_data->layer_location");
                goto exit;
            }
            if(asprintf(&(app_data->syslogng_location), "%s/%s", storage_location, "syslogng") < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not init string app_data->syslogng_location");
                goto exit;
            }
            if(asprintf(&(app_data->bundle_extract_location), "%s/%s", storage_location, "bundles") < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not init string app_data->bundle_extract_location");
                goto exit;
            }
            if(cthulhu_mkdir(app_data->rootfs_location, true) < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not make rootdir [%s]", app_data->rootfs_location);
                goto exit;
            }
            if(cthulhu_mkdir(app_data->data_location, true) < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not make data dir [%s]", app_data->data_location);
                goto exit;
            }
            if(cthulhu_mkdir(app_data->sb_host_data_location, true) < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not make data dir [%s]", app_data->sb_host_data_location);
                goto exit;
            }
            if(cthulhu_mkdir(app_data->ctr_host_data_location, true) < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not make data dir [%s]", app_data->ctr_host_data_location);
                goto exit;
            }
            if(cthulhu_mkdir(app_data->layer_location, true) < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not make data dir [%s]", app_data->layer_location);
                goto exit;
            }
            if(cthulhu_mkdir(app_data->syslogng_location, true) < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not make data dir [%s]", app_data->syslogng_location);
                goto exit;
            }
        }
    }
    cthulhu_unpriv_init();
    cthulhu_worker_init();
    cthulhu_syslogng_init();
    cthulhu_update_versions();
    cthulhu_get_backend_information();
    cthulhu_backend_notif_init();
    cthulhu_prestart_init();
    cthulhu_plugins_init();
    cthulhu_bundles_init();
    cthulhu_dm_changed_init();
    cthulhu_backend_init_functions();

    res = 0;
exit:
    free(backend_path);
    free(storage_location);
    return res;
}

static int cb_container_cleanup(UNUSED amxd_object_t* templ,
                                amxd_object_t* instance,
                                UNUSED void* priv) {
    delete_ctr_priv(instance);
    cthulhu_dm_changed_cleanup();

    return 0;
}

static int cb_sb_cleanup(UNUSED amxd_object_t* templ,
                         amxd_object_t* instance,
                         UNUSED void* priv) {
    char* sb_id = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_ID, NULL);
    cthulhu_notif_data_t* notif_data = NULL;

    cthulhu_notif_data_new(&notif_data, instance, cthulhu_notif_cmd_none, NULL);
    cthulhu_sb_stop(sb_id, notif_data);

    cthulhu_notif_data_delete(&notif_data);
    delete_sb_priv(instance);
    free(sb_id);
    return 0;
}


void cthulhu_cleanup(void) {
    cthulhu_worker_cleanup();
    cthulhu_plugins_cleanup();
    cthulhu_app_t* app_data = cthulhu_get_app_data();
    amxp_timer_stop(update_timer);
    amxp_timer_delete(&update_timer);
    // cleanup ee
    amxd_object_t* sandboxes = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_SANDBOX_INSTANCES);
    amxd_object_for_all(sandboxes, "*", cb_sb_cleanup, NULL);
    // cleanup containers
    amxd_object_t* containers = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES);
    amxd_object_for_all(containers, "*", cb_container_cleanup, NULL);
    amxm_so_close(&backend);
    cthulhu_sigmngrs_clean(&ctr_sigmngrs);
    amxc_set_delete(&ctr_start_cb_underway_set);
    cthulhu_prestart_cleanup();

    free(app_data->image_location);
    free(app_data->blob_location);
    free(app_data->bundle_location);
    free(app_data->rootfs_location);
    free(app_data->layer_location);
    free(app_data->data_location);
    free(app_data->sb_host_data_location);
    free(app_data->ctr_host_data_location);
    free(app_data->syslogng_location);
    free(app_data->bundle_extract_location);
}

