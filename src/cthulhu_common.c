/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <errno.h>
#include <limits.h>

#include <cthulhu/cthulhu_defines.h>
#include "cthulhu_priv.h"
#define ME "common"

static void crt_create_done(const amxc_var_t* const data, void* const priv) {
    amxd_status_t status = (amxd_status_t) GET_INT32(data, "status");
    unsigned int* active = (unsigned int*) priv;

    (*active)--;
    if(amxd_status_ok != status) {
        SAH_TRACEZ_ERROR(ME, "restore container failed '%d': %s", status, amxd_status_string(status));
    }

    if((*active) == 0) {
        SAH_TRACEZ_INFO(ME, "Restoring config done, trigger cthulhu initialize event");
        free(active);
        cthulhu_trigger_initialize_event();
    } else {
        SAH_TRACEZ_NOTICE(ME, "Still %u container to be restored", *active);
    }
}

int cthulhu_create_prestart_config(void) {
    cthulhu_prestartup_config_t* config = cthulhu_get_prestartup_config();

    if(!config) {
        SAH_TRACEZ_INFO(ME, "No config to be created");
        return 0;
    }
    amxd_dm_t* dm = cthulhu_get_dm();

    if(config->sb_create) {
        /* Create the list of Sandboxes */
        amxd_object_t* sb_instances_obj = amxd_dm_findf(dm, CTHULHU_DM_SANDBOX ".");
        const amxc_llist_t* sandboxes = amxc_var_get_const_amxc_llist_t(config->sb_create);
        if(!sandboxes) {
            SAH_TRACEZ_ERROR(ME, "No sandbox list");
        } else {
            amxc_var_t* inst = NULL;
            amxc_llist_for_each(it, sandboxes) {
                inst = amxc_llist_it_get_data(it, amxc_var_t, lit);
                if(amxc_var_type_of(inst) == AMXC_VAR_ID_HTABLE) {
                    amxc_var_t* fn_rv = NULL;
                    amxc_var_new(&fn_rv);

                    SAH_TRACEZ_INFO(ME, "Creating sandbox [%s]", GET_CHAR(inst, CTHULHU_SANDBOX_ID));
                    amxd_status_t retval = amxd_object_invoke_function(sb_instances_obj, CTHULHU_CMD_SB_CREATE, inst, fn_rv);
                    if(retval != amxd_status_ok) {
                        SAH_TRACEZ_ERROR(ME, "Failed to create sandbox: (%d) %s", retval, amxd_status_string(retval));
                    }

                    amxc_var_delete(&fn_rv);
                }
            }
        }
    }

    unsigned int* active = (unsigned int*) calloc(1, sizeof(unsigned int));
    if(config->ctr_create) {
        /* Create the list of containers */
        amxd_object_t* ctr_instances_obj = amxd_dm_findf(dm, CTHULHU_DM_CONTAINER ".");
        const amxc_llist_t* containers = amxc_var_get_const_amxc_llist_t(config->ctr_create);
        if(!containers) {
            SAH_TRACEZ_ERROR(ME, "No container list");
        } else {
            amxc_var_t* inst = NULL;
            amxc_llist_for_each(it, containers) {
                inst = amxc_llist_it_get_data(it, amxc_var_t, lit);
                if(amxc_var_type_of(inst) == AMXC_VAR_ID_HTABLE) {
                    amxc_var_t* fn_rv = NULL;
                    amxc_var_new(&fn_rv);

                    SAH_TRACEZ_INFO(ME, "Creating container [uuid: %s]", GET_CHAR(inst, CTHULHU_CONTAINER_LINKED_UUID));
                    amxd_status_t retval = amxd_object_invoke_function(ctr_instances_obj, CTHULHU_CMD_CTR_CREATE, inst, fn_rv);
                    if(retval == amxd_status_deferred) {
                        uint64_t call_id = amxc_var_constcast(uint64_t, fn_rv);
                        if(amxd_status_ok != amxd_function_set_deferred_cb(call_id, crt_create_done, active)) {
                            SAH_TRACEZ_ERROR(ME, "Failed to set deferred callback");
                        } else {
                            (*active)++;
                        }
                    } else {
                        SAH_TRACEZ_ERROR(ME, "Failed to invoke container create: (%d) %s", retval, amxd_status_string(retval));
                    }

                    amxc_var_delete(&fn_rv);
                }
            }
        }
    }

    if(*active == 0) {
        cthulhu_trigger_initialize_event();
        free(active);
    }

    return 0;
}

int cthulhu_metadata_fill(amxc_var_t* metadata, amxc_var_t* args, const char* metadata_params[], size_t param_cnt) {
    int ret = -1;
    when_null(metadata, exit);
    when_null(args, exit);
    ret = 0;

    for(size_t i = 0; i < param_cnt; i++) {
        amxc_var_t* var = GET_ARG(args, metadata_params[i]);
        if(!var) {
            continue;
        }
        amxc_var_set_key(metadata, metadata_params[i], var, AMXC_VAR_FLAG_COPY);
        ret++;
    }

exit:
    return ret;
}

int cthulhu_dm_args_fill(amxc_var_t* args, amxc_var_t* dm_params, const char* metadata_params[], const char* metadata_dm_params[], size_t param_cnt) {
    int ret = -1;
    when_null(args, exit);
    when_null(dm_params, exit);
    ret = 0;

    for(size_t i = 0; i < param_cnt; i++) {
        amxc_var_t* var = GET_ARG(args, metadata_params[i]);
        amxc_var_t* dm_var = GET_ARG(dm_params, metadata_dm_params[i]);
        if(var) {
            continue;
        }
        if(!dm_var) {
            SAH_TRACEZ_ERROR(ME, "Cannot find '%s' in the dm", metadata_params[i]);
            ret--;
            continue;
        }
        amxc_var_set_key(args, metadata_params[i], dm_var, AMXC_VAR_FLAG_COPY);
    }
exit:
    return ret;
}
/**
 * @brief Get the layer directory for a layer
 *
 * combine the base dir and the layer name to get a layer directory.
 * the layer name can contain semicolons, i.e. "sha256:23344522", these
 * will create subdirectories in the output
 *
 * @param basedir
 * @param layer
 * @return char*
 */
char* cthulhu_layer_get_dir_from_name(const char* basedir, const char* layer) {
    char* p = NULL;
    char* layer_dir = NULL;
    char* layer_tmp = NULL;
    ASSERT_NOT_NULL(basedir, goto exit);
    ASSERT_NOT_NULL(layer, goto exit);
    layer_dir = malloc(PATH_MAX);
    layer_tmp = strdup(layer);

    p = layer_tmp;
    while(p) {
        p = strchr(p, ':');
        if(p) {
            *p = '/';
        }
    }

    if(snprintf(layer_dir, PATH_MAX, "%s/%s", basedir, layer_tmp) >= PATH_MAX) {
        SAH_TRACEZ_ERROR(ME, "layer_dir path is too long (%s: %s)", basedir, layer);
        free_null(layer_dir);
        goto exit;
    }
exit:
    free(layer_tmp);
    return layer_dir;
}

