/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <signal.h>

#include <cthulhu/cthulhu_defines.h>

#include <unistd.h>
#include "cthulhu_priv.h"
#include "cthulhu_notif.h"
#include "cthulhu_unprivileged.h"

#define ME "main"

static cthulhu_app_t app;
static int is_stopping = 0;

/**
 * @brief since cthulhu uses forks to offload processing, and the datamodel should not
 * be accessed from a forked process, we add a guard ro prevent that the
 * datamodel is accessed from another process then the main one
 *
 * @return true if called from the main cthulhu process
 * @return false if called from a forked cthulhu process
 */
static bool cthulhu_is_main_process(void) {
    return app.main_process_pid == getpid();
}


amxd_dm_t* cthulhu_get_dm(void) {
    if(!cthulhu_is_main_process()) {
        SAH_TRACEZ_ERROR(ME, "Function is not called from main process");
        return NULL;
    }
    return app.dm;
}

lcm_worker_t* cthulhu_get_worker(void) {
    return app.worker;
}

cthulhu_app_t* cthulhu_get_app_data(void) {
    return &app;
}

amxo_parser_t* cthulhu_get_parser(void) {
    if(!cthulhu_is_main_process()) {
        SAH_TRACEZ_ERROR(ME, "Function is not called from main process");
        return NULL;
    }
    return app.parser;
}

amxd_object_t* cthulhu_root_get(void) {
    if(!cthulhu_is_main_process()) {
        SAH_TRACEZ_ERROR(ME, "Function is not called from main process");
        return NULL;
    }
    return amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM ".");
}

cthulhu_prestartup_config_t* cthulhu_get_prestartup_config(void) {
    return &app.prestartup_config;
}

static void cthulhu_sb_stop_all_cb(void) {
    SAH_TRACEZ_INFO("ME", "Continue stopping");
    cthulhu_cleanup();
    cthulhu_sb_cleanup();
    cthulhu_overlayfs_cleanup();
    exit(EXIT_SUCCESS);
}

static void cthulhu_sig_handler(int UNUSED signo) {
    is_stopping++;

    if(is_stopping == 1) {
        cthulhu_sb_stop_all(cthulhu_sb_stop_all_cb);
    }
    if(is_stopping == 3) {
        SAH_TRACEZ_ERROR(ME, "Force exit");
        exit(EXIT_SUCCESS);
    }
}

static void cthulhu_sighandler_init(void) {
    signal(SIGINT, cthulhu_sig_handler);
    signal(SIGTERM, cthulhu_sig_handler);
    signal(SIGABRT, cthulhu_sig_handler);
}

int _cthulhu_main(int reason,
                  amxd_dm_t* dm,
                  amxo_parser_t* parser) {

    switch(reason) {
    case 0:     // START
        memset(&app, 0, sizeof(cthulhu_app_t));
        app.main_process_pid = getpid();
        app.dm = dm;
        app.parser = parser;
        cthulhu_overlayfs_init();
        cthulhu_cgroup_init();
        if(cthulhu_init() < 0) {
            return -1;
        }
        cthulhu_sighandler_init();
        SAH_TRACE_INFO(CTHULHU " service started");
        break;
    case 1:     // STOP
        cthulhu_cleanup();
        cthulhu_sb_cleanup();
        cthulhu_overlayfs_cleanup();
        app.dm = NULL;
        app.parser = NULL;
        SAH_TRACE_INFO(CTHULHU " service stopped");
        break;
    }

    return 0;
}

/**
 * @brief Slot to put Cthulhu to initialized
 */
static void slot_put_cthulhu_initialized(UNUSED const amxc_var_t* const data,
                                         UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "Initialized");
    cthulhu_notif_initialized();
    cthulhu_set_dm_info_initialized();
}

void cthulhu_trigger_initialize_event(void) {
    // Put cthulhu Initialized on next event on eventloop
    amxp_sigmngr_deferred_call(NULL, slot_put_cthulhu_initialized, NULL, NULL);
}

/**
 * @brief Called when default or saved ODL file is loaded
 *
 * The loading strategy is as follows:
 * - the cthulhu_config.odl file is loaded because if ah include directive in cthulhu.odl
 * - this cthulhu_confif.odl contains the location of the plugins
 * - each plugin will load its required mibs
 * - since the mibs are loaded, the odl.dm-load event will be able to load the mibs defined in the saved odl
 *
 * For this to work
 * - a read-only persistent parameter Information.odlLoaded is created in cthulhu_definition.odl defaulting to 'false'
 * - This parameter is set to 'true' in cthulhu_defaults.odl and will be saved as such in the saved odl file
 * - the odl.load-dm-events = true is set to enable events of changing parameters during loading
 * - an event is defined in cthulhu_defaults.odl that will call this function when it changes to 'true'
 *
 */
void _odl_loaded(UNUSED const char* const sig_name,
                 UNUSED const amxc_var_t* const data,
                 UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "ODL Loaded");

    // odl loaded hook
    cthulhu_plugin_odl_loaded();

    // start all sandboxes that are enabled and do not have a parent
    cthulhu_sb_init();

    // Configure all sanboxes/containers configured as part of the prestart config
    cthulhu_create_prestart_config();
}
