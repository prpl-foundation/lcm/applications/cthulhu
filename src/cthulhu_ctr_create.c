/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <errno.h>

#include "cthulhu_priv.h"
#include "cthulhu_unprivileged.h"
#include <cthulhu/cthulhu_defines.h>
#include <cthulhu/cthulhu_config.h>
#include <cthulhu/cthulhu_config_variant.h>
#include <cthulhu/cthulhu_helpers.h>
#include <lcm/amxc_data.h>
#include <lcm/lcm_assert.h>
#include <amxj/amxj_variant.h>

#define ME "dm"

#define CTR_ID "ctr_id"
#define BUNDLE_NAME "bundle_name"
#define BUNDLE_EXTRACT_LOCATION "bundle_extract_location"
#define BUNDLE_LOCATION "bundle_location"
#define STORAGE_LOCATION "storage_location"
#define LAYER_LOCATION "layer_location"
#define CTR_ROOTFS "ctr_rootfs"
#define CONFIG "config"
#define LAYER_INDEX "layer_index"
#define LAYER_NAME "layer_name"
#define IMAGE_MANIFEST "image_manifest"
#define LAYER_DIGESTS "layer_digests"
#define NOTIF_DATA "notif_data"
#define DEFERRED_CALL_ID "deferred_call_id"
#define UPDATE "update"
#define CHANGE_OWNERSHIP "change_ownership"
#define NEW_UID "new_uid"
#define NEW_GID "new_gid"

#define GET_UINT64(a, n) amxc_var_dyncast(uint64_t, n == NULL ? a : GET_ARG(a, n))

/** DECLARATIONS */
static cthulhu_action_res_t cthulhu_build_container(cthulhu_notif_data_t* notif_data,
                                                    const char* ctr_id,
                                                    const char* bundle_name,
                                                    const char* bundle_version,
                                                    const char* disklocation,
                                                    const char* sb_id,
                                                    const amxc_var_t* const metadata,
                                                    uint64_t deferred_call_id,
                                                    bool update);
static void cthulhu_update_manifest_params(const char* ctr_id,
                                           image_spec_schema_image_manifest_schema* manifest_schema);
static int cthulhu_ctr_apply_metadata(const char* ctr_id, const amxc_var_t* const metadata);
static int cthulhu_extract_layers(lcm_worker_t* worker, amxc_var_t* var, int rc, amxc_var_t* data);
static int cthulhu_add_layers_to_dm(const amxc_var_t* layer_digests, const char* ctr_id);
static int cthulhu_build_container_layers_extracted(lcm_worker_t* worker, amxc_var_t* var, int rc, amxc_var_t* data);
static int cthulhu_config_fill(image_spec_schema_config_schema_config* image_config,
                               cthulhu_config_t* cthulhu_config);
static int cthulhu_extract_bundle_wrapper(lcm_worker_t* worker, amxc_var_t* var, int rc, amxc_var_t* data);
static int cthulhu_build_container_bundle_extracted(lcm_worker_t* worker, amxc_var_t* var, int rc, amxc_var_t* data);
static int cthulhu_backend_create(const char* ctr_id, cthulhu_config_t* cthulhu_config, cthulhu_notif_data_t* notif_data, bool update);
static void cthulhu_ctr_add_environmentvariables_to_dm(const char* ctr_id, const char* key, const char* value, bool isOCIEnv);
static int cthulhu_ctr_apply_metadata_envvar(const char* ctr_id, const amxc_var_t* const metadata);
/** END DECLARATIONS*/

static char* cthulhu_ctr_priv_sb_create(const char* ctr_id, const char* sb_id, cthulhu_notif_data_t* notif_data) {
    char* new_sb_id = NULL;

    if(asprintf(&new_sb_id, "__%s_%s__", sb_id, ctr_id) < 0) {
        printf("Could not allocated mem for string");
        goto exit;
    }
    if(cthulhu_sb_get(new_sb_id)) {
        SAH_TRACEZ_INFO(ME, "CTR[%s]: Private sandbox already exists. Reuse storage", ctr_id);
        goto exit;
    }
    if(cthulhu_sb_create(new_sb_id, sb_id, NULL, NULL, false, 100, -1, -1, true, notif_data, NULL) != cthulhu_action_res_done) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: could not create private container sandbox [%s]", ctr_id, new_sb_id);
        free(new_sb_id);
        new_sb_id = NULL;
    }
exit:
    return new_sb_id;
}

/**
 * @brief        cthulhu_ctr_remove_all_environmentvariables
 *
 * @detail       Remove all EnvironmentVariables from a container
 * @param[in]    ctr_obj, Container Object
 * @retval       0 in success, -1 on failure.
 */
int cthulhu_ctr_remove_all_environmentvariables(amxd_object_t* ctr_obj) {
    int res = -1;
    amxd_object_t* environmentvariables_templ = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES);
    if(!environmentvariables_templ) {
        SAH_TRACEZ_ERROR(ME, "Could not get the EnvironmentVariables");
        goto exit;
    }
    amxd_object_for_each(instance, it_environmentvariables, environmentvariables_templ) {
        amxd_object_t* environmentvariables_obj = amxc_llist_it_get_data(it_environmentvariables, amxd_object_t, it);
        amxd_object_free(&environmentvariables_obj);
    }
    res = 0;
exit:
    return res;
}

static void cthulhu_ctr_add_hostobject_to_dm(const char* ctr_id,
                                             const char* source,
                                             const char* destination,
                                             const char* options
                                             ) {
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* hostobject_instances_obj = NULL;
    amxd_object_t* hostobject_instance = NULL;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_dm_t* dm = cthulhu_get_dm();

    amxd_trans_init(&trans);

    ASSERT_NOT_NULL(source, goto exit);
    ASSERT_NOT_NULL(destination, goto exit);
    ASSERT_FALSE(string_is_empty(destination), goto exit);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit);

    hostobject_instances_obj = amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_HOSTOBJECT);
    if(!hostobject_instances_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: HostObjects not found", ctr_id);
        goto exit;
    }
    hostobject_instance = amxd_object_findf(hostobject_instances_obj, "[" CTHULHU_CONTAINER_HOSTOBJECT_DESTINATION " == '%s'].", destination);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    if(hostobject_instance) {
        // modify existing instance
        amxd_trans_select_object(&trans, hostobject_instance);
    } else {
        // create new instance
        amxd_trans_select_object(&trans, hostobject_instances_obj);
        amxd_trans_add_inst(&trans, 0, NULL);
    }

    amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_HOSTOBJECT_SOURCE, source);
    amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_HOSTOBJECT_DESTINATION, destination);
    amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_HOSTOBJECT_OPTIONS, options);

    status = amxd_trans_apply(&trans, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Transaction failed. HostObject not added. Status: %d", ctr_id, status);
    }

exit:
    amxd_trans_clean(&trans);
}

static char* cthulhu_config_get_string(const char* key) {
    char* res = NULL;
    amxd_object_t* config = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONFIG);
    ASSERT_NOT_NULL(config, goto exit, CTHULHU_DM_CONFIG " not found in the datamodel");
    res = amxd_object_get_cstring_t(config, key, NULL);
exit:
    return res;
}

static void cthulhu_ctr_add_mount_to_dm(const char* ctr_id,
                                        const char* source,
                                        const char* destination,
                                        const char* options
                                        ) {
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* mount_instances_obj = NULL;
    amxd_object_t* mount_instance = NULL;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_dm_t* dm = cthulhu_get_dm();

    amxd_trans_init(&trans);

    ASSERT_NOT_NULL(source, goto exit);
    ASSERT_NOT_NULL(destination, goto exit);
    ASSERT_FALSE(string_is_empty(destination), goto exit);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit);

    mount_instances_obj = amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_MOUNTS);
    if(!mount_instances_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Mounts not found", ctr_id);
        goto exit;
    }
    mount_instance = amxd_object_findf(mount_instances_obj, "[" CTHULHU_CONTAINER_MOUNTS_DESTINATION " == '%s'].", destination);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    if(mount_instance) {
        // modify existing instance
        amxd_trans_select_object(&trans, mount_instance);
    } else {
        // create new instance
        amxd_trans_select_object(&trans, mount_instances_obj);
        amxd_trans_add_inst(&trans, 0, NULL);
    }

    amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_MOUNTS_SOURCE, source);
    amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_MOUNTS_DESTINATION, destination);
    if(strcmp(source, "tmpfs") == 0) {
        amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_MOUNTS_TYPE, "tmpfs");
    } else {
        amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_MOUNTS_TYPE, "none");
    }
    if(!string_is_empty(options)) {
        amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_MOUNTS_OPTIONS, options);
    } else {
        char* mountoptions = NULL;
        if(strcmp(source, "tmpfs") == 0) {
            mountoptions = cthulhu_config_get_string(CTHULHU_CONFIG_HOSTOBJECTSMOUNTOPTIONSTMPFS);
        } else {
            mountoptions = cthulhu_config_get_string(CTHULHU_CONFIG_HOSTOBJECTSMOUNTOPTIONS);
        }
        if(!STRING_EMPTY(mountoptions)) {
            amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_MOUNTS_OPTIONS, mountoptions);
        } else {
            SAH_TRACEZ_WARNING(ME, "%s or %s is empty", CTHULHU_CONFIG_HOSTOBJECTSMOUNTOPTIONS, CTHULHU_CONFIG_HOSTOBJECTSMOUNTOPTIONSTMPFS);
        }
        free(mountoptions);
    }
    status = amxd_trans_apply(&trans, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Transaction failed. Mount not added. Status: %d", ctr_id, status);
    }

exit:
    amxd_trans_clean(&trans);
}

static int cthulhu_ctr_add_device_to_dm(const char* ctr_id,
                                        const char* source,
                                        const char* destination,
                                        const amxc_var_t* options_var) {
    int res = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* sb_obj = NULL;
    char* priv_sb_id = NULL;
    amxd_object_t* device_instances_obj = NULL;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_dm_t* dm = cthulhu_get_dm();

    amxc_var_t* var = NULL;
    const char* devicetype = NULL;
    const char* access = NULL;
    uint32_t major = 0;
    uint32_t minor = 0;
    bool create = false;

    amxd_trans_init(&trans);

    ASSERT_NOT_NULL(source, goto exit);
    ASSERT_NOT_NULL(destination, goto exit);
    ASSERT_FALSE(string_is_empty(destination), goto exit);

    // get options
    access = GET_CHAR(options_var, "access");
    if(!access) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: access is not provided in HostObject options", ctr_id);
        goto exit;
    }
    devicetype = GET_CHAR(options_var, "devicetype");
    if(!devicetype) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: devicetype is not provided in HostObject options", ctr_id);
        goto exit;
    }
    var = GET_ARG(options_var, "major");
    if(!var) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: major is not provided in HostObject options", ctr_id);
        goto exit;
    }
    major = GET_UINT32(var, NULL);

    var = GET_ARG(options_var, "minor");
    if(!var) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: minor is not provided in HostObject options", ctr_id);
        goto exit;
    }
    minor = GET_UINT32(var, NULL);

    var = GET_ARG(options_var, "create");
    if(var) {
        create = GET_BOOL(var, NULL);
    }

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit);

    priv_sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_PRIVATESANDBOX, NULL);
    ASSERT_NOT_NULL(priv_sb_id, goto exit);
    sb_obj = cthulhu_sb_get(priv_sb_id);
    ASSERT_NOT_NULL(sb_obj, goto exit);

    device_instances_obj = amxd_object_findf(sb_obj, CTHULHU_SANDBOX_DEVICES);
    if(!device_instances_obj) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Devices not found", priv_sb_id);
        goto exit;
    }

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, device_instances_obj);
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_cstring_t(&trans, CTHULHU_SANDBOX_DEVICES_DEVICE, destination);
    amxd_trans_set_cstring_t(&trans, CTHULHU_SANDBOX_DEVICES_TYPE, devicetype);
    amxd_trans_set_cstring_t(&trans, CTHULHU_SANDBOX_DEVICES_ACCESS, access);
    amxd_trans_set_uint32_t(&trans, CTHULHU_SANDBOX_DEVICES_MAJOR, major);
    amxd_trans_set_uint32_t(&trans, CTHULHU_SANDBOX_DEVICES_MINOR, minor);
    amxd_trans_set_bool(&trans, CTHULHU_SANDBOX_DEVICES_CREATE, create);

    status = amxd_trans_apply(&trans, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Transaction failed. Mount not added. Status: %d", ctr_id, status);
    }
    res = 0;
exit:
    amxd_trans_clean(&trans);
    free(priv_sb_id);
    return res;
}

/**
 * @brief        cthulhu_ctr_add_environmentvariables_to_dm
 *
 * @detail       Applies EnvironmentVariables configuration in cthulhu data-model
 * @param[in]    ctr_id, Container id
 * @param[in]    key, EnvironmentVariable name
 * @param[in]    value, EnvironmentVariable value
 * @param[in]    isOCIEnv, Boolean value to Env Origin type User supplied or from OCI Layer
 * @retval       void
 */
static void cthulhu_ctr_add_environmentvariables_to_dm(const char* ctr_id, const char* key, const char* value, bool isOCIEnv) {
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* environmentvariables_instances_obj = NULL;
    amxd_object_t* environmentvariables_instance = NULL;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_dm_t* dm = cthulhu_get_dm();

    amxd_trans_init(&trans);

    ASSERT_NOT_NULL(key, goto exit);
    ASSERT_NOT_NULL(value, goto exit);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        goto exit;
    }
    ASSERT_NOT_NULL(ctr_obj, goto exit);

    environmentvariables_instances_obj = amxd_object_findf(ctr_obj, isOCIEnv ?
                                                           CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI :
                                                           CTHULHU_CONTAINER_ENVIRONMENTVARIABLES);
    if(!environmentvariables_instances_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: %s EnvironmentVariables not found", ctr_id, isOCIEnv ? "Container" : "");
        goto exit;
    }

    environmentvariables_instance = amxd_object_findf(environmentvariables_instances_obj, isOCIEnv ?
                                                      "[" CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI_VALUE " == '%s']." :
                                                      "[" CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_VALUE " == '%s'].", key);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    environmentvariables_instances_obj = amxd_object_get(ctr_obj, isOCIEnv ?
                                                         CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI :
                                                         CTHULHU_CONTAINER_ENVIRONMENTVARIABLES);

    if(environmentvariables_instance) {
        // modify existing instance
        amxd_trans_select_object(&trans, environmentvariables_instance);
    } else {
        //create new instance
        amxd_trans_select_object(&trans, environmentvariables_instances_obj);
        amxd_trans_add_inst(&trans, 0, NULL);
    }

    amxd_trans_set_cstring_t(&trans, isOCIEnv ? CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI_KEY :
                             CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_KEY, key);
    amxd_trans_set_cstring_t(&trans, isOCIEnv ? CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI_VALUE :
                             CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_VALUE, value);

    status = amxd_trans_apply(&trans, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Transaction failed. EnvironmentVariables not added. Status: %d", ctr_id, status);
    }

exit:
    amxd_trans_clean(&trans);
}

static void cthulhu_ctr_add_defaults(const char* ctr_id) {
    cthulhu_ctr_add_mount_to_dm(ctr_id, "tmpfs", "tmp", "rw,nodev,nosuid,noatime,create=dir");
    cthulhu_ctr_add_mount_to_dm(ctr_id, "tmpfs", "run", "rw,nodev,nosuid,noatime,create=dir");
}

cthulhu_action_res_t cthulhu_ctr_create(const char* ctr_id,
                                        const char* linked_uuid,
                                        const char* bundle_name,
                                        const char* bundle_version,
                                        const char* disklocation,
                                        const char* sb_id,
                                        cthulhu_notif_data_t* notif_data,
                                        bool update,
                                        const amxc_var_t* const metadata,
                                        amxc_ts_t* ts_created,
                                        uint64_t deferred_call_id,
                                        const char* module_version) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* ctr_obj = NULL;
    char* priv_sb_id = NULL;
    cthulhu_notif_data_t* notif_data_no_id = NULL;
    amxc_ts_t ts_now;
    amxc_ts_t ts_zero = {0, 0, 0};

    if(!update) {
        // check if there is already a container with this id
        ctr_obj = cthulhu_ctr_get(ctr_id);
        if(ctr_obj != NULL) {
            SAH_TRACEZ_WARNING(ME, "CTR[%s]: Cannot create container since it already exists", ctr_id);
            res = cthulhu_action_res_done;
            goto exit;
        }
    }
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);
    if((priv_sb_id = cthulhu_ctr_priv_sb_create(ctr_id, sb_id, notif_data_no_id)) == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "CTR[%s]: Could not create private sandbox", ctr_id);
        goto exit;
    }

    if(amxc_ts_now(&ts_now)) {
        SAH_TRACEZ_ERROR(ME, "Cannot get current date");
    }
    // convert to UTC
    ts_now.offset = 0;

    // store initial values in DM
    amxc_var_t params;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_BUNDLE, bundle_name);
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_BUNDLEVERSION, bundle_version);
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_SANDBOX, str_or_empty(sb_id));
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_PRIVATESANDBOX, priv_sb_id);
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_LINKED_UUID, str_or_empty(linked_uuid));
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_DISKLOCATION, disklocation);
    if(module_version) {
        amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_MODULEVERSION, module_version);
    }

    if(update) {
        amxc_var_add_new_key_amxc_ts_t(&params, CTHULHU_CONTAINER_CREATED, ts_created);
        amxc_var_add_new_key_amxc_ts_t(&params, CTHULHU_CONTAINER_UPDATED, &ts_now);
        amxc_var_add_new_key_amxc_ts_t(&params, CTHULHU_CONTAINER_STARTTIME, &ts_zero);
    } else {
        amxc_ts_t ts_empty;
        ts_empty.nsec = 0;
        ts_empty.sec = 0;
        ts_empty.offset = 0;
        amxc_var_add_new_key_amxc_ts_t(&params, CTHULHU_CONTAINER_UPDATED, &ts_empty);
        amxc_var_add_new_key_amxc_ts_t(&params, CTHULHU_CONTAINER_CREATED, &ts_now);
    }
    cthulhu_ctr_update_dm(ctr_id, &params);
    amxc_var_clean(&params);

    cthulhu_ctr_add_defaults(ctr_id);

    res = cthulhu_build_container(notif_data, ctr_id, bundle_name, bundle_version, disklocation, priv_sb_id, metadata, deferred_call_id,
                                  update);

exit:
    cthulhu_notif_data_delete(&notif_data_no_id);
    free(priv_sb_id);
    return res;
}

static int imagemanifestlayers_to_var(amxc_var_t* var_list, image_spec_schema_image_manifest_schema* image_manifest) {
    int res = -1;
    ASSERT_NOT_NULL(var_list, goto exit);
    ASSERT_NOT_NULL(image_manifest, goto exit);

    ASSERT_SUCCESS(amxc_var_set_type(var_list, AMXC_VAR_ID_LIST), goto exit);
    for(size_t i = 0; i < image_manifest->layers_len; i++) {
        ASSERT_NOT_NULL(image_manifest->layers[i], goto exit, "The image manifest content descriptor is empty for index %zu", i);
        ASSERT_STR_NOT_EMPTY(image_manifest->layers[i]->digest, goto exit, "The image manifest content descriptor digest is empty for index %zu", i);
        amxc_var_add_new_cstring_t(var_list, image_manifest->layers[i]->digest);
    }
    res = 0;
exit:
    return res;
}

static cthulhu_action_res_t cthulhu_build_container(cthulhu_notif_data_t* notif_data,
                                                    const char* ctr_id,
                                                    const char* bundle_name,
                                                    const char* bundle_version,
                                                    const char* disklocation,
                                                    const char* sb_id,
                                                    const amxc_var_t* const metadata,
                                                    uint64_t deferred_call_id,
                                                    bool update) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    cthulhu_config_t* cthulhu_config = NULL;
    image_spec_schema_image_manifest_schema* image_manifest = NULL;
    cthulhu_app_t* app_data = cthulhu_get_app_data();
    const char* bundle_location = string_is_empty(disklocation) ? bundle_name : disklocation;
    amxm_shared_object_t* backend = NULL;
    lcm_worker_task_t* task = NULL;
    amxc_var_t* extract_params = NULL;
    amxc_var_t* cb_params = NULL;
    bool change_ownership = false;
    uid_t new_uid = 0;
    uid_t new_gid = 0;

    SAH_TRACEZ_INFO(ME, "CTR[%s]: Create container from %s:%s (location: %s)",
                    ctr_id, bundle_name, bundle_version, bundle_location);

    backend = cthulhu_backend_get();
    if(!backend) {
        SAH_TRACEZ_ERROR(ME, "No backend loaded");
        goto exit;
    }
    if(cthulhu_overlayfs_supported()) {
        // unmount if it is mounted
        cthulhu_overlayfs_umount_rootfs(ctr_id, true);
    }

    cthulhu_config_new(&cthulhu_config);


    cthulhu_config->id = strdup(ctr_id);
    cthulhu_config->bundle_name = strdup(bundle_name);
    cthulhu_config->bundle_version = strdup(bundle_version);
    if(sb_id) {
        cthulhu_config->sandbox = strdup(sb_id);
        char* cgroup_dir = cthulhu_cgroup_dir_get(sb_id);
        if(cgroup_dir) {
            cthulhu_config->cgroup_dir = cgroup_dir;
        } else {
            cthulhu_config->cgroup_dir = strdup("");
        }
    } else {
        cthulhu_config->sandbox = strdup("");
        cthulhu_config->cgroup_dir = strdup("");
    }

    cthulhu_config->rootfs = cthulu_overlayfs_get_rootfs(ctr_id);
    ASSERT_STR_NOT_EMPTY(cthulhu_config->rootfs, goto error, "CTR[%s]: Could not get the rootfs string", ctr_id);

    if(cthulhu_use_bundles()) {
        cthulhu_ctr_apply_metadata(ctr_id, metadata);
        ASSERT_SUCCESS(cthulhu_unpriv_create_user(ctr_id), goto error);
        ASSERT_SUCCESS(cthulhu_unpriv_get_settings(ctr_id, &change_ownership, &new_uid, &new_gid), goto error);
        ASSERT_SUCCESS(amxc_var_new(&extract_params), goto error);
        ASSERT_SUCCESS(amxc_var_set_type(extract_params, AMXC_VAR_ID_HTABLE), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_cstring_t(extract_params, BUNDLE_NAME, bundle_name), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_cstring_t(extract_params, BUNDLE_EXTRACT_LOCATION, app_data->bundle_extract_location), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_cstring_t(extract_params, BUNDLE_LOCATION, app_data->bundle_location), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_bool(extract_params, CHANGE_OWNERSHIP, change_ownership), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_uint64_t(extract_params, NEW_UID, new_uid), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_uint64_t(extract_params, NEW_GID, new_gid), goto error);

        ASSERT_SUCCESS(amxc_var_new(&cb_params), goto error);
        ASSERT_SUCCESS(amxc_var_set_type(cb_params, AMXC_VAR_ID_HTABLE), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_cstring_t(cb_params, CTR_ID, ctr_id), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_cstring_t(cb_params, BUNDLE_NAME, bundle_name), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_uint64_t(cb_params, DEFERRED_CALL_ID, deferred_call_id), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_cthulhu_config_t(cb_params, CONFIG, cthulhu_config), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_data(cb_params, NOTIF_DATA, cthulhu_notif_data_copy(notif_data)), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_bool(cb_params, UPDATE, update), goto error);

        ASSERT_SUCCESS(lcm_worker_task_new(&task), goto error);
        ASSERT_SUCCESS(lcm_worker_task_add_function(task, cthulhu_extract_bundle_wrapper, extract_params), goto error);
        ASSERT_SUCCESS(lcm_worker_task_add_callback(task, cthulhu_build_container_bundle_extracted, cb_params), goto error);

        ASSERT_SUCCESS(lcm_worker_add_task(cthulhu_get_worker(), task, false), goto error);
    } else {
        image_manifest = cthulhu_get_image_manifest_schema_from_image(bundle_location, ctr_id, bundle_version, app_data->image_location, app_data->blob_location);
        if(image_manifest == NULL) {
            NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not find Image Manifest for image %s:%s", bundle_name, bundle_version);
            goto error;
        }

        cthulhu_update_manifest_params(ctr_id, image_manifest);
        cthulhu_plugin_ctr_create(ctr_id, metadata, image_manifest, cthulhu_config);
        cthulhu_ctr_apply_metadata(ctr_id, metadata);
        ASSERT_SUCCESS(cthulhu_unpriv_create_user(ctr_id), goto error);
        ASSERT_SUCCESS(cthulhu_unpriv_get_settings(ctr_id, &change_ownership, &new_uid, &new_gid), goto error);

        ASSERT_SUCCESS(amxc_var_new(&extract_params), goto error);
        ASSERT_SUCCESS(amxc_var_set_type(extract_params, AMXC_VAR_ID_HTABLE), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_cstring_t(extract_params, CTR_ID, ctr_id), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_cstring_t(extract_params, BUNDLE_NAME, bundle_name), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_cstring_t(extract_params, STORAGE_LOCATION, app_data->blob_location), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_cstring_t(extract_params, LAYER_LOCATION, app_data->layer_location), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_cstring_t(extract_params, CTR_ROOTFS, cthulhu_config->rootfs), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_bool(extract_params, CHANGE_OWNERSHIP, change_ownership), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_uint64_t(extract_params, NEW_UID, new_uid), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_uint64_t(extract_params, NEW_GID, new_gid), goto error);
        amxc_var_t* layer_digests = amxc_var_add_new_key(extract_params, LAYER_DIGESTS);
        ASSERT_NOT_NULL(layer_digests, goto error);
        imagemanifestlayers_to_var(layer_digests, image_manifest);

        ASSERT_SUCCESS(amxc_var_new(&cb_params), goto error);
        ASSERT_SUCCESS(amxc_var_set_type(cb_params, AMXC_VAR_ID_HTABLE), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_cstring_t(cb_params, CTR_ID, ctr_id), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_cstring_t(cb_params, BUNDLE_NAME, bundle_name), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_uint64_t(cb_params, DEFERRED_CALL_ID, deferred_call_id), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_cthulhu_config_t(cb_params, CONFIG, cthulhu_config), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_data(cb_params, IMAGE_MANIFEST, image_manifest), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_data(cb_params, NOTIF_DATA, cthulhu_notif_data_copy(notif_data)), goto error);
        ASSERT_NOT_NULL(amxc_var_add_new_key_bool(cb_params, UPDATE, update), goto error);
        amxc_var_t* layers = amxc_var_add_new_key(cb_params, LAYER_DIGESTS);
        ASSERT_SUCCESS(amxc_var_copy(layers, layer_digests), goto error);

        ASSERT_SUCCESS(lcm_worker_task_new(&task), goto error);
        ASSERT_SUCCESS(lcm_worker_task_add_function(task, cthulhu_extract_layers, extract_params), goto error);
        ASSERT_SUCCESS(lcm_worker_task_add_callback(task, cthulhu_build_container_layers_extracted, cb_params), goto error);

        ASSERT_SUCCESS(lcm_worker_add_task(cthulhu_get_worker(), task, false), goto error);
    }
    res = cthulhu_action_res_deferred;
    goto exit;

error:
    lcm_worker_task_delete(&task);
    amxc_var_delete(&cb_params);
    if(image_manifest != NULL) {
        free_image_spec_schema_image_manifest_schema(image_manifest);
    }
    cthulhu_config_delete(&cthulhu_config);

exit:
    amxc_var_delete(&extract_params);
    return res;
}

static int cthulhu_extract_layers(UNUSED lcm_worker_t* worker, amxc_var_t* var, UNUSED int rc, UNUSED amxc_var_t* data) {
    int retval = -1;
    amxc_string_t target;
    const char* ctr_id = GET_CHAR(var, CTR_ID);
    const char* bundle_name = GET_CHAR(var, BUNDLE_NAME);
    const char* storage_location = GET_CHAR(var, STORAGE_LOCATION);
    const char* layer_location = GET_CHAR(var, LAYER_LOCATION);
    const char* ctr_rootfs = GET_CHAR(var, CTR_ROOTFS);
    bool change_ownership = GET_BOOL(var, CHANGE_OWNERSHIP);
    uid_t new_uid = GET_UINT64(var, NEW_UID);
    gid_t new_gid = GET_UINT64(var, NEW_GID);
    amxc_var_t* layer_digests = GET_ARG(var, LAYER_DIGESTS);
    amxc_string_t* layer_dir = NULL;

    amxc_string_init(&target, 0);

    ASSERT_NOT_NULL(layer_digests, goto exit, "CTR[%s]: Layer Digests are empty for image [%s]", ctr_id, bundle_name);
    ASSERT_NOT_NULL(amxc_var_get_first(layer_digests), goto exit, "CTR[%s]: No layers are defined for image [%s]", ctr_id, bundle_name);

    // create the rootfs location. if it exists, delete it first
    // the fact that we are here means that this is a new container instance with this name
    if(cthulhu_isdir(ctr_rootfs)) {
        SAH_TRACEZ_INFO(ME, "CTR[%s]: Delete directory %s", ctr_id, ctr_rootfs);
        cthulhu_rmdir(ctr_rootfs);
        ASSERT_FALSE(cthulhu_isdir(ctr_rootfs), goto exit, "CTR[%s]: Failed to remove dir %s", ctr_id, ctr_rootfs);
    }
    ASSERT_SUCCESS(cthulhu_mkdir(ctr_rootfs, false), goto exit, "CTR[%s]: Could not create dir [%s] (%d: %s)", ctr_id, ctr_rootfs, errno, strerror(errno));

    amxc_var_for_each(digest_var, layer_digests) {
        const char* digest = amxc_var_get_const_cstring_t(digest_var);

        if(cthulhu_overlayfs_supported()) {
            if(cthulhu_extract_layer(digest,
                                     storage_location,
                                     layer_location) != 0) {
                SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not extract all layers", ctr_id);
                goto exit;
            }
            if(change_ownership) {
                layer_dir = cthulhu_get_digest_file(digest, layer_location);
                ASSERT_NOT_NULL(layer_dir, goto exit);
                CHECK_FAILURE_LOG(WARNING, cthulhu_unpriv_layer_post_extract(layer_dir->buffer, new_uid, new_gid), NO_INSTRUCTION,
                                  "Container will be installed but running it might fail");
                amxc_string_delete(&layer_dir);
            }
        } else {
            if(cthulhu_extract_layer_to_rootfs(digest,
                                               storage_location,
                                               ctr_rootfs) != 0) {
                SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not extract all layers", ctr_id);
                goto exit;
            }
            if(change_ownership) {
                cthulhu_chown_recursive(ctr_rootfs, new_uid, new_gid);
            }
        }
    }
    retval = 0;

exit:
    amxc_string_delete(&layer_dir);
    amxc_string_clean(&target);
    return retval;
}



static int cthulhu_add_layers_to_dm(const amxc_var_t* layer_digests, const char* ctr_id) {
    int res = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* layer_templ = NULL;
    amxd_trans_t trans;
    int layer_index = 0;

    amxd_trans_init(&trans);
    ctr_obj = cthulhu_ctr_get(ctr_id);
    CHECK_NULL_LOG(ERROR, ctr_obj, NO_INSTRUCTION, "CTR[%s]: Could not get DM object", ctr_id);

    layer_templ = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_LAYERS);
    ASSERT_NOT_NULL(layer_templ, goto exit, "CTR[%s]: Could not get the layers", ctr_id);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, layer_templ);

    amxc_var_for_each(digest_var, layer_digests) {
        amxd_trans_select_object(&trans, layer_templ);
        const char* layer_name = amxc_var_get_const_cstring_t(digest_var);
        amxd_trans_add_inst(&trans, 0, NULL);
        amxd_trans_set_value(int32_t, &trans, CTHULHU_CONTAINER_LAYERS_INDEX, layer_index++);
        amxd_trans_set_value(cstring_t, &trans, CTHULHU_CONTAINER_LAYERS_LAYER, layer_name);
    }

    ASSERT_EQUAL(amxd_trans_apply(&trans, cthulhu_get_dm()), amxd_status_ok, goto exit,
                 "CTR[%s]: Could not create layer instance", ctr_id);

    res = 0;
exit:
    amxd_trans_clean(&trans);
    return res;
}

static int cthulhu_build_container_layers_extracted(UNUSED lcm_worker_t* worker, amxc_var_t* var, int rc, UNUSED amxc_var_t* data) {
    int res = -1;
    image_spec_schema_config_schema* image_config = NULL;
    const char* bundle_name = GET_CHAR(var, BUNDLE_NAME);
    const char* ctr_id = GET_CHAR(var, CTR_ID);
    image_spec_schema_image_manifest_schema* image_manifest = GET_DATA(var, IMAGE_MANIFEST);
    cthulhu_notif_data_t* notif_data = GET_DATA(var, NOTIF_DATA);
    int have_deferred_call_id = 0;
    uint64_t deferred_call_id = 0;
    bool update = GET_BOOL(var, UPDATE);
    amxc_var_t* layer_digests = GET_ARG(var, LAYER_DIGESTS);

    amxc_var_t* tmp = amxc_var_get_key(var, CONFIG, AMXC_VAR_FLAG_DEFAULT);
    cthulhu_config_t* cthulhu_config = amxc_var_take_cthulhu_config_t(tmp);

    if(GET_ARG(var, DEFERRED_CALL_ID)) {
        deferred_call_id = GET_UINT64(var, DEFERRED_CALL_ID);
        have_deferred_call_id = 1;
    }

    cthulhu_app_t* app_data = cthulhu_get_app_data();

    if(rc != 0) {
        NOTIF_FAILED(notif_data, tr181_fault_system_resources_exceeded, "Could not extract the layers for image %s. Is the disk full?", bundle_name);
        goto exit;
    }
    if(!image_manifest) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not find Image Manifest for image %s", bundle_name);
        goto exit;
    }

    image_config = cthulhu_get_image_config(image_manifest, app_data->blob_location);
    if(image_config == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not find Image Config for image %s", bundle_name);
        goto exit;
    }
    // put the config data needed by the plugins in the config_hash
    if(cthulhu_config_fill(image_config->config, cthulhu_config) != 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Failed to convert config to amxc_var");
        goto exit;
    }

    if(cthulhu_add_layers_to_dm(layer_digests, ctr_id)) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Internal error. Layers cannot be configured");
        goto exit;
    }

    cthulhu_backend_create(ctr_id, cthulhu_config, notif_data, update);

    res = 0;
exit:
    amxc_var_delete(&var);
    cthulhu_notif_data_delete(&notif_data);

    cthulhu_config_delete(&cthulhu_config);
    if(image_manifest != NULL) {
        free_image_spec_schema_image_manifest_schema(image_manifest);
    }
    if(image_config != NULL) {
        free_image_spec_schema_config_schema(image_config);
    }
    if(have_deferred_call_id) {
        amxd_function_deferred_done(deferred_call_id, res == 0 ? amxd_status_ok : amxd_status_unknown_error, NULL, NULL);
    }
    return res;
}

static int cthulhu_backend_create(const char* ctr_id, cthulhu_config_t* cthulhu_config, cthulhu_notif_data_t* notif_data, bool update) {
    int res = -1;
    amxc_var_t create_data;
    amxc_var_t* ret = NULL;
    amxm_shared_object_t* backend = cthulhu_backend_get();
    amxd_object_t* ctr_obj = NULL;
    bool autostart = false;
    bool lastreqstateactive = false;

    amxc_var_new(&ret);
    amxc_var_init(&create_data);

    amxc_var_set(cthulhu_config_t, &create_data, cthulhu_config);

    res = amxm_so_execute_function(backend, MOD_CTHULHU, CTHULHU_CMD_CTR_CREATE, &create_data, ret);
    if(res != 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Module error (%d): %s",
                     res, amxc_var_constcast(cstring_t, ret));
        goto exit;
    }
    cthulhu_plugin_ctr_postcreate(ctr_id);

    cthulhu_update_containers();

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj && notif_data) {
        notif_data->obj = ctr_obj;
        cthulhu_notif_ctr_created(notif_data);
    }

    autostart = amxd_object_get_bool(ctr_obj, CTHULHU_CONTAINER_AUTOSTART, NULL);
    lastreqstateactive = amxd_object_get_bool(ctr_obj, CTHULHU_CONTAINER_LASTREQSTATEACTIVE, NULL);
    /* Start the container if autostart is 1 in install and in update case with lastreqstate as active */
    if(((update == 1) && (lastreqstateactive == 1)) || ((!update) && (autostart))) {
        cthulhu_notif_data_t* notif_data_no_id = cthulhu_notif_data_no_id(notif_data);
        cthulhu_ctr_start(ctr_id, notif_data_no_id);
        cthulhu_notif_data_delete(&notif_data_no_id);
    }

    res = 0;

exit:
    amxc_var_delete(&ret);
    amxc_var_clean(&create_data);

    return res;

}

static int cthulhu_extract_bundle_wrapper(UNUSED lcm_worker_t* worker, amxc_var_t* var, UNUSED int rc, UNUSED amxc_var_t* data) {
    int retval = -1;
    const char* bundle_name = GET_CHAR(var, BUNDLE_NAME);
    const char* bundle_extract_location = GET_CHAR(var, BUNDLE_EXTRACT_LOCATION);
    const char* bundle_location = GET_CHAR(var, BUNDLE_LOCATION);

    if(cthulhu_extract_bundle(bundle_name, bundle_extract_location, bundle_location) != 0) {
        SAH_TRACEZ_ERROR(ME, "Could not extract bundle %s", bundle_name);
        goto exit;
    }

    retval = 0;

exit:
    return retval;
}

static int cthulhu_build_container_bundle_extracted(UNUSED lcm_worker_t* worker, amxc_var_t* var, int rc, UNUSED amxc_var_t* data) {
    int res = -1;
    const char* bundle_name = GET_CHAR(var, BUNDLE_NAME);
    const char* ctr_id = GET_CHAR(var, CTR_ID);
    cthulhu_notif_data_t* notif_data = GET_DATA(var, NOTIF_DATA);
    int have_deferred_call_id = 0;
    uint64_t deferred_call_id = 0;
    bool update = GET_BOOL(var, UPDATE);

    amxc_var_t* tmp = amxc_var_get_key(var, CONFIG, AMXC_VAR_FLAG_DEFAULT);
    cthulhu_config_t* cthulhu_config = amxc_var_take_cthulhu_config_t(tmp);

    if(GET_ARG(var, DEFERRED_CALL_ID)) {
        deferred_call_id = GET_UINT64(var, DEFERRED_CALL_ID);
        have_deferred_call_id = 1;
    }

    if(rc != 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not extract the layers for image %s. Is the disk full?", bundle_name);
        goto exit;
    }

    cthulhu_config->bundle_provided = true;

    cthulhu_backend_create(ctr_id, cthulhu_config, notif_data, update);

    res = 0;
exit:
    amxc_var_delete(&var);
    cthulhu_notif_data_delete(&notif_data);

    cthulhu_config_delete(&cthulhu_config);

    if(have_deferred_call_id) {
        amxd_function_deferred_done(deferred_call_id, res == 0 ? amxd_status_ok : amxd_status_unknown_error, NULL, NULL);
    }
    return res;
}

static void cthulhu_update_manifest_params(const char* ctr_id,
                                           image_spec_schema_image_manifest_schema* manifest_schema) {
    amxc_var_t params;
    size_t it_annotations;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    if(manifest_schema->annotations) {
        for(it_annotations = 0; it_annotations < manifest_schema->annotations->len; it_annotations++) {
            char* annotation_key = manifest_schema->annotations->keys[it_annotations];
            if(strcmp("org.opencontainers.image.description", annotation_key) == 0) {
                amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_DESCRIPTION, manifest_schema->annotations->values[it_annotations]);
            } else if(strcmp("org.opencontainers.image.vendor", annotation_key) == 0) {
                amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_VENDOR, manifest_schema->annotations->values[it_annotations]);
            }
        }
    }
    cthulhu_ctr_update_dm(ctr_id, &params);
    amxc_var_clean(&params);
}

/**
 * @brief        cthulhu_ctr_apply_metadata_envvar
 *
 * @detail       Apply all EnvironmentVariables from metadata to a container
 * @param[in]    ctr_id, Container ID
 * @param[in]    metadata, metadata variant
 * @retval       0 in success, -1 on failure.
 */
static int cthulhu_ctr_apply_metadata_envvar(const char* ctr_id, const amxc_var_t* const metadata) {
    int ret = -1;
    amxd_object_t* ctr_obj = NULL;
    const amxc_var_t* environmentvariables_var = NULL;
    const amxc_llist_t* environmentvariables = NULL;
    amxc_var_t* environmentvariable = NULL;

    ASSERT_STR_NOT_EMPTY(ctr_id, goto exit);
    ASSERT_NOT_NULL(metadata, goto exit);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get DM object", ctr_id);
        goto exit;
    }

    environmentvariables_var = GET_ARG(metadata, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES);
    CHECK_NULL_LOG(INFO, environmentvariables_var, goto exit, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES " argument is not present");

    environmentvariables = amxc_var_get_const_amxc_llist_t(environmentvariables_var);
    CHECK_NULL_LOG(INFO, environmentvariables, goto exit, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES " argument is empty");

    ret = cthulhu_ctr_remove_all_environmentvariables(ctr_obj);
    if(ret == -1) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Can not clean Cthulhu DM for Env Vars", ctr_id);
        goto exit;
    }
    amxc_llist_for_each(it, environmentvariables) {
        environmentvariable = amxc_llist_it_get_data(it, amxc_var_t, lit);
        ASSERT_TRUE(amxc_var_type_of(environmentvariable) == AMXC_VAR_ID_HTABLE, goto exit);
        const char* key = GET_CHAR(environmentvariable, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_KEY);
        const char* value = GET_CHAR(environmentvariable, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_VALUE);

        cthulhu_ctr_add_environmentvariables_to_dm(ctr_id, key, value, false);
    }
    ret = 0;
exit:
    return ret;
}

static int cthulhu_ctr_apply_metadata_resources(const char* ctr_id, const amxc_var_t* const metadata) {
    int ret = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* resources_obj = NULL;
    char* priv_sb_id = NULL;
    amxd_object_t* priv_sb_obj = NULL;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxc_var_t* arg = NULL;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    ASSERT_STR_NOT_EMPTY(ctr_id, goto exit);
    ASSERT_NOT_NULL(metadata, goto exit);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get DM object", ctr_id);
        goto exit;
    }

// apply it on the container
    resources_obj = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_RESOURCES);
    if(!resources_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get Resources from object", ctr_id);
        goto exit;
    }

    amxd_trans_select_object(&trans, resources_obj);

    if((arg = GET_ARG(metadata, CTHULHU_CONTAINER_RESOURCES_DISKSPACE)) != NULL) {
        int32_t diskspace = amxc_var_get_int32_t(arg);
        if(diskspace >= -1) {
            SAH_TRACEZ_INFO(ME, "CTR[%s]: Set Diskspace limit to: %d", ctr_id, diskspace);
            amxd_trans_set_int32_t(&trans, CTHULHU_CONTAINER_RESOURCES_DISKSPACE, diskspace);
        }
    }
    if((arg = GET_ARG(metadata, CTHULHU_CONTAINER_RESOURCES_MEMORY)) != NULL) {
        int32_t memory = amxc_var_get_int32_t(arg);
        if(memory >= -1) {
            SAH_TRACEZ_INFO(ME, "CTR[%s]: Set Memory limit to: %d", ctr_id, memory);
            amxd_trans_set_int32_t(&trans, CTHULHU_CONTAINER_RESOURCES_MEMORY, memory);
        }
    }
    if((arg = GET_ARG(metadata, CTHULHU_CONTAINER_RESOURCES_CPU)) != NULL) {
        int32_t cpu = amxc_var_get_int32_t(arg);
        if((cpu >= 0) && (cpu <= 100)) {
            SAH_TRACEZ_INFO(ME, "CTR[%s]: Set CPU limit to: %d", ctr_id, cpu);
            amxd_trans_set_int32_t(&trans, CTHULHU_CONTAINER_RESOURCES_CPU, cpu);
        }
    }

    status = amxd_trans_apply(&trans, cthulhu_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Transaction failed. Status: %d", ctr_id, status);
        goto exit;
    }


// apply it on the private sandbox to prevent race conditions
    amxd_trans_clean(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    priv_sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_PRIVATESANDBOX, NULL);
    ASSERT_STR_NOT_EMPTY(priv_sb_id, goto exit);

    priv_sb_obj = cthulhu_sb_get(priv_sb_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get DM object", priv_sb_id);
        goto exit;
    }

    amxd_trans_select_object(&trans, priv_sb_obj);

    if((arg = GET_ARG(metadata, CTHULHU_CONTAINER_RESOURCES_DISKSPACE)) != NULL) {
        int32_t diskspace = amxc_var_get_int32_t(arg);
        if(diskspace >= -1) {
            SAH_TRACEZ_INFO(ME, "CTR[%s]: Set Diskspace limit to: %d", ctr_id, diskspace);
            amxd_trans_set_int32_t(&trans, CTHULHU_SANDBOX_DISKSPACE, diskspace);
        }
    }
    if((arg = GET_ARG(metadata, CTHULHU_CONTAINER_RESOURCES_MEMORY)) != NULL) {
        int32_t memory = amxc_var_get_int32_t(arg);
        if(memory >= -1) {
            SAH_TRACEZ_INFO(ME, "CTR[%s]: Set Memory limit to: %d", ctr_id, memory);
            amxd_trans_set_int32_t(&trans, CTHULHU_SANDBOX_MEMORY, memory);
        }
    }
    if((arg = GET_ARG(metadata, CTHULHU_CONTAINER_RESOURCES_CPU)) != NULL) {
        int32_t cpu = amxc_var_get_int32_t(arg);
        if((cpu >= 0) && (cpu <= 100)) {
            SAH_TRACEZ_INFO(ME, "CTR[%s]: Set CPU limit to: %d", ctr_id, cpu);
            amxd_trans_set_int32_t(&trans, CTHULHU_SANDBOX_CPU, cpu);
        }
    }

    status = amxd_trans_apply(&trans, cthulhu_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Transaction failed. Status: %d", ctr_id, status);
        goto exit;
    }
    ret = 0;

exit:
    free(priv_sb_id);
    amxd_trans_clean(&trans);
    return ret;
}

static int cthulhu_ctr_apply_metadata_autostart(const char* ctr_id, const amxc_var_t* const metadata) {
    int ret = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxc_var_t* arg = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    ASSERT_STR_NOT_EMPTY(ctr_id, goto exit);
    ASSERT_NOT_NULL(metadata, goto exit);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get DM object", ctr_id);
        goto exit;
    }

    amxd_trans_select_object(&trans, ctr_obj);

    if((arg = GET_ARG(metadata, CTHULHU_CONTAINER_AUTOSTART)) != NULL) {
        bool autostart = amxc_var_get_bool(arg);
        amxd_trans_set_bool(&trans, CTHULHU_CONTAINER_AUTOSTART, autostart);
    }

    status = amxd_trans_apply(&trans, cthulhu_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Transaction failed. Status: %d", ctr_id, status);
        goto exit;
    }
    ret = 0;

exit:
    amxd_trans_clean(&trans);
    return ret;
}

static int cthulhu_ctr_apply_metadata_autorestart(const char* ctr_id, const amxc_var_t* const metadata) {
    int ret = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* autorestart_obj = NULL;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxc_var_t* arg = NULL;
    amxc_var_t* temp_ptr = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    ASSERT_STR_NOT_EMPTY(ctr_id, goto exit);
    ASSERT_NOT_NULL(metadata, goto exit);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get DM object", ctr_id);
        goto exit;
    }

    autorestart_obj = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_AUTORESTART);
    if(!autorestart_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get Resources from object", ctr_id);
        goto exit;
    }

    amxd_trans_select_object(&trans, autorestart_obj);
    // amxd_trans_add_inst(&trans, 0, NULL);

    if((arg = GET_ARG(metadata, CTHULHU_CONTAINER_AUTORESTART)) != NULL) {
        if((temp_ptr = GET_ARG(arg, CTHULHU_DM_CTR_AR_ENABLE)) != NULL) {
            bool enable = amxc_var_dyncast(bool, temp_ptr);
            amxd_trans_set_bool(&trans, CTHULHU_DM_CTR_AR_ENABLED, enable);
        }

        if((temp_ptr = GET_ARG(arg, CTHULHU_DM_CTR_AR_MAX_RETRYCOUNT)) != NULL) {
            uint32_t max_retry_count = amxc_var_dyncast(uint32_t, temp_ptr);
            amxd_trans_set_uint32_t(&trans, CTHULHU_DM_CTR_AR_MAX_RETRYCOUNT, max_retry_count);
        }

        if((temp_ptr = GET_ARG(arg, CTHULHU_DM_CTR_AR_MIN_INT)) != NULL) {
            uint32_t minimum_interval = amxc_var_dyncast(uint32_t, temp_ptr);
            amxd_trans_set_uint32_t(&trans, CTHULHU_DM_CTR_AR_MIN_INT, minimum_interval);
        }

        if((temp_ptr = GET_ARG(arg, CTHULHU_DM_CTR_AR_MAX_INT)) != NULL) {
            uint32_t maximum_interval = amxc_var_dyncast(uint32_t, temp_ptr);
            amxd_trans_set_uint32_t(&trans, CTHULHU_DM_CTR_AR_MAX_INT, maximum_interval);
        }

        if((temp_ptr = GET_ARG(arg, CTHULHU_DM_CTR_AR_RETRY_INT)) != NULL) {
            uint32_t retry_interval = amxc_var_dyncast(uint32_t, temp_ptr);
            amxd_trans_set_uint32_t(&trans, CTHULHU_DM_CTR_AR_RETRY_INT, retry_interval);
        }

        if((temp_ptr = GET_ARG(arg, CTHULHU_DM_CTR_AR_RESET)) != NULL) {
            uint32_t reset_period = amxc_var_dyncast(uint32_t, temp_ptr);
            amxd_trans_set_uint32_t(&trans, CTHULHU_DM_CTR_AR_RESET, reset_period);
        }

        if((temp_ptr = GET_ARG(arg, CTHULHU_DM_CTR_AR_RETRYCOUNT)) != NULL) {
            uint32_t retry_count = amxc_var_dyncast(uint32_t, temp_ptr);
            amxd_trans_set_uint32_t(&trans, CTHULHU_DM_CTR_AR_RETRYCOUNT, retry_count);
        }
    }

    status = amxd_trans_apply(&trans, cthulhu_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Transaction failed. Status: %d", ctr_id, status);
        goto exit;
    }

exit:
    amxd_trans_clean(&trans);
    return ret;
}

static int cthulhu_ctr_remove_all_hostobjects(amxd_object_t* ctr_obj) {
    bool res = -1;
    amxd_object_t* hostobjects_templ = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_HOSTOBJECT);
    if(!hostobjects_templ) {
        SAH_TRACEZ_ERROR(ME, "Could not get the HostObjects");
        goto exit;
    }
    amxd_object_for_each(instance, it_hostobject, hostobjects_templ) {
        amxd_object_t* hostobject_obj = amxc_llist_it_get_data(it_hostobject, amxd_object_t, it);
        amxd_object_free(&hostobject_obj);
    }
    res = 0;
exit:
    return res;
}

static int cthulhu_ctr_remove_all_mounts(amxd_object_t* ctr_obj) {
    bool res = -1;
    amxd_object_t* mounts_templ = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_MOUNTS);
    if(!mounts_templ) {
        SAH_TRACEZ_ERROR(ME, "Could not get the mounts");
        goto exit;
    }
    amxd_object_for_each(instance, it_mount, mounts_templ) {
        amxd_object_t* mount_obj = amxc_llist_it_get_data(it_mount, amxd_object_t, it);
        amxd_object_free(&mount_obj);
    }
    res = 0;
exit:
    return res;
}

static int cthulhu_ctr_remove_all_devices(amxd_object_t* ctr_obj) {
    bool res = -1;
    char* priv_sb_id = NULL;
    amxd_object_t* sb_obj = NULL;
    amxd_object_t* device_instances_obj = NULL;

    priv_sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_PRIVATESANDBOX, NULL);
    ASSERT_NOT_NULL(priv_sb_id, goto exit);
    sb_obj = cthulhu_sb_get(priv_sb_id);
    ASSERT_NOT_NULL(sb_obj, goto exit);

    device_instances_obj = amxd_object_findf(sb_obj, CTHULHU_SANDBOX_DEVICES);

    if(!device_instances_obj) {
        SAH_TRACEZ_ERROR(ME, "Could not get the devices template");
        goto exit;
    }
    amxd_object_for_each(instance, it_dev, device_instances_obj) {
        amxd_object_t* dev_obj = amxc_llist_it_get_data(it_dev, amxd_object_t, it);
        amxd_object_free(&dev_obj);
    }
    res = 0;
exit:
    free(priv_sb_id);
    return res;
}

static int cthulhu_ctr_add_option(amxc_var_t* table, const amxc_string_t* option) {
    int res = -1;
    amxc_llist_t option_split;
    amxc_llist_init(&option_split);
    if(amxc_string_split_to_llist(option, &option_split, '=') != AMXC_STRING_SPLIT_OK) {
        SAH_TRACEZ_ERROR(ME, "Could not split option [%s]", option->buffer);
        goto exit;
    }
    switch(amxc_llist_size(&option_split)) {
    case 1:
    {
        amxc_string_t* key = amxc_string_from_llist_it(amxc_llist_get_at(&option_split, 0));
        amxc_string_trim(key, NULL);
        amxc_var_add_new_key(table, key->buffer);
        break;
    }
    case 2:
    {
        amxc_string_t* key = amxc_string_from_llist_it(amxc_llist_get_at(&option_split, 0));
        amxc_string_t* val = amxc_string_from_llist_it(amxc_llist_get_at(&option_split, 1));
        amxc_string_trim(key, NULL);
        amxc_string_trim(val, NULL);
        amxc_var_add_new_key_cstring_t(table, key->buffer, val->buffer);
        break;
    }
    default:
        SAH_TRACEZ_ERROR(ME, "Option cannot be decoded [%s]", option->buffer);
        goto exit;
    }

    res = 0;
exit:
    amxc_llist_clean(&option_split, amxc_string_list_it_free);
    return res;
}

amxc_var_t* cthulhu_ctr_options_string_to_var(const char* options) {
    amxc_var_t* options_var = NULL;
    amxc_string_t options_str;
    amxc_llist_t options_list;
    amxc_llist_it_t* it;

    amxc_string_init(&options_str, 0);
    amxc_llist_init(&options_list);

    amxc_var_new(&options_var);
    amxc_var_set_type(options_var, AMXC_VAR_ID_HTABLE);

    ASSERT_NOT_NULL(options, goto exit);

    ASSERT_SUCCESS(amxc_string_append(&options_str, options, strlen(options)), goto error);
    if(amxc_string_split_to_llist(&options_str, &options_list, ',') != AMXC_STRING_SPLIT_OK) {
        SAH_TRACEZ_ERROR(ME, "Could not split options [%s]", options_str.buffer);
        goto error;
    }

    while((it = amxc_llist_take_first(&options_list)) != NULL) {
        amxc_string_t* option = amxc_string_from_llist_it(it);
        cthulhu_ctr_add_option(options_var, option);
        amxc_string_delete(&option);
    }
    goto exit;
error:
    amxc_var_delete(&options_var);
exit:
    amxc_string_clean(&options_str);
    amxc_llist_clean(&options_list, amxc_string_list_it_free);
    return options_var;
}

amxc_string_t* cthulhu_ctr_options_var_to_string(amxc_var_t* options_var) {
    amxc_string_t* options_string = NULL;
    amxc_string_new(&options_string, 32);

    // build options string
    amxc_var_for_each(option, options_var) {
        if(!amxc_string_is_empty(options_string)) {
            amxc_string_append(options_string, ",", 1);
        }
        const char* key = amxc_var_key(option);
        if(amxc_var_type_of(option) == AMXC_VAR_ID_NULL) {
            amxc_string_append(options_string, key, strlen(key));
        } else {
            amxc_string_appendf(options_string, "%s=%s", key, GET_CHAR(option, NULL));
        }
    }
    return options_string;
}

static int cthulhu_ctr_apply_metadata_mount(const char* ctr_id, const amxc_var_t* const metadata) {
    int ret = -1;
    amxd_object_t* ctr_obj = NULL;
    const amxc_var_t* hostobjects_var = NULL;
    const amxc_llist_t* hostobjects = NULL;
    amxc_var_t* hostobject = NULL;
    amxc_llist_t options_list;

    amxc_llist_init(&options_list);

    ASSERT_STR_NOT_EMPTY(ctr_id, goto exit);
    ASSERT_NOT_NULL(metadata, goto exit);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get DM object", ctr_id);
        goto exit;
    }

    hostobjects_var = GET_ARG(metadata, CTHULHU_CONTAINER_HOSTOBJECT);
    ret = 0;
    CHECK_NULL_LOG(INFO, hostobjects_var, goto exit, CTHULHU_CONTAINER_HOSTOBJECT " argument is not present");

    ret = -1;
    hostobjects = amxc_var_get_const_amxc_llist_t(hostobjects_var);
    CHECK_NULL_LOG(INFO, hostobjects, goto exit, CTHULHU_CONTAINER_HOSTOBJECT " argument empty");

    // a hostobjects list is provided - clear all existing settings an apply the defaults
    cthulhu_ctr_remove_all_hostobjects(ctr_obj);
    cthulhu_ctr_remove_all_mounts(ctr_obj);
    cthulhu_ctr_remove_all_devices(ctr_obj);
    cthulhu_ctr_add_defaults(ctr_id);

    amxc_llist_for_each(it, hostobjects) {
        bool err = false;
        hostobject = amxc_llist_it_get_data(it, amxc_var_t, lit);
        ASSERT_TRUE(amxc_var_type_of(hostobject) == AMXC_VAR_ID_HTABLE, goto exit);
        const char* source = GET_CHAR(hostobject, CTHULHU_CONTAINER_HOSTOBJECT_SOURCE);
        const char* destination = GET_CHAR(hostobject, CTHULHU_CONTAINER_HOSTOBJECT_DESTINATION);
        const char* options = GET_CHAR(hostobject, CTHULHU_CONTAINER_HOSTOBJECT_OPTIONS);

        amxc_var_t* options_var = cthulhu_ctr_options_string_to_var(options);
        ASSERT_NOT_NULL(options_var, goto exit);
        amxc_var_t* type_var = amxc_var_take_key(options_var, "type");
        if(!type_var) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: No type is defined in HostObject options [%s]", ctr_id, options);
            err = true;
            goto clean_loop;
        }
        const char* type = GET_CHAR(type_var, NULL);
        if(string_is_empty(type)) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Type is invalid in HostObject options [%s]", ctr_id, options);
            err = true;
            goto clean_loop;
        }

        if(strcmp(type, "mount") == 0) {
            amxc_string_t* options_str = cthulhu_ctr_options_var_to_string(options_var);
            cthulhu_ctr_add_mount_to_dm(ctr_id, source, destination, options_str->buffer);
            amxc_string_delete(&options_str);
        } else if(strcmp(type, "device") == 0) {
            cthulhu_ctr_add_device_to_dm(ctr_id, source, destination, options_var);
        } else {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Type is unknown [%s]", ctr_id, type);
            err = true;
            goto clean_loop;
        }
        cthulhu_ctr_add_hostobject_to_dm(ctr_id, source, destination, options);

clean_loop:
        amxc_var_delete(&options_var);
        amxc_var_delete(&type_var);
        if(err) {
            goto exit;
        }

    }
    ret = 0;
exit:
    amxc_llist_clean(&options_list, amxc_string_list_it_free);
    return ret;
}

static int cthulhu_ctr_apply_metadata_unprivileged(const char* ctr_id, const amxc_var_t* const metadata) {
    int ret = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* unpriv_obj = NULL;
    const amxc_var_t* metadata_var = NULL;
    bool unprivileged = true;
    uint32_t num_required_uids = 10;

    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get DM object", ctr_id);
        goto exit;
    }
    unpriv_obj = amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_UNPRIVILEGED);
    if(!unpriv_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get Unprivileged object", ctr_id);
        goto exit;
    }

    amxd_trans_select_object(&trans, unpriv_obj);

    metadata_var = GET_ARG(metadata, CTHULHU_CONTAINER_UNPRIVILEGED_PRIVILEGED);
    if(metadata_var) {
        unprivileged = !amxc_var_dyncast(bool, metadata_var);
    }
    amxd_trans_set_bool(&trans, CTHULHU_CONTAINER_UNPRIVILEGED_ENABLED, unprivileged);

    metadata_var = GET_ARG(metadata, CTHULHU_CONTAINER_UNPRIVILEGED_NUMREQUIREDUIDS);
    if(metadata_var) {
        num_required_uids = amxc_var_dyncast(uint32_t, metadata_var);
    }
    amxd_trans_set_uint32_t(&trans, CTHULHU_CONTAINER_UNPRIVILEGED_NUMREQUIREDUIDS, num_required_uids);
    if(num_required_uids > 0) {
        amxd_trans_set_uint32_t(&trans, CTHULHU_CONTAINER_UNPRIVILEGED_UIDMAPPINGRANGE, num_required_uids - 1);
        amxd_trans_set_uint32_t(&trans, CTHULHU_CONTAINER_UNPRIVILEGED_GIDMAPPINGRANGE, num_required_uids - 1);
    }

    status = amxd_trans_apply(&trans, cthulhu_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Transaction failed. Status: %d", ctr_id, status);
    }

    ret = 0;
exit:
    amxd_trans_clean(&trans);
    return ret;
}

static int cthulhu_ctr_apply_metadata_debug(const char* ctr_id, UNUSED const amxc_var_t* const metadata) {
    int ret = -1;
    amxd_object_t* debug_config = NULL;
    amxd_object_t* ctr_obj = NULL;

    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit, "CTR[%s]: Could not get DM object", ctr_id);

    debug_config = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONFIG "." CTHULHU_CONFIG_DEBUG);
    ASSERT_NOT_NULL(debug_config, goto exit, "%s not found in DM", CTHULHU_CONFIG_DEBUG);

    bool keep_overlayfs_mounted = amxd_object_get_bool(debug_config, CTHULHU_CONFIG_DEBUG_DEFAULTKEEPOVERLAYFSMOUNTED, NULL);

    amxd_trans_select_object(&trans, ctr_obj);
    amxd_trans_set_bool(&trans, CTHULHU_CONTAINER_KEEPOVERLAYFSMOUNTED, keep_overlayfs_mounted);

    status = amxd_trans_apply(&trans, cthulhu_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Transaction failed. Status: %d", ctr_id, status);
    }

    ret = 0;
exit:
    amxd_trans_clean(&trans);
    return ret;
}

static int cthulhu_ctr_apply_metadata_appdata(const char* ctr_id, const amxc_var_t* const metadata) {
    int ret = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* sb_obj = NULL;
    amxd_object_t* appdata_list_obj = NULL;
    const amxc_llist_t* appdata_list_in = NULL;
    const amxc_var_t* appdata_var = NULL;
    char* sb_id = NULL;
    amxc_var_t* var = NULL;

    appdata_var = GET_ARG(metadata, CTHULHU_CONTAINER_APPLICATIONDATA);
    CHECK_NULL_LOG(INFO, appdata_var, ret = 0; goto exit, CTHULHU_CONTAINER_APPLICATIONDATA " argument is not present");
    ASSERT_TRUE(amxc_var_type_of(appdata_var) == AMXC_VAR_ID_LIST, goto exit, "Variable " CTHULHU_CONTAINER_APPLICATIONDATA " is not a list");
    appdata_list_in = amxc_var_get_const_amxc_llist_t(appdata_var);
    ASSERT_NOT_NULL(appdata_list_in, goto exit, CTHULHU_CONTAINER_APPLICATIONDATA " argument is not valid");

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit, "CTR[%s]: Could not get DM object", ctr_id);
    sb_id = cthulhu_ctr_get_sb_id(ctr_obj);
    ASSERT_NOT_NULL(sb_id, goto exit, "CTR[%s]: Could not get Sandbox ID", ctr_id)
    sb_obj = cthulhu_sb_get(sb_id);
    ASSERT_NOT_NULL(sb_obj, goto exit, "CTR[%s]: Could not get Sandbox [%s]", ctr_id, sb_id);

    appdata_list_obj = amxd_object_findf(sb_obj, CTHULHU_SANDBOX_APPLICATIONDATA);
    ASSERT_NOT_NULL(appdata_list_obj, goto exit, "SB[%s] %s not found", sb_id, CTHULHU_SANDBOX_APPLICATIONDATA);

    amxc_llist_for_each(it, appdata_list_in) {
        amxc_var_t* appdata = amxc_llist_it_get_data(it, amxc_var_t, lit);
        ASSERT_TRUE(amxc_var_type_of(appdata) == AMXC_VAR_ID_HTABLE, goto exit, "an element of " CTHULHU_CONTAINER_APPLICATIONDATA " is not a htable");
        const char* name = GET_CHAR(appdata, CTHULHU_CONTAINER_APPLICATIONDATA_NAME);
        uint32_t capacity = GET_UINT32(appdata, CTHULHU_CONTAINER_APPLICATIONDATA_CAPACITY);
        bool encrypted = true;
        var = GET_ARG(appdata, CTHULHU_CONTAINER_APPLICATIONDATA_ENCRYPTED);
        if(var) {
            encrypted = amxc_var_dyncast(bool, var);
        }
        SAH_TRACEZ_WARNING(ME, "CTR[%s]: Application Data Encryption is requested but not supported for Application Data [%s]", ctr_id, name);
        const char* retain = GET_CHAR(appdata, CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN);
        const char* accesspath = GET_CHAR(appdata, CTHULHU_CONTAINER_APPLICATIONDATA_ACCESSPATH);
        ASSERT_SUCCESS(cthulhu_sb_create_applicationdata(sb_id, ctr_id, name, capacity, encrypted, retain, accesspath), goto exit);
    }

exit:
    free(sb_id);
    return ret;
}

static int cthulhu_ctr_apply_metadata(const char* ctr_id, const amxc_var_t* const metadata) {
    int res = 0;
    CHECK_NULL_LOG(INFO, metadata, return 0, "metadata argument is not present");

    res |= cthulhu_ctr_apply_metadata_resources(ctr_id, metadata);
    res |= cthulhu_ctr_apply_metadata_autostart(ctr_id, metadata);
    res |= cthulhu_ctr_apply_metadata_autorestart(ctr_id, metadata);
    res |= cthulhu_ctr_apply_metadata_mount(ctr_id, metadata);
    res |= cthulhu_ctr_apply_metadata_envvar(ctr_id, metadata);
    res |= cthulhu_ctr_apply_metadata_debug(ctr_id, metadata);
    res |= cthulhu_ctr_apply_metadata_unprivileged(ctr_id, metadata);
    res |= cthulhu_ctr_apply_metadata_appdata(ctr_id, metadata);

    return res;
}

/**
 * Helper to append a string as a amxc_var_t to an amxc_htable_t
 *
 * @return int 0 on success
 *
 */
static int htable_add_string(amxc_htable_t* table, const char* key, const char* value) {
    int retval = -1;
    amxc_var_t* var = NULL;
    ASSERT_NOT_NULL(table, goto exit);
    ASSERT_NOT_NULL(key, goto exit);
    ASSERT_NOT_NULL(value, goto exit);

    ASSERT_SUCCESS(amxc_var_new(&var), goto exit);
    ASSERT_SUCCESS(amxc_var_set(cstring_t, var, value), goto exit);

    ASSERT_SUCCESS(amxc_htable_insert(table, key, &var->hit), goto exit);
    retval = 0;

exit:
    if(retval != 0) {
        amxc_var_delete(&var);
    }
    return retval;
}

/**
 * Fill in a cthulhu_config_t object with data taken from an OCI
 * config schema
 *
 *
 * @param image_config
 * @param cthulhu_config
 *
 * @return int 0 on success
 */
static int cthulhu_config_fill(image_spec_schema_config_schema_config* image_config,
                               cthulhu_config_t* cthulhu_config) {
    int res = -1;
    if(image_config == NULL) {
        SAH_TRACEZ_ERROR(ME, "Image_config is empty");
        goto exit;
    }
    if(cthulhu_config == NULL) {
        SAH_TRACEZ_ERROR(ME, "config is NULL");
        goto exit;
    }

    // User (Optional)
    if(image_config->user != NULL) {
        cthulhu_config->user = strdup(image_config->user);
    }
    // ExposedPorts (Optional)
    if(image_config->exposed_ports != NULL) {
        for(size_t i = 0; i < image_config->exposed_ports->len; ++i) {
            llist_append_string(&cthulhu_config->exposed_ports,
                                image_config->exposed_ports->keys[i]);
        }
    }
    // Env (Optional)
    for(size_t i = 0; i < image_config->env_len; ++i) {
        llist_append_string(&cthulhu_config->env, image_config->env[i]);
    }
    //Adding these Env Vars to Cthulhu DM
    amxc_llist_for_each(it, &cthulhu_config->env) {
        char* env = amxc_var_dyncast(cstring_t, amxc_var_from_llist_it(it));
        const char delimiters[] = "=";
        char* key = strtok(env, delimiters);
        char* value = strtok(NULL, delimiters);
        //Adding these Env to Cthulhu DM under EnvVariablesOCI
        cthulhu_ctr_add_environmentvariables_to_dm(cthulhu_config->id, key, value, true);
        free(env);
    }

    // Entrypoint (Optional)
    for(size_t i = 0; i < image_config->entrypoint_len; ++i) {
        llist_append_string(&cthulhu_config->entry_point, image_config->entrypoint[i]);
    }

    // Cmd (Optional)
    for(size_t i = 0; i < image_config->cmd_len; ++i) {
        llist_append_string(&cthulhu_config->cmd, image_config->cmd[i]);
    }

    // Volumes (Optional)
    if(image_config->volumes) {
        for(size_t i = 0; i < image_config->volumes->len; ++i) {
            llist_append_string(&cthulhu_config->volumes, image_config->volumes->keys[i]);
        }
    }
    // WorkingDir (Optional)
    if(image_config->working_dir) {
        cthulhu_config->working_dir = strdup(image_config->working_dir);
    }

    // Labels (Optional)
    if(image_config->labels) {
        for(size_t i = 0; i < image_config->labels->len; ++i) {
            htable_add_string(&cthulhu_config->labels,
                              image_config->labels->keys[i],
                              image_config->labels->values[i]);
        }
    }

    // StopSignal (Optional)
    if(image_config->stop_signal) {
        cthulhu_config->stop_signal = strdup(image_config->stop_signal);
    }

    res = 0;
exit:
    return res;
}
