/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>


#include "cthulhu_priv.h"
#include "test_setup.h"
#include "test_idmap.h"

#define ME "test"

int test_idmap_setup(UNUSED void** state) {
    LOG_ENTRY
    int res = test_cthulhu_setup();
    return res;
    LOG_EXIT
}

int test_idmap_teardown(UNUSED void** state) {
    LOG_ENTRY
    return test_cthulhu_teardown();
    LOG_EXIT
}



void test_idmap_create(UNUSED void** state) {
    LOG_ENTRY
    char templatesrc[] = "/tmp/idmap_src.XXXXXX";
    char* srcdir = mkdtemp(templatesrc);
    char templatedst[] = "/tmp/idmap_dest.XXXXXX";
    char* destdir = mkdtemp(templatedst);

    amxc_llist_t usermappings;
    amxc_llist_init(&usermappings);

    cthulhu_usermapping_data_add_to_list(&usermappings, cthulhu_usermapping_user, 0, 1000, 1);
    cthulhu_usermapping_data_add_to_list(&usermappings, cthulhu_usermapping_group, 0, 1000, 1);

    int res = cthulhu_idmap(srcdir, destdir, &usermappings);
    SAH_TRACEZ_ERROR(ME, "RES: %d", res);
    amxc_llist_clean(&usermappings, cthulhu_usermapping_data_it_free);
    LOG_EXIT
}

void test_idmap_layer(UNUSED void** state) {
    LOG_ENTRY
    amxc_llist_t usermappings;
    amxc_llist_init(&usermappings);
    char layername[] = "sha256:layername";
    cthulhu_app_t* app_data = cthulhu_get_app_data();


    cthulhu_usermapping_data_add_to_list(&usermappings, cthulhu_usermapping_user, 0, 1000, 1);
    cthulhu_usermapping_data_add_to_list(&usermappings, cthulhu_usermapping_group, 0, 1000, 1);

// create layer dir
    char* layerdir = cthulhu_layer_get_dir_from_name(app_data->layer_location, layername);
    cthulhu_mkdir(layerdir, true);
    free(layerdir);

    char* layer = cthulhu_idmap_layer(layername, "ctrid", &usermappings);
    SAH_TRACEZ_ERROR(ME, "LAYER: %s", layer);
    assert_non_null(layer);
    assert_string_equal(layer, "/tmp/data/cthulhu/layers/idmap/ctrid/sha256/layername");
    amxc_llist_clean(&usermappings, cthulhu_usermapping_data_it_free);
    free(layer);

    LOG_EXIT
}