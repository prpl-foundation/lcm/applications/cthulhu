/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>


#include "cthulhu_priv.h"
#include "test_unprivileged.h"
#include "expected_signal.h"
#include "test_setup.h"
#include "dummy_be.h"



#define ME "test"

#define ID_START 0x3000


const char* sb_name = "sb_for_ctr";

int test_unpriv_setup(UNUSED void** state) {
    LOG_ENTRY
    int res = test_cthulhu_setup();
    testhelper_cthulhu_create_sandbox(sb_name, true);
    return res;
    LOG_EXIT
}

int test_unpriv_teardown(UNUSED void** state) {
    LOG_ENTRY
    testhelper_cthulhu_stop_sandbox(sb_name);
    int res = test_cthulhu_teardown();
    return res;
    LOG_EXIT
}


void test_unpriv_create_ctr(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    amxc_htable_t unpriv;

    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_htable_init(&unpriv, 5);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_UNPRIVILEGED_PRIVILEGED, false);
    amxc_var_add_key(uint32_t, &args, CTHULHU_CONTAINER_UNPRIVILEGED_NUMREQUIREDUIDS, 101);

    amxc_var_add_new_key_amxc_htable_t(&args, CTHULHU_CONTAINER_UNPRIVILEGED, &unpriv);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    expected_sig_add_match_bool(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_ENABLED, true, false);
    expected_sig_add_match_uint32_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_NUMREQUIREDUIDS, 101, false);
    expected_sig_add_match_uint32_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_UIDMAPPINGSTART, 0, false);
    expected_sig_add_match_uint32_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_UIDMAPPINGRANGE, 100, false);
    expected_sig_add_match_uint32_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_GIDMAPPINGSTART, 0, false);
    expected_sig_add_match_uint32_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_GIDMAPPINGRANGE, 100, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);

    amxc_htable_clean(&unpriv, variant_htable_it_free);
    LOG_EXIT
}

void test_unpriv_start_ctr(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_START, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "dm:object-changed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Container.Instances.cpe-ctr_1.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.from", CTHULHU_CONTAINER_STATUS_STARTING, false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", CTHULHU_CONTAINER_STATUS_RUNNING, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", true, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_RUNNING, false);
    expected_sig_add_match_bool(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_ENABLED, true, false);
    expected_sig_add_match_uint32_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_NUMREQUIREDUIDS, 101, false);
    expected_sig_add_match_uint32_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_UIDMAPPINGSTART, ID_START, false);
    expected_sig_add_match_uint32_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_UIDMAPPINGRANGE, 100, false);
    expected_sig_add_match_uint32_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_GIDMAPPINGSTART, ID_START, false);
    expected_sig_add_match_uint32_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_GIDMAPPINGRANGE, 100, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_USERNAME, "cthulhu_1", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_GROUPNAME, "cthulhu_1", false);

    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_unpriv_stop_ctr(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_STOP, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "dm:object-changed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Container.Instances.cpe-ctr_1.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.from", CTHULHU_CONTAINER_STATUS_RUNNING, false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", CTHULHU_CONTAINER_STATUS_STOPPING, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "dm:object-changed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Container.Instances.cpe-ctr_1.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.from", CTHULHU_CONTAINER_STATUS_STOPPING, false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", true, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    expected_sig_add_match_uint32_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_NUMREQUIREDUIDS, 101, false);
    expected_sig_add_match_uint32_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_UIDMAPPINGSTART, 0, false);
    expected_sig_add_match_uint32_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_UIDMAPPINGRANGE, 100, false);
    expected_sig_add_match_uint32_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_GIDMAPPINGSTART, 0, false);
    expected_sig_add_match_uint32_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_GIDMAPPINGRANGE, 100, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_USERNAME, "cthulhu_1", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj." CTHULHU_CONTAINER_UNPRIVILEGED "." CTHULHU_CONTAINER_UNPRIVILEGED_GROUPNAME, "cthulhu_1", false);

    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_unpriv_remove_ctr(UNUSED void** state) {
    LOG_ENTRY
    const char* ctr_id = "ctr_1";

    testhelper_remove_ctr(ctr_id, true);
    LOG_EXIT
}


static void create_unpriv_ctr(const char* ctr_id, uint32_t NumRequiredUIDs) {
    amxd_object_t* new_ctr = NULL;
    amxd_object_t* unpriv = NULL;
    amxd_object_t* ctr_instances = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES);
    assert_int_equal(amxd_object_add_instance(&new_ctr, ctr_instances, ctr_id, 0, NULL), amxd_status_ok);
    assert_int_equal(amxd_object_set_cstring_t(new_ctr, CTHULHU_CONTAINER_ID, ctr_id), amxd_status_ok);

    unpriv = amxd_object_findf(new_ctr, CTHULHU_CONTAINER_UNPRIVILEGED);
    assert_non_null(unpriv);
    assert_int_equal(amxd_object_set_bool(unpriv, CTHULHU_CONTAINER_UNPRIVILEGED_ENABLED, true), amxd_status_ok);
    assert_int_equal(amxd_object_set_uint32_t(unpriv, CTHULHU_CONTAINER_UNPRIVILEGED_NUMREQUIREDUIDS, NumRequiredUIDs), amxd_status_ok);
    assert_int_equal(amxd_object_set_uint32_t(unpriv, CTHULHU_CONTAINER_UNPRIVILEGED_UIDMAPPINGRANGE, NumRequiredUIDs - 1), amxd_status_ok);
    assert_int_equal(amxd_object_set_uint32_t(unpriv, CTHULHU_CONTAINER_UNPRIVILEGED_GIDMAPPINGRANGE, NumRequiredUIDs - 1), amxd_status_ok);
}

static void print_ranges(void) {
    printf("CTR_ID\tUIDREQ\tUIDSTART\tUIDRANGE\tUIDSTART\tUIDRANGE\n");
    amxd_object_t* ctr_instances = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES);
    amxd_object_for_each(instance, it_ctr, ctr_instances) {
        amxd_object_t* ctr_obj = amxc_llist_it_get_data(it_ctr, amxd_object_t, it);
        amxd_object_t* unpriv_obj = amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_UNPRIVILEGED);
        char* id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_ID, NULL);
        uint32_t numrequids = amxd_object_get_uint32_t(unpriv_obj, CTHULHU_CONTAINER_UNPRIVILEGED_NUMREQUIREDUIDS, NULL);
        uint32_t uidstart = amxd_object_get_uint32_t(unpriv_obj, CTHULHU_CONTAINER_UNPRIVILEGED_UIDMAPPINGSTART, NULL);
        uint32_t uidrange = amxd_object_get_uint32_t(unpriv_obj, CTHULHU_CONTAINER_UNPRIVILEGED_UIDMAPPINGRANGE, NULL);
        uint32_t gidstart = amxd_object_get_uint32_t(unpriv_obj, CTHULHU_CONTAINER_UNPRIVILEGED_GIDMAPPINGSTART, NULL);
        uint32_t gidrange = amxd_object_get_uint32_t(unpriv_obj, CTHULHU_CONTAINER_UNPRIVILEGED_GIDMAPPINGRANGE, NULL);
        printf("%s\t%u\t%u\t%u\t%u\t%u\n", id, numrequids, uidstart, uidrange, gidstart, gidrange);
        free(id);
    }
}

static void check_unpriv_range(const char* ctr_id, uint32_t uidstart, uint32_t uidrange, uint32_t gidstart, uint32_t gidrange) {
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* unpriv_obj = NULL;
    ctr_obj = cthulhu_ctr_get(ctr_id);
    assert_non_null(ctr_obj);
    unpriv_obj = amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_UNPRIVILEGED);

    uint32_t dm_uidstart = amxd_object_get_uint32_t(unpriv_obj, CTHULHU_CONTAINER_UNPRIVILEGED_UIDMAPPINGSTART, NULL);
    uint32_t dm_uidrange = amxd_object_get_uint32_t(unpriv_obj, CTHULHU_CONTAINER_UNPRIVILEGED_UIDMAPPINGRANGE, NULL);
    uint32_t dm_gidstart = amxd_object_get_uint32_t(unpriv_obj, CTHULHU_CONTAINER_UNPRIVILEGED_GIDMAPPINGSTART, NULL);
    uint32_t dm_gidrange = amxd_object_get_uint32_t(unpriv_obj, CTHULHU_CONTAINER_UNPRIVILEGED_GIDMAPPINGRANGE, NULL);
    assert_int_equal(uidstart, dm_uidstart);
    assert_int_equal(uidrange, dm_uidrange);
    assert_int_equal(gidstart, dm_gidstart);
    assert_int_equal(gidrange, dm_gidrange);

}

static void add_and_check_range(const char* ctr_id, uint32_t expected_start, uint32_t expected_range) {
    uint32_t uid = 0;
    uint32_t range = 0;
    printf("Check %s\n##############\n", ctr_id);
    cthulhu_unpriv_get_user_subids(ctr_id, &uid, &range);
    printf("uid %u range %u\n", uid, range);
    print_ranges();
    assert_int_equal(uid, expected_start);
    assert_int_equal(range, expected_range);
    check_unpriv_range(ctr_id, expected_start, expected_range, 0, expected_range);
    cthulhu_unpriv_get_group_subids(ctr_id, &uid, &range);
    print_ranges();
    assert_int_equal(uid, expected_start);
    assert_int_equal(range, expected_range);
    check_unpriv_range(ctr_id, expected_start, expected_range, expected_start, expected_range);
}

void test_unpriv_range(UNUSED void** state) {
    LOG_ENTRY
    uint32_t uid = 0;
    uint32_t range = 0;
    uint32_t expected_start = 0;
    uint32_t expected_range = 0;

    create_unpriv_ctr("ctr1", 1);
    create_unpriv_ctr("ctr2", 100 + 1);
    create_unpriv_ctr("ctr3", 80 + 1);
    create_unpriv_ctr("ctr4", 120 + 1);
    create_unpriv_ctr("ctr5", 90 + 1);
    create_unpriv_ctr("ctr6", 70 + 1);
    print_ranges();

    // test with 1 uid - no range should be defined
    // since root user is the only uid
    add_and_check_range("ctr1", 0, 0);
    // test with 100 uids
    add_and_check_range("ctr2", ID_START, 100);
    // add a range of 80
    add_and_check_range("ctr3", ID_START + 100, 80);
    // add a range of 120
    add_and_check_range("ctr4", ID_START + 100 + 80, 120);

    // remove ctr 3
    printf("Remove ctr3\n##############\n");
    amxd_object_t* ctr3 = cthulhu_ctr_get("ctr3");
    amxd_object_free(&ctr3);
    print_ranges();

    // add 90 UIDs, they should be put at the end since there is no room in the created gap of ctr3
    add_and_check_range("ctr5", ID_START + 100 + 80 + 120, 90);
    // add 70 UIDs, they should be fill the gap made by removing ctr3
    add_and_check_range("ctr6", ID_START + 100, 70);


    LOG_EXIT
}
