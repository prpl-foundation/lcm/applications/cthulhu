/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <amxc/amxc_macros.h>
#include <lcm/lcm_assert.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>


#include "cthulhu_priv.h"
#include "test_applicationdata.h"
#include "expected_signal.h"
#include "test_setup.h"



#define ME "test"

const char* sb_name = "sb_for_ctr";


static int appdata_add(amxc_llist_t* appdata_list, const char* name, uint32_t capacity, bool encrypted, const char* retain, const char* accesspath) {
    int res = -1;
    amxc_var_t* appdata = NULL;

    ASSERT_NOT_NULL(appdata_list, goto exit);

    ASSERT_SUCCESS(amxc_var_new(&appdata), goto exit);
    ASSERT_SUCCESS(amxc_var_set_type(appdata, AMXC_VAR_ID_HTABLE), goto exit);
    if(capacity > 0) {
        amxc_var_add_new_key_uint32_t(appdata, CTHULHU_CONTAINER_APPLICATIONDATA_CAPACITY, capacity);
    }
    amxc_var_add_new_key_bool(appdata, CTHULHU_CONTAINER_APPLICATIONDATA_ENCRYPTED, encrypted);

    if(name) {
        amxc_var_add_new_key_cstring_t(appdata, CTHULHU_CONTAINER_APPLICATIONDATA_NAME, name);
    }
    if(retain) {
        amxc_var_add_new_key_cstring_t(appdata, CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN, retain);
    }
    if(accesspath) {
        amxc_var_add_new_key_cstring_t(appdata, CTHULHU_CONTAINER_APPLICATIONDATA_ACCESSPATH, accesspath);
    }
    ASSERT_SUCCESS(amxc_llist_append(appdata_list, &(appdata)->lit), goto exit);

    res = 0;
exit:
    return res;
}

static amxd_object_t* test_get_appdata_list(const char* sb_id) {
    return amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX_INSTANCES ".[" CTHULHU_SANDBOX_ID " == '%s']."CTHULHU_SANDBOX_APPLICATIONDATA, sb_id);
}

static bool test_sb_contains_appdata(const char* sb_id,
                                     const char* name,
                                     uint32_t capacity,
                                     bool encrypted,
                                     const char* retain,
                                     const char* accesspath,
                                     const char* appuuid) {
    bool res = false;
    amxd_object_t* appdata_list = test_get_appdata_list(sb_id);
    amxd_object_for_each(instance, it_appdata, appdata_list) {
        amxd_object_t* instance = amxc_llist_it_get_data(it_appdata, amxd_object_t, it);
        char* inst_name = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_NAME, NULL);
        uint32_t inst_capacity = amxd_object_get_uint32_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_CAPACITY, NULL);
        bool inst_encrypted = amxd_object_get_bool(instance, CTHULHU_SANDBOX_APPLICATIONDATA_ENCRYPTED, NULL);
        char* inst_retain = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_RETAIN, NULL);
        char* inst_accesspath = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_ACCESSPATH, NULL);
        char* inst_appuuid = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_APPLICATIONDATA_APPLICATIONUUID, NULL);

        SAH_TRACEZ_NOTICE(ME, "ApplicationData\n\tName: %s\n\tCapacity: %u\n\tEncrypted: %d\n\tRetain: %s\n\tAccessPath: %s\n\tApplicationUUID: %s",
                          inst_name, inst_capacity, inst_encrypted, inst_retain, inst_accesspath, inst_appuuid);
        if(((strcmp(name, inst_name) == 0)) &&
           (capacity == inst_capacity) &&
           (encrypted == inst_encrypted) &&
           ((strcmp(retain, inst_retain) == 0)) &&
           ((strcmp(accesspath, inst_accesspath) == 0)) &&
           ((strcmp(appuuid, inst_appuuid) == 0))) {
            res = true;
            free(inst_name);
            free(inst_retain);
            free(inst_accesspath);
            free(inst_appuuid);
            break;
        }
        free(inst_name);
        free(inst_retain);
        free(inst_accesspath);
        free(inst_appuuid);
    }
    return res;
}


int test_cthulhu_appdata_setup (UNUSED void** state) {
    LOG_ENTRY
    int res = test_cthulhu_setup();
    testhelper_cthulhu_create_sandbox(sb_name, true);
    return res;
    LOG_EXIT
}

int test_cthulhu_appdata_teardown (UNUSED void** state) {
    LOG_ENTRY
    testhelper_cthulhu_stop_sandbox(sb_name);
    return test_cthulhu_teardown();
    LOG_EXIT
}

void test_cthulhu_appdata_invalid(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_appdata";
    amxc_llist_t* appdata_list = NULL;
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    // appdata is not a list
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_APPLICATIONDATA, "not_a_list");
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_unknown_error);
    amxc_var_clean(&args);
    // hostobject does not contain name
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&appdata_list), 0);
    assert_int_equal(appdata_add(appdata_list, NULL, 4, true, "Forever", "/accesspath"), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_APPLICATIONDATA, appdata_list);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_unknown_error);
    amxc_llist_delete(&appdata_list, variant_list_it_free);
    amxc_var_clean(&args);
    // hostobject does not contain capacity
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&appdata_list), 0);
    assert_int_equal(appdata_add(appdata_list, "my_appdata", 0, true, "Forever", "/accesspath"), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_APPLICATIONDATA, appdata_list);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_unknown_error);
    amxc_llist_delete(&appdata_list, variant_list_it_free);
    amxc_var_clean(&args);
    // hostobject does not contain retain
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&appdata_list), 0);
    assert_int_equal(appdata_add(appdata_list, "my_appdata", 3, true, NULL, "/accesspath"), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_APPLICATIONDATA, appdata_list);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_unknown_error);
    amxc_llist_delete(&appdata_list, variant_list_it_free);
    amxc_var_clean(&args);
    // hostobject does not contain accesspath
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&appdata_list), 0);
    assert_int_equal(appdata_add(appdata_list, "my_appdata", 3, true, "Forever", NULL), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_APPLICATIONDATA, appdata_list);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_unknown_error);
    amxc_llist_delete(&appdata_list, variant_list_it_free);
    amxc_var_clean(&args);
    // hostobject retain value is invalid
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&appdata_list), 0);
    assert_int_equal(appdata_add(appdata_list, "my_appdata", 3, true, "invalid_remain", "/accesspath"), 0);
    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_APPLICATIONDATA, appdata_list);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_unknown_error);
    amxc_llist_delete(&appdata_list, variant_list_it_free);
    amxc_var_clean(&args);

    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_appdata_add(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_appdata";
    amxc_llist_t* appdata_list = NULL;
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    // appdata is not a list
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&appdata_list), 0);
    assert_int_equal(appdata_add(appdata_list, "my_appdata", 3, true, CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_FOREVER, "/accesspath"), 0);
    assert_int_equal(appdata_add(appdata_list, "my_appdata2", 4, false, CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_UNTILSTOPPED, "/accesspath2"), 0);
    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_APPLICATIONDATA, appdata_list);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_llist_delete(&appdata_list, variant_list_it_free);

    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    expected_sig_clean(&expected_sig);

    assert_true(test_sb_contains_appdata(sb_name, "my_appdata", 3, true,
                                         CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_FOREVER, "/accesspath", ctr_id));
    assert_true(test_sb_contains_appdata(sb_name, "my_appdata2", 4, false,
                                         CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_UNTILSTOPPED, "/accesspath2", ctr_id));

    LOG_EXIT
}

void test_cthulhu_appdata_present_after_remove(UNUSED void** state) {
    LOG_ENTRY
    const char* ctr_id = "ctr_appdata";

    testhelper_remove_ctr(ctr_id, false);

    assert_true(test_sb_contains_appdata(sb_name, "my_appdata", 3, true,
                                         CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_FOREVER, "/accesspath", ctr_id));
    assert_false(test_sb_contains_appdata(sb_name, "my_appdata2", 4, false,
                                          CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_UNTILSTOPPED, "/accesspath2", ctr_id));

    LOG_EXIT
}

void test_cthulhu_appdata_add_again(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_appdata";
    amxc_llist_t* appdata_list = NULL;
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    // appdata is not a list
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&appdata_list), 0);
    assert_int_equal(appdata_add(appdata_list, "my_appdata", 5, true, CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_FOREVER, "/accesspath3"), 0);
    assert_int_equal(appdata_add(appdata_list, "my_appdata2", 6, false, CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_UNTILSTOPPED, "/accesspath4"), 0);
    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_APPLICATIONDATA, appdata_list);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_llist_delete(&appdata_list, variant_list_it_free);

    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    expected_sig_clean(&expected_sig);

    // old values are overwritten
    assert_false(test_sb_contains_appdata(sb_name, "my_appdata", 3, true,
                                          CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_FOREVER, "/accesspath", ctr_id));
    assert_false(test_sb_contains_appdata(sb_name, "my_appdata2", 4, false,
                                          CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_UNTILSTOPPED, "/accesspath2", ctr_id));

    assert_true(test_sb_contains_appdata(sb_name, "my_appdata", 5, true,
                                         CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_FOREVER, "/accesspath3", ctr_id));
    assert_true(test_sb_contains_appdata(sb_name, "my_appdata2", 6, false,
                                         CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_UNTILSTOPPED, "/accesspath4", ctr_id));

    LOG_EXIT
}

void test_cthulhu_appdata_removed_after_remove(UNUSED void** state) {
    LOG_ENTRY
    const char* ctr_id = "ctr_appdata";

    // remove data!
    testhelper_remove_ctr(ctr_id, true);

    assert_false(test_sb_contains_appdata(sb_name, "my_appdata", 5, true,
                                          CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_FOREVER, "/accesspath3", ctr_id));
    assert_false(test_sb_contains_appdata(sb_name, "my_appdata2", 6, false,
                                          CTHULHU_CONTAINER_APPLICATIONDATA_RETAIN_UNTILSTOPPED, "/accesspath4", ctr_id));


    LOG_EXIT
}